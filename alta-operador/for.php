
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 1.4.2
Purchase: http://wrapbootstrap.com
-->

<html xmlns="http://www.w3.org/1999/xhtml">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title>Alta Operador</title>

    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="/shortcut icon" href="assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link id="beyond-link" href="/assets/css/beyond.min.css" rel="stylesheet" />
    <link href="/assets/css/demo.min.css" rel="stylesheet" />
    <link href="/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="/assets/css/animate.min.css" rel="stylesheet" />
   
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />
     <link href="/assets/css/cambios.css" rel="stylesheet" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="/assets/js/skins.min.js"></script>
</head>
<!-- /Head -->
<!-- Body -->
<body>
   <?php 
    	include("../includes/topvar.php");
    ?>
    <!-- Main Container -->
    <div class="main-container container-fluid">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <div class="page-sidebar" id="sidebar">
               
            </div>
            <!-- /Page Sidebar -->
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Breadcrumb -->
                <div class="page-breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="/principal">Home</a>
                        </li>
                        <li>
                            Alta de Operador
                        </li>

                    </ul>
                </div>
                <!-- /Page Breadcrumb -->
                
                <!-- Page Body -->
                <div class="page-body">
                    		
                                                	   <ul class="nav nav-tabs" id="myTab">
				                                            <li class="active">
				                                                <a data-toggle="tab" href="#home">
				                                                    Datos
				                                                </a>
				                                            </li>
				                                        </ul>          
				                                    <form role="form" id="alta"> 
                                                	<div class="tab-content">
                                                			<div id="home" class="tab-pane in active">
			                                                    <div id="registration-form">
			                                                        
			                                                           <div class="row">
			                                                           		
				                                                            <div class="col-sm-12">
			                                                           				<h2>Tipo de negocio</h2>
			                                                           		</div>
			                                                           		<div class="col-sm-4">
					                                                            <div class="form-group">
					                                                                <span class="input-icon icon-right">
					                                                                	<label>Tipo de negocio</label>
					                                                                     <select  name="categoria"  class="form-control">
					                                                                    	<option value="">Tipo de negocio</option>
					                                                                    </select>
					                                                                    
					                                                                </span>
					                                                            </div>
				                                                            </div>
				                                                           
			                                                           		<div class="col-lg-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
		                                                   					<a href="javascript:void(0);" class="btn btn-primary shiny" id="clon">Agregar pregunta</a>
		                                                   			</div>
				                                                   	<div class="col-lg-6 col-sm-6 col-xs-12">
				                                                   		<div class="widget">
				                                                   			<div class="widget-header bordered-bottom bordered-blue">
									                                            <span class="widget-caption">Pregunta 1</span>
									                                        </div>
									                                        <div class="widget-body">
									                                        		<div>
											                                                  <div class="form-group" style="background: background: #f1f1f1; padding: 10px; border: 1px solid #ccc; "> 
											                                                    <span class="input-icon icon-right">
											                                                       	<label>Pregunta </label>
											                                                        <input  name="pregunta[]"  class="form-control">
								                                                                </span>
							                                                            	  </div>
							                                                            	  <div class="" style="margin-bottom: 15px;">
								                                                   					<a href="javascript:void(0);" class="btn btn-darkorange shiny" id="clon">Agregar respuesta</a>
								                                                   			</div>
											                                                   <div class="form-group">
											                                                       <span class="input-icon icon-right">
											                                                             <label>Respuesta 1</label>
											                                                             <input  name="pregunta[]"  class="form-control">
											                                                         </span>
											                                                    </div>
											                                                    <div class="form-group">
											                                                        <span class="input-icon icon-right">
											                                                            <label>Respuesta 2</label>
											                                                            <input  name="pregunta[]"  class="form-control">
											                                                        </span>
											                                                    </div>
											                                              </div>
									                                                </div>
						                                               		 </div> <!-- Fin del div wdiget-body -->
						                                           		</div> <!-- Fin del div wdiget -->
				                                                           
				                                                      </div> <!-- col-lg-6 col-sm-6 col-xs-12 -->
				                                                            
			                                                            <div class="form-group">
			                                                                <div class="checkbox">
			                                                                    <label>
			                                                                        <input type="checkbox" class="colored-blue" name="status" value="on">
			                                                                        <span class="text">Activo / Inactivo</span>
			                                                                    </label>
			                                                                </div>
			                                                            </div>
			                                                            
			                                                        
			                                                    </div>
		                                                    </div><!-- fin del primer tab id home -->
                                                    </div><!-- fin del tab content -->
                                                    <button class="btn btn-primary" type="submit">Guardar</button>
			                                                            <img id="carga" src="/assets/img/carga.gif">
			                                                            <div id="respuesta"></div>
                                   					</form>
                </div>
                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->

    </div>
	 <!--Email Modal Templates-->
    <div id="myModal" style="display:none;">
        <div class="row">
            <div class="col-md-12">
                <?php 
                	//print alta("zonas", $link);
                ?>
            </div>
        </div>
    </div>
    <!--End Email Templates-->
    <!--Basic Scripts-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="/assets/js/beyond.min.js"></script>
    <!--Jquery Select2-->
    <script src="/assets/js/select2/select2.js"></script>
    <script src="/assets/js/bootbox/bootbox.js"></script>
	
    
</body>
<!--  /Body -->
</html>
