# MySQL-Front 5.1  (Build 1.28)

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE */;
/*!40101 SET SQL_MODE='NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES */;
/*!40103 SET SQL_NOTES='ON' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;


# Host: sectornetcancun.com    Database: sectorne_taxy
# ------------------------------------------------------
# Server version 5.5.5-10.1.28-MariaDB

#
# Source for table actividad_proveedor
#

DROP TABLE IF EXISTS `actividad_proveedor`;
CREATE TABLE `actividad_proveedor` (
  `id_actividad_empresarial` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(400) DEFAULT NULL,
  PRIMARY KEY (`id_actividad_empresarial`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

#
# Dumping data for table actividad_proveedor
#
LOCK TABLES `actividad_proveedor` WRITE;
/*!40000 ALTER TABLE `actividad_proveedor` DISABLE KEYS */;

INSERT INTO `actividad_proveedor` VALUES (1,'Accesos Autom�ticos');
INSERT INTO `actividad_proveedor` VALUES (2,'Agencia de Viajes');
INSERT INTO `actividad_proveedor` VALUES (3,'Agencia Inmobiliaria');
INSERT INTO `actividad_proveedor` VALUES (4,'Ascensores y Elevadores');
INSERT INTO `actividad_proveedor` VALUES (5,'Auditor�a Externa');
INSERT INTO `actividad_proveedor` VALUES (6,'Carpinteria');
INSERT INTO `actividad_proveedor` VALUES (7,'Cerrajer�a, Puertas y Ventanas');
INSERT INTO `actividad_proveedor` VALUES (8,'Construcci�n y Remodelaci�n');
INSERT INTO `actividad_proveedor` VALUES (9,'Contabilidad y Finanzas');
INSERT INTO `actividad_proveedor` VALUES (10,'Desarrollador Inmobiliario');
INSERT INTO `actividad_proveedor` VALUES (11,'Imprentas');
INSERT INTO `actividad_proveedor` VALUES (12,'Imagen Corporativa');
INSERT INTO `actividad_proveedor` VALUES (13,'Dise�o, Publicidad en P�ginas y Buscadores Web');
INSERT INTO `actividad_proveedor` VALUES (14,'Electricidad');
INSERT INTO `actividad_proveedor` VALUES (15,'Equipo de Comunicaci�n');
INSERT INTO `actividad_proveedor` VALUES (16,'Equipo de Seguridad Industrial');
INSERT INTO `actividad_proveedor` VALUES (17,'Herrer�a y Canceler�a');
INSERT INTO `actividad_proveedor` VALUES (18,'Instalaciones Hidrosanitarias');
INSERT INTO `actividad_proveedor` VALUES (19,'Internet y Conexos');
INSERT INTO `actividad_proveedor` VALUES (20,'Jardiner�a');
INSERT INTO `actividad_proveedor` VALUES (21,'Mobiliario y Equipo de Oficina');
INSERT INTO `actividad_proveedor` VALUES (22,'Pintura e Impermeabilizaci�n');
INSERT INTO `actividad_proveedor` VALUES (23,'Piscinas, Bombas y Cisternas');
INSERT INTO `actividad_proveedor` VALUES (24,'Pisos, Lozas y Recubrimientos');
INSERT INTO `actividad_proveedor` VALUES (25,'Publicidad en Exteriores');
INSERT INTO `actividad_proveedor` VALUES (26,'Seguridad Residencial');
INSERT INTO `actividad_proveedor` VALUES (27,'Servicios Tur�sticos y Recreativos');
INSERT INTO `actividad_proveedor` VALUES (28,'Tierra, Qu�micos y Fertilizantes');
INSERT INTO `actividad_proveedor` VALUES (29,'Transportaci�n de Personal');
INSERT INTO `actividad_proveedor` VALUES (30,'Transportaci�n de Turismo');
INSERT INTO `actividad_proveedor` VALUES (31,'Transportaci�n Ejecutiva');
INSERT INTO `actividad_proveedor` VALUES (32,'Tratamiento de Agua');
INSERT INTO `actividad_proveedor` VALUES (33,'Uniformes y Equipo de Trabajo');
INSERT INTO `actividad_proveedor` VALUES (34,'Veh�culos Automotores');
INSERT INTO `actividad_proveedor` VALUES (35,'Otros (Describa)');
/*!40000 ALTER TABLE `actividad_proveedor` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table administracion_condos
#

DROP TABLE IF EXISTS `administracion_condos`;
CREATE TABLE `administracion_condos` (
  `id_administracion_condos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `tel_movil` varchar(255) DEFAULT NULL,
  `tel_oficina` varchar(255) DEFAULT NULL,
  `nombre_condominio` varchar(255) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `propiedad_condominio` int(11) DEFAULT NULL,
  `unidades_privativas` int(11) DEFAULT NULL,
  `tipo_cuota` int(6) DEFAULT NULL,
  `tipo_admin` int(11) DEFAULT NULL,
  `integrante_comite` int(11) DEFAULT NULL,
  `comentarios` text,
  PRIMARY KEY (`id_administracion_condos`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

#
# Dumping data for table administracion_condos
#
LOCK TABLES `administracion_condos` WRITE;
/*!40000 ALTER TABLE `administracion_condos` DISABLE KEYS */;

INSERT INTO `administracion_condos` VALUES (2,'jesus gabriel vazquez canche','vazquez1072@gmail.com','9983498879','0','La Odisea',1,'Aguas calientes',1,1,1,1,1,'daasdasdasd');
INSERT INTO `administracion_condos` VALUES (5,'Hugo Cedillo','hugocedillo@outlook.com','7222609882','','La Castellana',23,'Canc�n',1,5,3,2,2,'');
INSERT INTO `administracion_condos` VALUES (6,'Hugo Cedillo','hugocedillo@outlook.com','7222609882','7222609882','La Castellana',23,'Cancun',2,6,1,1,1,'');
INSERT INTO `administracion_condos` VALUES (7,'Hugo Cedillo','hugocedillo@outlook.com','7222609882','','Fracc. La Castilla',23,'Canc�n',2,6,3,2,2,'');
INSERT INTO `administracion_condos` VALUES (8,'jesus gabriel vazquez canche','info@sectornetcancun.com','9983498879','56565656','una ',2,'cancun',2,5,2,1,1,'');
INSERT INTO `administracion_condos` VALUES (9,'Hugo Cedillo','hugocedillo@outlook.com','7222609882','7222609882','La Castellana',23,'Cancun',1,5,3,3,2,'Saludos');
INSERT INTO `administracion_condos` VALUES (10,'Hugo Cedillo','hugocedillo@outlook.com','7222609882','','La Castellana',23,'Cancun',2,3,1,3,2,'');
INSERT INTO `administracion_condos` VALUES (11,'Hugo Cedillo','hugocedillo@outlook.com','7222609882','','La Castellana',23,'Cancun',2,3,1,3,2,'');
INSERT INTO `administracion_condos` VALUES (12,'Hugo Cedillo','hugocedillo@outlook.com','7222609882','','La Castellana',23,'Cancun',2,4,3,3,2,'');
INSERT INTO `administracion_condos` VALUES (13,'Hugo Cedillo Gonzalez','hugocedillo@outlook.com','7222609882','','La Castilla',23,'Cancun',2,3,3,3,2,'');
/*!40000 ALTER TABLE `administracion_condos` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table amenidades
#

DROP TABLE IF EXISTS `amenidades`;
CREATE TABLE `amenidades` (
  `id_amenidad` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_esp` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `nombre_eng` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `status` varchar(3) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`id_amenidad`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

#
# Dumping data for table amenidades
#
LOCK TABLES `amenidades` WRITE;
/*!40000 ALTER TABLE `amenidades` DISABLE KEYS */;

INSERT INTO `amenidades` VALUES (2,'Transportaci�n en Van','Van Transportation','on');
INSERT INTO `amenidades` VALUES (3,'Transportaci�n en Embarcaci�n','Boat Transportation','on');
INSERT INTO `amenidades` VALUES (4,'Alimentos','Food','on');
INSERT INTO `amenidades` VALUES (5,'Refrescos','Soft Drinks','on');
INSERT INTO `amenidades` VALUES (7,'Barra Libre','Open Bar','on');
INSERT INTO `amenidades` VALUES (8,'Entrada','Access Ticket','on');
INSERT INTO `amenidades` VALUES (10,'Refrigerio','Box Lunch','on');
INSERT INTO `amenidades` VALUES (11,'Frutos','Light Fruits','on');
INSERT INTO `amenidades` VALUES (13,'Gu�a de Tour','Tour Guide','on');
INSERT INTO `amenidades` VALUES (14,'Bicicleta','Bike','on');
INSERT INTO `amenidades` VALUES (19,'Transportaci�n en Autob�s','Bus Transportation','on');
INSERT INTO `amenidades` VALUES (20,'Caf� / T�','Hot Drinks','on');
INSERT INTO `amenidades` VALUES (21,'Coktel de Bienvenida','Welcome Drink','on');
INSERT INTO `amenidades` VALUES (22,'Sombrillas','Umbrellas','on');
INSERT INTO `amenidades` VALUES (23,'Agua Embotellada','Bottled Water','on');
INSERT INTO `amenidades` VALUES (25,'Vino','Wine','on');
INSERT INTO `amenidades` VALUES (26,'Glam Picnic','Glam Picnic','on');
INSERT INTO `amenidades` VALUES (27,'Cerveza','Beer','on');
INSERT INTO `amenidades` VALUES (28,'Binoculares',' Binoculars','on');
INSERT INTO `amenidades` VALUES (29,'M�sica en Vivo','Live Music','on');
INSERT INTO `amenidades` VALUES (30,'Desayuno','Breakfast','on');
INSERT INTO `amenidades` VALUES (31,'Hotel','Hotel','on');
INSERT INTO `amenidades` VALUES (33,'Movilidad','Mobility','');
INSERT INTO `amenidades` VALUES (34,'Buffet','Buffet','on');
INSERT INTO `amenidades` VALUES (35,'Equipo de Seguridad','Security Gear','on');
INSERT INTO `amenidades` VALUES (36,'Botanas','Snacks','on');
/*!40000 ALTER TABLE `amenidades` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table anios
#

DROP TABLE IF EXISTS `anios`;
CREATE TABLE `anios` (
  `id_anio` int(11) NOT NULL AUTO_INCREMENT,
  `anio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_anio`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

#
# Dumping data for table anios
#
LOCK TABLES `anios` WRITE;
/*!40000 ALTER TABLE `anios` DISABLE KEYS */;

INSERT INTO `anios` VALUES (1,2015);
INSERT INTO `anios` VALUES (2,2016);
INSERT INTO `anios` VALUES (3,2017);
/*!40000 ALTER TABLE `anios` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table area_personal
#

DROP TABLE IF EXISTS `area_personal`;
CREATE TABLE `area_personal` (
  `id_area_personal` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `status` varchar(5) NOT NULL,
  PRIMARY KEY (`id_area_personal`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Dumping data for table area_personal
#
LOCK TABLES `area_personal` WRITE;
/*!40000 ALTER TABLE `area_personal` DISABLE KEYS */;

INSERT INTO `area_personal` VALUES (1,'Presidente Comit� Vigilancia','on');
INSERT INTO `area_personal` VALUES (3,'Secretario Comit� Vigilancia','on');
INSERT INTO `area_personal` VALUES (4,'Vocal Comit� Vigilancia','on');
INSERT INTO `area_personal` VALUES (5,'Personal de HealProp','on');
INSERT INTO `area_personal` VALUES (6,'Ninguna Responsabilidad','on');
INSERT INTO `area_personal` VALUES (7,'Otro (Indique en comentarios)','on');
/*!40000 ALTER TABLE `area_personal` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table articulos
#

DROP TABLE IF EXISTS `articulos`;
CREATE TABLE `articulos` (
  `id_articulo` int(10) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(10) NOT NULL,
  `subcategoria_id` int(10) NOT NULL,
  `texto_esp` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `titulo` varchar(300) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `fecha` varchar(100) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `autor` varchar(500) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL DEFAULT '0',
  `imagen` varchar(300) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `url` varchar(500) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL DEFAULT '',
  `tag_title` varchar(150) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `tag_description` varchar(200) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `status` varchar(4) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `texto_eng` text CHARACTER SET utf8 COLLATE utf8_swedish_ci,
  `fecha_real` date DEFAULT NULL,
  PRIMARY KEY (`id_articulo`),
  FULLTEXT KEY `titulo` (`titulo`,`texto_esp`,`texto_eng`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

#
# Dumping data for table articulos
#
LOCK TABLES `articulos` WRITE;
/*!40000 ALTER TABLE `articulos` DISABLE KEYS */;

INSERT INTO `articulos` VALUES (19,16,0,'Aqu� hay algunos consejos y tips para viajar a Cancun que le ayudar�n a planificar mejor su viaje.\r\n\r\nMejor �poca para viajar a Cancun\r\nCanc�n es un destino estupendo para viajar durante cualquier �poca del a�o, aunque seguramente habr� temporadas que te interesen m�s que otras. Entonces� �Cuando viajar a Canc�n? Debes conocer cual es la mejor temporada para viajar a Cancun acorde a tus necesidades.\r\n\r\nDe Diciembre a Marzo: Durante estos meses las tarifas son las m�s altas del a�o en Canc�n, aunque en Febrero puedes encontrar algunas promociones y ofertas.\r\n\r\nDe Abril a Junio: Es de las mejores temporadas para viajar a Cancun, como es temporada media, hay menos visitantes durante estos meses. Esto te ayudar� a encontrar f�cilmente precios razonables si buscas ofertas de vuelos y hoteles.\r\n\r\nDe Julio a Agosto: Es temporada alta en Canc�n, por lo que es mejor hacer una reserva anticipada para no llevarnos luego una sorpresa.\r\n\r\nDe Septiembre a Noviembre: Es temporada de lluvias y huracanes. Normalmente te encontrar�s solo con algunos d�as nublados pero si eso no te supone un problema quiz�s pueda ser una buena opci�n. Adem�s, las tarifas de alojamiento en Canc�n son las m�s bajas durante todo el a�o.\r\n\r\n\r\nConseguir vuelos baratos a Canc�n\r\nSi quieres conseguir mejores precios para volar a Canc�n, te recomendamos que tengas algunas cosas en cuenta y sigas estos consejos para ayudarte a encontrar las mejores tarifas.\r\n\r\n? Podr�s encontrar siempre mejores precios si vuelas entre semana, especialmente de martes a jueves.\r\n\r\n? Trata de embarcar en alg�n vuelo nocturno. Suelen ser m�s baratos y menos concurridos.\r\n\r\n? Intenta evitar los vuelos directos. Muchas veces te saldr� m�s barato el viaje si haces escala y coges otro vuelo hacia tu destino final.\r\n\r\nPor otra parte, utilizar los mejores buscadores de vuelos es esencial si quieres adquirir un billete m�s barato.','Mejores Vacaciones en Cancun','20-10-2017','HCG','','tips-para-viajar-a-cancun','Viajando Inteligentemente a Canc�n','','on','Aqu� hay algunos consejos y tips para viajar a Cancun que le ayudar�n a planificar mejor su viaje.\r\n\r\nMejor �poca para viajar a Cancun\r\nCanc�n es un destino estupendo para viajar durante cualquier �poca del a�o, aunque seguramente habr� temporadas que te interesen m�s que otras. Entonces� �Cuando viajar a Canc�n? Debes conocer cual es la mejor temporada para viajar a Cancun acorde a tus necesidades.\r\n\r\nDe Diciembre a Marzo: Durante estos meses las tarifas son las m�s altas del a�o en Canc�n, aunque en Febrero puedes encontrar algunas promociones y ofertas.\r\n\r\nDe Abril a Junio: Es de las mejores temporadas para viajar a Cancun, como es temporada media, hay menos visitantes durante estos meses. Esto te ayudar� a encontrar f�cilmente precios razonables si buscas ofertas de vuelos y hoteles.\r\n\r\nDe Julio a Agosto: Es temporada alta en Canc�n, por lo que es mejor hacer una reserva anticipada para no llevarnos luego una sorpresa.\r\n\r\nDe Septiembre a Noviembre: Es temporada de lluvias y huracanes. Normalmente te encontrar�s solo con algunos d�as nublados pero si eso no te supone un problema quiz�s pueda ser una buena opci�n. Adem�s, las tarifas de alojamiento en Canc�n son las m�s bajas durante todo el a�o.\r\n\r\n\r\nConseguir vuelos baratos a Canc�n\r\nSi quieres conseguir mejores precios para volar a Canc�n, te recomendamos que tengas algunas cosas en cuenta y sigas estos consejos para ayudarte a encontrar las mejores tarifas.\r\n\r\n? Podr�s encontrar siempre mejores precios si vuelas entre semana, especialmente de martes a jueves.\r\n\r\n? Trata de embarcar en alg�n vuelo nocturno. Suelen ser m�s baratos y menos concurridos.\r\n\r\n? Intenta evitar los vuelos directos. Muchas veces te saldr� m�s barato el viaje si haces escala y coges otro vuelo hacia tu destino final.\r\n\r\nPor otra parte, utilizar los mejores buscadores de vuelos es esencial si quieres adquirir un billete m�s barato.','2017-10-20');
INSERT INTO `articulos` VALUES (22,18,0,'Many of us will avoid tours and excursions simply to maintain a safe distance from potential tourist traps and gimmicks. Of course, some signature tourist stops are found in every travel guide for good reason. I�ve been living in Cancun for nearly a decade, and I�ve seen everything from the smallest, back-alley street market to the parades of tacky group tours milling throughout Cancun. We all love the well-kept-local secrets; but there are a few hallmark sites that can�t be missed. The best tours are informative, entertaining, and give you context that will truly allow these destinations to come to life. Each of the tours listed below are popular amongst visitors, and can be easily booked from the tour desk at your hotel: perfect for a hassle-free day trip during your Cancun vacation. So grab your camera, lace-up your sneakers, strap on your fannypack and and give in to the touristy spirit. \r\n\r\nXcaret\r\n\r\nLocated one hour south of Cancun is this immense nature park in the Riviera Maya. Visitors are invited to snorkel in the Caribbean shores, to partake in Mexican festivities (Mayan ball games; the aerial feats of the papantla performers), and to interact with the regional wildlife. While you�re at Xcaret, make sure to swim through the underground river and visit the butterfly pavilion�my personal favorite activities. \r\n\r\nTulum\r\n\r\nPicture an ancient temple set atop a rocky cliff with the waves of the Caribbean Sea crashing on the beach below. That image is Tulum. This centuries-old Mayan city is a necessary addition to any Cancun vacation photo album. I recommend touring the ruins with a trained guide, who can narrate the scene with the city�s extraordinary historical background. \r\n\r\nWhale Sharks\r\n\r\nEvery May through September, whale sharks migrate to the Mexican Caribbean to feed on the plankton-rich waters. During the summer season, you can book boat tours from Cancun to snorkel alongside them on their journey. From head to tail, these enormous creatures can stretch to 45-feet long, making them the world�s largest fish. Don�t be alarmed by the word shark; whale sharks are peaceful and harmless filter-feeders.\r\n\r\nIsla Contoy\r\n\r\nOnly a handful of tour companies have permits to take visitors to this secluded Caribbean island, allowing only 200 people a day onto Isla Contoy�s pristine white-sand beaches. The abundant wildlife includes stingrays, sea turtles, lobsters, and more than 150 bird species. The island provides a lush tropical haven for animals and visitors alike.','Tours Top Ten of Cancun','07-11-2017','HCG','','tours-top-ten-en-cancun','Tours Top Ten en Cancun','','on','Many of us will avoid tours and excursions simply to maintain a safe distance from potential tourist traps and gimmicks. Of course, some signature tourist stops are found in every travel guide for good reason. I�ve been living in Cancun for nearly a decade, and I�ve seen everything from the smallest, back-alley street market to the parades of tacky group tours milling throughout Cancun. We all love the well-kept-local secrets; but there are a few hallmark sites that can�t be missed. The best tours are informative, entertaining, and give you context that will truly allow these destinations to come to life. Each of the tours listed below are popular amongst visitors, and can be easily booked from the tour desk at your hotel: perfect for a hassle-free day trip during your Cancun vacation. So grab your camera, lace-up your sneakers, strap on your fannypack and and give in to the touristy spirit. \r\n\r\nXcaret\r\n\r\nLocated one hour south of Cancun is this immense nature park in the Riviera Maya. Visitors are invited to snorkel in the Caribbean shores, to partake in Mexican festivities (Mayan ball games; the aerial feats of the papantla performers), and to interact with the regional wildlife. While you�re at Xcaret, make sure to swim through the underground river and visit the butterfly pavilion�my personal favorite activities. \r\n\r\nTulum\r\n\r\nPicture an ancient temple set atop a rocky cliff with the waves of the Caribbean Sea crashing on the beach below. That image is Tulum. This centuries-old Mayan city is a necessary addition to any Cancun vacation photo album. I recommend touring the ruins with a trained guide, who can narrate the scene with the city�s extraordinary historical background. \r\n\r\nWhale Sharks\r\n\r\nEvery May through September, whale sharks migrate to the Mexican Caribbean to feed on the plankton-rich waters. During the summer season, you can book boat tours from Cancun to snorkel alongside them on their journey. From head to tail, these enormous creatures can stretch to 45-feet long, making them the world�s largest fish. Don�t be alarmed by the word shark; whale sharks are peaceful and harmless filter-feeders.\r\n\r\nIsla Contoy\r\n\r\nOnly a handful of tour companies have permits to take visitors to this secluded Caribbean island, allowing only 200 people a day onto Isla Contoy�s pristine white-sand beaches. The abundant wildlife includes stingrays, sea turtles, lobsters, and more than 150 bird species. The island provides a lush tropical haven for animals and visitors alike.','2017-11-07');
INSERT INTO `articulos` VALUES (23,14,0,'La civilizaci�n maya fue una civilizaci�n mesoamericana desarrollada por los pueblos mayas, que destac� en Am�rica1? por su escritura gl�fica, uno de los pocos sistemas de escritura plenamente desarrollados del continente americano precolombino, as� como por su arte, arquitectura y sistemas de matem�tica, astronom�a y ecolog�a.2? Se desarroll� en la regi�n que abarca el sureste de M�xico, correspondiente a los estados de Yucat�n, Campeche, Tabasco, Quintana Roo y la zona oriental de Chiapas, as� como en la mayor�a de Guatemala, Belice, la parte occidental de Honduras y de El Salvador. Esta regi�n se compone de las tierras bajas del norte que abarca la pen�nsula de Yucat�n, las tierras altas de la Sierra Madre que se extiende por el estado mexicano de Chiapas, el sur de Guatemala hasta El Salvador, y las tierras bajas del sur en la llanura litoral del Pac�fico.\r\n\r\nDurante el periodo formativo, antes de 2000 a. C., se inici� el desarrollo de la agricultura y la poblaci�n se hizo sedentaria estableci�ndose en las primeras aldeas. En el per�odo Precl�sico (c. 2000 a. C. hasta 250 d. C.) se desarrollaron las primeras sociedades complejas y se cultivaron los alimentos b�sicos de la dieta maya: el ma�z, los frijoles, las calabazas y los chiles. Las primeras ciudades mayas se desarrollaron en torno a 750 a. C. Alrededor de 500 a. C. estas ciudades pose�an una arquitectura monumental, incluyendo grandes templos con fachadas de estuco. La escritura gl�fica se utiliz� desde el siglo iii a. C. En el Precl�sico Tard�o se desarrollaron grandes ciudades en la Cuenca del Pet�n, y Kaminaljuy� alcanz� prominencia en el altiplano guatemalteco. Desde alrededor de 250 d. C., el per�odo cl�sico se define en gran medida por el levantamiento de monumentos esculpidos empleando las fechas de Cuenta Larga. En este per�odo se desarroll� un gran n�mero de ciudades-estado vinculadas entre si por una compleja red de comercio. En las tierras bajas mayas surgieron dos grandes poderes rivales, Tikal y Calakmul. Se vio tambi�n la intervenci�n extranjera en la pol�tica din�stica maya de la ciudad de Teotihuacan del centro de M�xico. En el siglo ix, se produjo un colapso pol�tico general en la regi�n central maya, que origin� guerras internas, el abandono de las ciudades, y un desplazamiento poblacional hacia el norte. Durante el per�odo Poscl�sico surgi� Chich�n Itz� en el norte, y se produjo la expansi�n del reino quich� en el altiplano de Guatemala. En el siglo xvi el Imperio espa�ol coloniz� la regi�n mesoamericana, y tras una larga serie de campa�as militares la �ltima ciudad maya cay� en 1697.','Tours y Cultura Maya','03-11-2017','HCG','','tours-y-cultura-maya','Tours y Cultura Maya','','on','La civilizaci�n maya fue una civilizaci�n mesoamericana desarrollada por los pueblos mayas, que destac� en Am�rica1? por su escritura gl�fica, uno de los pocos sistemas de escritura plenamente desarrollados del continente americano precolombino, as� como por su arte, arquitectura y sistemas de matem�tica, astronom�a y ecolog�a.2? Se desarroll� en la regi�n que abarca el sureste de M�xico, correspondiente a los estados de Yucat�n, Campeche, Tabasco, Quintana Roo y la zona oriental de Chiapas, as� como en la mayor�a de Guatemala, Belice, la parte occidental de Honduras y de El Salvador. Esta regi�n se compone de las tierras bajas del norte que abarca la pen�nsula de Yucat�n, las tierras altas de la Sierra Madre que se extiende por el estado mexicano de Chiapas, el sur de Guatemala hasta El Salvador, y las tierras bajas del sur en la llanura litoral del Pac�fico.\r\n\r\nDurante el periodo formativo, antes de 2000 a. C., se inici� el desarrollo de la agricultura y la poblaci�n se hizo sedentaria estableci�ndose en las primeras aldeas. En el per�odo Precl�sico (c. 2000 a. C. hasta 250 d. C.) se desarrollaron las primeras sociedades complejas y se cultivaron los alimentos b�sicos de la dieta maya: el ma�z, los frijoles, las calabazas y los chiles. Las primeras ciudades mayas se desarrollaron en torno a 750 a. C. Alrededor de 500 a. C. estas ciudades pose�an una arquitectura monumental, incluyendo grandes templos con fachadas de estuco. La escritura gl�fica se utiliz� desde el siglo iii a. C. En el Precl�sico Tard�o se desarrollaron grandes ciudades en la Cuenca del Pet�n, y Kaminaljuy� alcanz� prominencia en el altiplano guatemalteco. Desde alrededor de 250 d. C., el per�odo cl�sico se define en gran medida por el levantamiento de monumentos esculpidos empleando las fechas de Cuenta Larga. En este per�odo se desarroll� un gran n�mero de ciudades-estado vinculadas entre si por una compleja red de comercio. En las tierras bajas mayas surgieron dos grandes poderes rivales, Tikal y Calakmul. Se vio tambi�n la intervenci�n extranjera en la pol�tica din�stica maya de la ciudad de Teotihuacan del centro de M�xico. En el siglo ix, se produjo un colapso pol�tico general en la regi�n central maya, que origin� guerras internas, el abandono de las ciudades, y un desplazamiento poblacional hacia el norte. Durante el per�odo Poscl�sico surgi� Chich�n Itz� en el norte, y se produjo la expansi�n del reino quich� en el altiplano de Guatemala. En el siglo xvi el Imperio espa�ol coloniz� la regi�n mesoamericana, y tras una larga serie de campa�as militares la �ltima ciudad maya cay� en 1697.','2017-11-03');
INSERT INTO `articulos` VALUES (24,19,0,'El Rose Bar es un bar tipo antro con m�sica electr�nica moderna. Este club se encuentra dentro del hotel Me, no es muy grande pero tiene mucho ambiente. Tiene dos secciones una abierta y otra cerrada, a veces abren una o la otra. Este es un club elegante y es buen lugar para comenzar la fiesta ya que no cierran muy tarde.\r\n\r\nSantanera es para nuestro gusto el mejor bar en Playa del Carmen. Tiene varios pisos, cada uno diferente, y la m�sica es tipo electr�nica. Su ambiente es muy internacional como una mezcla entre latino y europeo.\r\n\r\nPlaya Mamitas es una de las m�s famosas de la regi�n no solo por su belleza sino por su ambiente. En esta playa encontrar�s muchas personas disfrutando del sol, la m�sica tipo electr�nica y restaurantes y clubes de playa. Para socializar es sin duda una de las mejores. Playa Mamitas tambi�n es famosa por sus festivales y eventos musicales con los mejores DJs de todo el mundo como Tiesto.\r\n\r\nCoco Bongo es un club �nico en el mundo ya que se vuelve un espect�culo ver los shows en vivo toda la noche. Este es un lugar que se convierte en una s�per fiesta, la m�sica es moderna internacional y su ambiente tambi�n.\r\n ','Discotecas de moda en Playa del Carmen','21-11-2017','HCG','','antros-en-playa-del-carmen','Antros en Playa del Carmen','','on','El Rose Bar es un bar tipo antro con m�sica electr�nica moderna. Este club se encuentra dentro del hotel Me, no es muy grande pero tiene mucho ambiente. Tiene dos secciones una abierta y otra cerrada, a veces abren una o la otra. Este es un club elegante y es buen lugar para comenzar la fiesta ya que no cierran muy tarde.\r\n\r\nSantanera es para nuestro gusto el mejor bar en Playa del Carmen. Tiene varios pisos, cada uno diferente, y la m�sica es tipo electr�nica. Su ambiente es muy internacional como una mezcla entre latino y europeo.\r\n\r\nPlaya Mamitas es una de las m�s famosas de la regi�n no solo por su belleza sino por su ambiente. En esta playa encontrar�s muchas personas disfrutando del sol, la m�sica tipo electr�nica y restaurantes y clubes de playa. Para socializar es sin duda una de las mejores. Playa Mamitas tambi�n es famosa por sus festivales y eventos musicales con los mejores DJs de todo el mundo como Tiesto.\r\n\r\nCoco Bongo es un club �nico en el mundo ya que se vuelve un espect�culo ver los shows en vivo toda la noche. Este es un lugar que se convierte en una s�per fiesta, la m�sica es moderna internacional y su ambiente tambi�n.','2017-11-06');
INSERT INTO `articulos` VALUES (25,14,0,'Tulum es una ciudad del estado de Quintana Roo, al sureste de M�xico, en la costa del Mar Caribe, parte de la zona tur�stica llamada Riviera Maya, que se caracteriza por estar amurallada y ser expositora principal de la cultura maya.\r\n\r\nTulum fu� una ciudad de la antigua cultura Maya y fue concebida como un mundo perfecto y racional que servir�a de morada tanto para Dioses como para humanos. Est� hecho en funci�n de los puntos cardinales y se encuentra dentro del Parque Nacional Tulum, el cual abarca una extensi�n de 664 hect�reas.\r\n\r\nSon muchos los atractivos que presenta esta ciudad, pero una de las cosas que debes hacer es visitar las ruinas en Tulum, y as� acercarte m�s a su historia y cultura.\r\n\r\nLas Ruinas de Tulum\r\n\r\nUna de las atracciones tur�sticas m�s interesantes en M�xico son las Ruinas de Tulum. \r\n\r\nAparte de la historia presente en las Ruinas de Tulum, estas resultan impresionantes para sus visitantes. Es posible ba�arse en las playas cercanas, comer y caminar para descubrir toda la belleza de este lugar. Desde las ruinas se puede observar el color del mar y tambi�n tienes la opci�n de pasear en bicicleta para ir a alguna de las playas.','Que hacer estas Vacaciones en Tulum?','14-11-2017','HCG','','vacaciones-en-tulum','Vacaciones en Tulum','','on','Tulum es una ciudad del estado de Quintana Roo, al sureste de M�xico, en la costa del Mar Caribe, parte de la zona tur�stica llamada Riviera Maya, que se caracteriza por estar amurallada y ser expositora principal de la cultura maya.\r\n\r\nTulum fu� una ciudad de la antigua cultura Maya y fue concebida como un mundo perfecto y racional que servir�a de morada tanto para Dioses como para humanos. Est� hecho en funci�n de los puntos cardinales y se encuentra dentro del Parque Nacional Tulum, el cual abarca una extensi�n de 664 hect�reas.\r\n\r\nSon muchos los atractivos que presenta esta ciudad, pero una de las cosas que debes hacer es visitar las ruinas en Tulum, y as� acercarte m�s a su historia y cultura.\r\n\r\nLas Ruinas de Tulum\r\n\r\nUna de las atracciones tur�sticas m�s interesantes en M�xico son las Ruinas de Tulum. \r\n\r\nAparte de la historia presente en las Ruinas de Tulum, estas resultan impresionantes para sus visitantes. Es posible ba�arse en las playas cercanas, comer y caminar para descubrir toda la belleza de este lugar. Desde las ruinas se puede observar el color del mar y tambi�n tienes la opci�n de pasear en bicicleta para ir a alguna de las playas.','2017-11-14');
INSERT INTO `articulos` VALUES (26,16,0,'Pocos paisajes de M�xico atrapan tanto como el de Tulum: las misteriosas pir�mides mayas rodeadas de vegetaci�n pura, la playas de arenas bien blancas y el mar turquesa casi irreal de fondo convierten a este peque�o poblado de la Riviera Maya en una invitaci�n irresistible. Tulum es un lugar que hay que visitar.\r\n\r\nLuego de casi una semana disfrutando de Playa del Carmen, llegu� a Tulum atra�da por esas mismas postales. No me llev� mucho tiempo saber que una excursi�n de d�a completo no ser�a suficiente, y me dispuse a buscar una caba�a. Para alguien que llega sin mucha informaci�n, la tarea de encontrar d�nde hospedarse por unos d�as puede resultar un tanto enga�osa: el pueblo de Tulum se encuentra a ambos lados de la carretera, y no se asemeja a esa idea perfecta de playas, palmeras y ruinas. Pero no hay que desesperar. Si bien es cierto que es en el centro donde se encuentran las opciones de alojamiento m�s econ�micas, existe tambi�n una zona hotelera que se despliega todo a lo largo de la costa, muy cerca del Parque Nacional, y rodeada en un ambiente natural �nico. All� fue donde encontr� un bungalow con una terraza directo al infinito, y me fui a dormir con el ruido de las olas y el viento de mar en las mejillas.\r\n\r\nA la ma�ana siguiente part� hacia las ruinas. Tulum, en lengua nativa, significa �muralla�, pero los antiguos habitantes sol�an llamar a este lugar Zama, que significa �amanecer�. No es nada extra�o: desde el acantilado en donde se ubica parte de la ciudadela las luces del alba ti�en el horizonte de unos tonos dram�ticos capaces de hipnotizar a cualquiera. Adem�s de las fotos �nicas, visitar las ruinas temprano en la ma�ana es una buena opci�n para explorar con tranquilidad el yacimiento arqueol�gico de Tulum, evitando los grandes grupos de turistas.\r\n\r\nDe la antigua ciudad maya, construida entre los a�os 1200 y 1450 d.c., s�lo es posible visitar lo que fue su n�cleo ceremonial y pol�tico, es decir, lo que est� dentro de las murallas. Aunque el estado de conservaci�n de todos los edificios es excelente, ninguno llama tanto la atenci�n como �El Castillo�, reconocible en todas las postales. Qued� sorprendida al saber que esta construcci�n era utilizada por los mayas como un faro, y no como un centro ceremonial.\r\n\r\nDado que muy cerca de las costas de Tulum se encuentra el segundo arrecife de coral m�s grande del mundo, los mayas se val�an de un sistema de antorchas encendidas en su torre para guiar a sus embarcaciones hasta la costa. Hoy, ajenas al esplendor de aquella civilizaci�n, las iguanas reinan el paisaje y toman sol ignorando a los turistas. Entre las construcciones que m�s llamaron mi atenci�n, recuerdo �El Templo del Dios Descendente�, en cuya puerta se ve tallada una figura de la deidad bajando desde el cielo, y �El Templo de los frescos�, en donde se conserva una gran cantidad de pinturas murales.\r\n\r\nTermin� la visita, como casi todos esos d�as, contemplando las ruinas desde la playa contigua. La perspectiva de la ciudad prehisp�nica desde el mar, con el agua cristalina hasta el cuello, es una imagen de esas que uno no olvida, por m�s que pase el tiempo.','Explora Tulum, Discover Xel Ha','21-11-2017','HCG','','explora-tulum-discover-xel-ha','Explorando Tulum y Xel Ha','','on','Pocos paisajes de M�xico atrapan tanto como el de Tulum: las misteriosas pir�mides mayas rodeadas de vegetaci�n pura, la playas de arenas bien blancas y el mar turquesa casi irreal de fondo convierten a este peque�o poblado de la Riviera Maya en una invitaci�n irresistible. Tulum es un lugar que hay que visitar.\r\n\r\nLuego de casi una semana disfrutando de Playa del Carmen, llegu� a Tulum atra�da por esas mismas postales. No me llev� mucho tiempo saber que una excursi�n de d�a completo no ser�a suficiente, y me dispuse a buscar una caba�a. Para alguien que llega sin mucha informaci�n, la tarea de encontrar d�nde hospedarse por unos d�as puede resultar un tanto enga�osa: el pueblo de Tulum se encuentra a ambos lados de la carretera, y no se asemeja a esa idea perfecta de playas, palmeras y ruinas. Pero no hay que desesperar. Si bien es cierto que es en el centro donde se encuentran las opciones de alojamiento m�s econ�micas, existe tambi�n una zona hotelera que se despliega todo a lo largo de la costa, muy cerca del Parque Nacional, y rodeada en un ambiente natural �nico. All� fue donde encontr� un bungalow con una terraza directo al infinito, y me fui a dormir con el ruido de las olas y el viento de mar en las mejillas.\r\n\r\nA la ma�ana siguiente part� hacia las ruinas. Tulum, en lengua nativa, significa �muralla�, pero los antiguos habitantes sol�an llamar a este lugar Zama, que significa �amanecer�. No es nada extra�o: desde el acantilado en donde se ubica parte de la ciudadela las luces del alba ti�en el horizonte de unos tonos dram�ticos capaces de hipnotizar a cualquiera. Adem�s de las fotos �nicas, visitar las ruinas temprano en la ma�ana es una buena opci�n para explorar con tranquilidad el yacimiento arqueol�gico de Tulum, evitando los grandes grupos de turistas.\r\n\r\nDe la antigua ciudad maya, construida entre los a�os 1200 y 1450 d.c., s�lo es posible visitar lo que fue su n�cleo ceremonial y pol�tico, es decir, lo que est� dentro de las murallas. Aunque el estado de conservaci�n de todos los edificios es excelente, ninguno llama tanto la atenci�n como �El Castillo�, reconocible en todas las postales. Qued� sorprendida al saber que esta construcci�n era utilizada por los mayas como un faro, y no como un centro ceremonial.\r\n\r\nDado que muy cerca de las costas de Tulum se encuentra el segundo arrecife de coral m�s grande del mundo, los mayas se val�an de un sistema de antorchas encendidas en su torre para guiar a sus embarcaciones hasta la costa. Hoy, ajenas al esplendor de aquella civilizaci�n, las iguanas reinan el paisaje y toman sol ignorando a los turistas. Entre las construcciones que m�s llamaron mi atenci�n, recuerdo �El Templo del Dios Descendente�, en cuya puerta se ve tallada una figura de la deidad bajando desde el cielo, y �El Templo de los frescos�, en donde se conserva una gran cantidad de pinturas murales.\r\n\r\nTermin� la visita, como casi todos esos d�as, contemplando las ruinas desde la playa contigua. La perspectiva de la ciudad prehisp�nica desde el mar, con el agua cristalina hasta el cuello, es una imagen de esas que uno no olvida, por m�s que pase el tiempo.','2017-11-13');
INSERT INTO `articulos` VALUES (28,16,0,' Playa del Carmen tiene muchas opciones para pasarla bien, as� que si planeas pasar muy pocos d�as, regresa pronto hasta dos o tres veces m�s bastan para conocer los atractivos tur�sticos que la ciudad cosmopolita ofrece.\r\n\r\n- Las vacaciones de verano e invierno pertenecen a la temporada alta de Playa del Carmen y todo Quintana Roo, si eliges tours y hospedaje lejos de estos meses puedes obtener precios accesibles.\r\n\r\n- En verano el calor es a�n m�s intenso as� como la humedad, si no est�s acostumbrado elige las vacaciones de invierno, las ma�anas son frescas y las noches tambi�n, prep�rate con un abrigo sencillo.\r\n\r\n- Para llegar a Playa del Carmen desde Canc�n, te resulta muy econ�mico transportarse en una van, en caso de no tener tantas maletas. En este tipo de transportes tambi�n se puede llegar a los atractivos como Xel-H� o Xplor si deseas pasear a tu propio ritmo y ahorrar dinero.\r\n\r\n- Si te hospedar�s en un hotel todo incluido elige uno que est� cerca de la Quinta Avenida para llegar a pie, aunque lo m�s recomendable es no elegir este tipo de hoteles, ya que afuera hay demasiados lugares por ver y visitar.\r\n\r\n- Prep�rate para caminar con un buen calzado, camisetas con mangas cortas, lentes de sol y bloqueador solar UV de m�s de 50. Para salir en la noche basta con ropa casual y sandalias. Para el antro y restaurantes lujosos, algo elegante pero sencillo y no tan pretencioso.\r\n\r\n- Ir a Cozumel es una gran recomendaci�n, as� como salir de playa para tomar diferentes aventuras, Sian ka� an y Tulum tambi�n son muy recomendables. Hay lugares que brindan informaci�n para tomar los tours, tambi�n puedes hacerlo por tu cuenta en taxis o combis r�pidas.\r\n\r\n- Si sales a excursiones no olvides llevar repelente de mosquitos.\r\n\r\n- Para comer hay de todos los sabores y todo tipo de bolsillo, pero si la cuesti�n es ahorrar hay establecimientos de comida r�pida. Adem�s te recomiendo buscar lugares en las calles aleda�as a la Quinta Avenida y dar una vuelta por la avenida Ju�rez, hay establecimientos de comida local muy buena y te ahorrar�s unos pesos.\r\n\r\n- Verifica que en los restaurantes tu cuenta no incluya propina.\r\n\r\n- Al salir a comprar, las primeras tiendas de la Quinta Avenida tienen los precios m�s altos, si caminas un poco m�s notar�s que ofrecen las cosas por unos pesos menos. Si es posible regatea. Los precios est�n tasados en d�lares y euros.\r\n\r\n- Para cambiar dinero no hay nada mejor que los bancos.','Vacacionando en Playa del Carmen','21-11-2017','HCG','','vacacionando-en-playa-del-carmen','Vacacionando en Playa del Carmen','','on',' Playa del Carmen tiene muchas opciones para pasarla bien, as� que si planeas pasar muy pocos d�as, regresa pronto hasta dos o tres veces m�s bastan para conocer los atractivos tur�sticos que la ciudad cosmopolita ofrece.\r\n\r\n- Las vacaciones de verano e invierno pertenecen a la temporada alta de Playa del Carmen y todo Quintana Roo, si eliges tours y hospedaje lejos de estos meses puedes obtener precios accesibles.\r\n\r\n- En verano el calor es a�n m�s intenso as� como la humedad, si no est�s acostumbrado elige las vacaciones de invierno, las ma�anas son frescas y las noches tambi�n, prep�rate con un abrigo sencillo.\r\n\r\n- Para llegar a Playa del Carmen desde Canc�n, te resulta muy econ�mico transportarse en una van, en caso de no tener tantas maletas. En este tipo de transportes tambi�n se puede llegar a los atractivos como Xel-H� o Xplor si deseas pasear a tu propio ritmo y ahorrar dinero.\r\n\r\n- Si te hospedar�s en un hotel todo incluido elige uno que est� cerca de la Quinta Avenida para llegar a pie, aunque lo m�s recomendable es no elegir este tipo de hoteles, ya que afuera hay demasiados lugares por ver y visitar.\r\n\r\n- Prep�rate para caminar con un buen calzado, camisetas con mangas cortas, lentes de sol y bloqueador solar UV de m�s de 50. Para salir en la noche basta con ropa casual y sandalias. Para el antro y restaurantes lujosos, algo elegante pero sencillo y no tan pretencioso.\r\n\r\n- Ir a Cozumel es una gran recomendaci�n, as� como salir de playa para tomar diferentes aventuras, Sian ka� an y Tulum tambi�n son muy recomendables. Hay lugares que brindan informaci�n para tomar los tours, tambi�n puedes hacerlo por tu cuenta en taxis o combis r�pidas.\r\n\r\n- Si sales a excursiones no olvides llevar repelente de mosquitos.\r\n\r\n- Para comer hay de todos los sabores y todo tipo de bolsillo, pero si la cuesti�n es ahorrar hay establecimientos de comida r�pida. Adem�s te recomiendo buscar lugares en las calles aleda�as a la Quinta Avenida y dar una vuelta por la avenida Ju�rez, hay establecimientos de comida local muy buena y te ahorrar�s unos pesos.\r\n\r\n- Verifica que en los restaurantes tu cuenta no incluya propina.\r\n\r\n- Al salir a comprar, las primeras tiendas de la Quinta Avenida tienen los precios m�s altos, si caminas un poco m�s notar�s que ofrecen las cosas por unos pesos menos. Si es posible regatea. Los precios est�n tasados en d�lares y euros.\r\n\r\n- Para cambiar dinero no hay nada mejor que los bancos.','2017-11-21');
INSERT INTO `articulos` VALUES (31,19,0,'<strong>Ir a Canc�n y no ir de antro es como no haber ido a Canc�n. En la zona hotelera hay muchos clubes a los cuales puedes ir, pero �cuales son los mejores? �cuanto cuestan? �como debo ir vestido? Son preguntas que te puedes hacer y que necesitas contestarte antes de llegar a Canc�n.</strong>\r\n\r\n<i>Ahora te dir� algunos detalles de los antros m�s importantes de Canc�n:</i>\r\n\r\n <strong>Coco Bongo.</strong> Hay uno en Playa del Carmen y otro en la zona hotelera de Canc�n. El precio regular es entre $65 y $75 dolares, lo que son casi los $1000 pesos. No s� si todav�a, y no encontr� informaci�n en internet, pero cuando fui a Canc�n en el avi�n de VivaAerobus ven�an varios paquetes, recuerdo que ven�a la entrada a Coco Bongo por unos $800 pesos. Si viajas por viva te recomiendo que revises el bolet�n de las ofertas que hay en el asiento de enfrente, comp�ralo con los precios que revisaste y si te sale conviene es una buena forma de ahorrar y saber que tendr�s algo seguro. La verdad el show de Coco Bongo est� muy divertido, aunque si est�s en zona regular no te lo pasar�s tan bien, recuerdo que estaba a reventar el lugar y ol�a extra�o, como a una mezcla de cerveza y orines (que asco, lo s�), pero en la zona VIP es completamente diferente, y supongo que tambi�n tiene que ver la temporada, ya que fui en semana santa y estaba todo mu saturado.\r\n\r\n<span>Mandala Beach.</span> El concepto de este antro es genial, est� junto a la playa y gran parte del antro est� sobre la arena. Hay una alberca y a veces hacen pool party. Tambi�n hacen bikini contest y cosas por el estilo, la verdad me encant� la tem�tica de este antro. Lo malo es que el acceso a playa y alberca solo es los martes, pero los dem�s d�as es un antro normal. Si vas de d�a cuesta $22 dolares y de noche $60, la verdad vale mucho la pena.\r\n\r\n<strong>La Vaquita.</strong> Un club muy divertido. La tem�tica es que la gente haga el rid�culo, hay pelucas y cosas as� para ponerte y tomarte fotos. De vez en cuando sale una botarga de vaca haciendo cosas raras. Es bastante mi estilo de lugar. La entrada cuesta $30 dolares.\r\n','Antros y Discotecas en Cancun','21-11-2017','HCG','','antros-bares-discotecas-cancun','Antros en Canc�n','','on','<strong>Ir a Canc�n y no ir de antro es como no haber ido a Canc�n. En la zona hotelera hay muchos clubes a los cuales puedes ir, pero �cuales son los mejores? �cuanto cuestan? �como debo ir vestido? Son preguntas que te puedes hacer y que necesitas contestarte antes de llegar a Canc�n.</strong>\r\n\r\n<i>Ahora te dir� algunos detalles de los antros m�s importantes de Canc�n:</i>\r\n\r\n <strong>Coco Bongo.</strong> Hay uno en Playa del Carmen y otro en la zona hotelera de Canc�n. El precio regular es entre $65 y $75 dolares, lo que son casi los $1000 pesos. No s� si todav�a, y no encontr� informaci�n en internet, pero cuando fui a Canc�n en el avi�n de VivaAerobus ven�an varios paquetes, recuerdo que ven�a la entrada a Coco Bongo por unos $800 pesos. Si viajas por viva te recomiendo que revises el bolet�n de las ofertas que hay en el asiento de enfrente, comp�ralo con los precios que revisaste y si te sale conviene es una buena forma de ahorrar y saber que tendr�s algo seguro. La verdad el show de Coco Bongo est� muy divertido, aunque si est�s en zona regular no te lo pasar�s tan bien, recuerdo que estaba a reventar el lugar y ol�a extra�o, como a una mezcla de cerveza y orines (que asco, lo s�), pero en la zona VIP es completamente diferente, y supongo que tambi�n tiene que ver la temporada, ya que fui en semana santa y estaba todo mu saturado.\r\n\r\n<span>Mandala Beach.</span> El concepto de este antro es genial, est� junto a la playa y gran parte del antro est� sobre la arena. Hay una alberca y a veces hacen pool party. Tambi�n hacen bikini contest y cosas por el estilo, la verdad me encant� la tem�tica de este antro. Lo malo es que el acceso a playa y alberca solo es los martes, pero los dem�s d�as es un antro normal. Si vas de d�a cuesta $22 dolares y de noche $60, la verdad vale mucho la pena.\r\n\r\n<strong>La Vaquita.</strong> Un club muy divertido. La tem�tica es que la gente haga el rid�culo, hay pelucas y cosas as� para ponerte y tomarte fotos. De vez en cuando sale una botarga de vaca haciendo cosas raras. E bastante mi estilo de lugar. La entrada cuesta $30 dolares.\r\n','2017-11-21');
INSERT INTO `articulos` VALUES (32,13,0,'Dentro del mosaico infinito de la comida mexicana destaca por la diversidad de sus condimentos y por la enorme creatividad empleada en la preparaci�n de los platillos, la comida del sureste mexicano, marcada por la herencia que le viene de tiempo atr�s: la maya y la espa�ola.\r\n\r\nEn el caso de Quintana Roo esta herencia se ha acentuado por el surgimiento de n�cleos de poblaci�n provenientes de otros lugares, as� como por la influencia reciente del corredor tur�stico que se extiende sobre buena parte de la costa del estado. No obstante, las tradiciones se mantienen ah� donde el pasado encuentra el sustento de la tradici�n, y as� los platillos propios de la pen�nsula todav�a pueden disfrutarse en muchos lugares.\r\n\r\nPara los quintanarroenses es familiar el sabor que le dan a la comida regional el chilmole, el pipi�n, conocido como oni sikil, y el papatz tzul. Los platillos como el cocido, el relleno blanco, el relleno negro, el chocolomo y la cochinita pibil, por referirnos s�lo a algunas riquezas, requieren de conocimiento y gran habilidad en su preparaci�n. \r\n\r\nEntre los antojos que con toda seguridad se pueden saborear en Quintana Roo est�n los panuchos, salbutes, tamales, empanadas y garnachas, que hacen las delicias de la gente.\r\n\r\nPor otra parte, el crecimiento de las corrientes tur�sticas en los �ltimos a�os dio lugar a una comida marcadamente cosmopolita que incorpora sobre todo los frutos del mar y ciertos sabores con reminiscencia ind�gena.\r\n\r\nAntojitos:\r\n\r\nEnchiladas quintanarroenses: Las tortillas ligeramente fritas en  manteca de puerco se pasan por una salsa hecha con chiles anchos y pasillas, almendras y cacahuates, todo molido con caldo de pollo. Se rellenan de pollo deshebrado y se adornan con cebolla y queso.\r\n\r\nEmpanadas de caz�n: Son de masa de ma�z rellenas con el caz�n cocido y desmenuzado guisado con cebolla, jitomate y epazote, y luego fritas.\r\n\r\nEn algunos lugares hacen tamales de elote con chile habanero y tocino, y otros de chipil�n en hoja de pl�tano.','Gastronomia en Yucat�n','21-11-2017','HCG','','comida-regional-en-yucatan','Gastronomia en Yucat�n','','on','Dentro del mosaico infinito de la comida mexicana destaca por la diversidad de sus condimentos y por la enorme creatividad empleada en la preparaci�n de los platillos, la comida del sureste mexicano, marcada por la herencia que le viene de tiempo atr�s: la maya y la espa�ola.\r\n\r\nEn el caso de Quintana Roo esta herencia se ha acentuado por el surgimiento de n�cleos de poblaci�n provenientes de otros lugares, as� como por la influencia reciente del corredor tur�stico que se extiende sobre buena parte de la costa del estado. No obstante, las tradiciones se mantienen ah� donde el pasado encuentra el sustento de la tradici�n, y as� los platillos propios de la pen�nsula todav�a pueden disfrutarse en muchos lugares.\r\n\r\nPara los quintanarroenses es familiar el sabor que le dan a la comida regional el chilmole, el pipi�n, conocido como oni sikil, y el papatz tzul. Los platillos como el cocido, el relleno blanco, el relleno negro, el chocolomo y la cochinita pibil, por referirnos s�lo a algunas riquezas, requieren de conocimiento y gran habilidad en su preparaci�n. \r\n\r\nEntre los antojos que con toda seguridad se pueden saborear en Quintana Roo est�n los panuchos, salbutes, tamales, empanadas y garnachas, que hacen las delicias de la gente.\r\n\r\nPor otra parte, el crecimiento de las corrientes tur�sticas en los �ltimos a�os dio lugar a una comida marcadamente cosmopolita que incorpora sobre todo los frutos del mar y ciertos sabores con reminiscencia ind�gena.\r\n\r\nAntojitos:\r\n\r\nEnchiladas quintanarroenses: Las tortillas ligeramente fritas en  manteca de puerco se pasan por una salsa hecha con chiles anchos y pasillas, almendras y cacahuates, todo molido con caldo de pollo. Se rellenan de pollo deshebrado y se adornan con cebolla y queso.\r\n\r\nEmpanadas de caz�n: Son de masa de ma�z rellenas con el caz�n cocido y desmenuzado guisado con cebolla, jitomate y epazote, y luego fritas.\r\n\r\nEn algunos lugares hacen tamales de elote con chile habanero y tocino, y otros de chipil�n en hoja de pl�tano.','2017-11-22');
/*!40000 ALTER TABLE `articulos` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table cart_descripcion
#

DROP TABLE IF EXISTS `cart_descripcion`;
CREATE TABLE `cart_descripcion` (
  `iddescripcion` int(11) NOT NULL AUTO_INCREMENT,
  `idventa` int(11) NOT NULL DEFAULT '0',
  `idproducto` int(11) NOT NULL DEFAULT '0',
  `idplan` int(11) NOT NULL DEFAULT '0',
  `servicio` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `fecha` datetime NOT NULL,
  `adultos` int(10) NOT NULL DEFAULT '0',
  `menores` int(10) NOT NULL DEFAULT '0',
  `totaladulto` int(10) NOT NULL DEFAULT '0',
  `totalmenores` int(10) NOT NULL DEFAULT '0',
  `totalclientes` int(10) NOT NULL DEFAULT '0',
  `precioadulto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `preciomenores` decimal(10,2) NOT NULL DEFAULT '0.00',
  `preciototaladultos` decimal(10,2) NOT NULL DEFAULT '0.00',
  `preciototalmenores` decimal(10,2) NOT NULL DEFAULT '0.00',
  `preciototal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `moneda` varchar(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `isdelete` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`iddescripcion`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table cart_descripcion
#
LOCK TABLES `cart_descripcion` WRITE;
/*!40000 ALTER TABLE `cart_descripcion` DISABLE KEYS */;

INSERT INTO `cart_descripcion` VALUES (1,17,30,0,'tour','2017-04-29',1,0,1,0,1,99,0,99,0,99,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (2,18,8,0,'tour','2017-04-28',2,0,2,0,2,30000,0,60000,0,60000,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (3,19,8,0,'tour','2017-04-28',4,0,4,0,4,30000,0,120000,0,120000,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (4,20,30,0,'tour','2017-04-29',2,1,2,1,3,99,0,198,0,198,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (6,21,30,0,'tour','2017-05-18',2,0,2,0,2,99,0,198,0,198,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (7,22,2,1,'golf','2017-05-12',2,0,2,0,2,130,0,260,0,260,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (8,23,4,1,'golf','2017-05-18',2,0,2,0,2,129,0,258,0,258,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (9,24,5,0,'tour','2017-05-12',1,1,1,1,2,53,0,53,0,53,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (10,24,4,0,'tour','2017-05-12',2,0,2,0,2,60,0,120,0,120,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (11,24,9,0,'tour','2017-05-10',3,1,3,1,4,95,50,285,50,335,'MXN',0,1);
INSERT INTO `cart_descripcion` VALUES (12,25,19,0,'tour','2017-05-16',2,2,2,2,4,109,79,218,158,376,'MXN',0,1);
/*!40000 ALTER TABLE `cart_descripcion` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table cart_venta
#

DROP TABLE IF EXISTS `cart_venta`;
CREATE TABLE `cart_venta` (
  `idventa` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL DEFAULT '0',
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modificacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total` decimal(10,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `venta` tinyint(4) NOT NULL DEFAULT '0',
  `response` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `auth` int(11) NOT NULL DEFAULT '0',
  `cardname` varchar(350) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `cardnumber` varchar(350) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `cardtype` varchar(150) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`idventa`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table cart_venta
#
LOCK TABLES `cart_venta` WRITE;
/*!40000 ALTER TABLE `cart_venta` DISABLE KEYS */;

INSERT INTO `cart_venta` VALUES (17,47,'2017-04-28 17:47:19','2017-04-28 16:47:19',99,1,1,'approved',99,835403,'pedro salas ruiz','1111','//VISA');
INSERT INTO `cart_venta` VALUES (18,48,'2017-04-28 17:53:19','2017-04-28 16:53:19',60000,1,1,'denied',60000,0,'pedro cardenas salas','1111','//VISA');
INSERT INTO `cart_venta` VALUES (19,49,'2017-04-28 18:00:41','2017-04-28 17:00:41',120000,1,1,'error',120000,0,'pedro cardenas salas','','');
INSERT INTO `cart_venta` VALUES (20,0,'2017-04-29 11:44:10','2017-04-29 10:44:10',198,1,0,'0',0,0,'0','0','0');
INSERT INTO `cart_venta` VALUES (21,50,'2017-05-04 16:34:50','2017-05-04 15:34:50',198,1,1,'approved',198,635653,'pedro cardenas loria','1111','//VISA');
INSERT INTO `cart_venta` VALUES (22,0,'2017-05-05 13:11:48','2017-05-05 12:11:48',260,1,0,'0',0,0,'0','0','0');
INSERT INTO `cart_venta` VALUES (23,0,'2017-05-10 15:48:21','2017-05-10 14:48:21',258,1,0,'0',0,0,'0','0','0');
INSERT INTO `cart_venta` VALUES (24,0,'2017-05-10 21:55:22','2017-05-10 20:55:22',508,1,0,'0',0,0,'0','0','0');
INSERT INTO `cart_venta` VALUES (25,51,'2017-05-11 23:05:47','2017-05-11 22:05:47',376,1,1,'0',0,0,'0','0','0');
INSERT INTO `cart_venta` VALUES (26,0,'2017-05-12 18:40:09','2017-05-12 17:40:09',240,1,0,'0',0,0,'0','0','0');
/*!40000 ALTER TABLE `cart_venta` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table categoria_blog
#

DROP TABLE IF EXISTS `categoria_blog`;
CREATE TABLE `categoria_blog` (
  `id_categoria_blog` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL DEFAULT '',
  `status` varchar(4) NOT NULL,
  `tag_title` varchar(255) DEFAULT NULL,
  `tag_description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_categoria_blog`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

#
# Dumping data for table categoria_blog
#
LOCK TABLES `categoria_blog` WRITE;
/*!40000 ALTER TABLE `categoria_blog` DISABLE KEYS */;

INSERT INTO `categoria_blog` VALUES (12,'Vacaciones en Canc�n','on','','','vacaciones-en-cancun-xangos');
INSERT INTO `categoria_blog` VALUES (13,'Comida Regional','on','','','comida-regional-mexico-xangos');
INSERT INTO `categoria_blog` VALUES (14,'Pir�mides y Cultura ','on','','','piramides-cultura-cancun-xangos');
INSERT INTO `categoria_blog` VALUES (16,'Diversi�n Extrema ','on','','','diversion-extrema-cancun-xangos');
INSERT INTO `categoria_blog` VALUES (18,'Golf en Canc�n','on','','','golf-en-cancun-xangos');
INSERT INTO `categoria_blog` VALUES (19,'Bares, Antros y Discotecas','on','Bares, Antros y Discotecas en Canc�n','','bares-antros-discotecas-en-cancun');
/*!40000 ALTER TABLE `categoria_blog` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table categorias
#

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `id_categoria` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_esp` varchar(200) NOT NULL DEFAULT '',
  `status` varchar(10) NOT NULL,
  `nombre_eng` varchar(255) DEFAULT NULL,
  `tag_title_esp` varchar(255) DEFAULT NULL,
  `tag_description_esp` varchar(255) DEFAULT NULL,
  `url_esp` varchar(255) DEFAULT NULL,
  `tag_description_eng` varchar(255) DEFAULT NULL,
  `tag_title_eng` varchar(255) DEFAULT NULL,
  `url_eng` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

#
# Dumping data for table categorias
#
LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;

INSERT INTO `categorias` VALUES (1,'AVENTURA','on','ADVENTURE','TOURS DE AVENTURAS EN CANCUN MEXICO','Aventurate a vivir la experiencia mas ins�lita del Caribe Mexicano','tour-de-aventura-cancun-xangos','ADVENTURE TOURS IN CANCUN MEXICO','ADVENTURE TOURS IN CANCUN MEXICO','adventure-tour-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (7,'VIDA NOCTURNA','on','NIGHTLIFE','DIVERSION Y VIDA NOCTURNA EN CANCUN','En Canc�n la noche no duerme. Siente la energ�a de los imponentes escenarios, antros y discotecas ','antros-cantinas-bares-cancun-xangos','FUN AND NIGHTLIFE IN CANCUN','FUN AND NIGHTLIFE IN CANCUN','fun-night-drinks-music-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (9,'ARQUEOL�GICOS','on','ARCHEOLOGY TOURS','TOURS ARQUEOLOGICOS EN CANCUN MEXICO','Avent�rate a conocer la cultura y arqueolog�a maya','piramides-arqueologia-cancun-xangos','ARCHEOLOGY TOURS IN CANCUN MEXICO','ARCHEOLOGY TOURS IN CANCUN MEXICO','pyramids-culture-mayan-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (13,'PARQUES ECOL�GICOS','on','ECOPARKS','LAS MEJORES TARIFAS EN PARQUES ECOL�GICOS','Parques Tem�ticos en Canc�n al mejor precio','parques-tematicos-cancun-xangos','THE BEST RATES IN ECOLOGICAL PARKS','THE BEST RATES IN ECOLOGICAL PARKS','theme-parks-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (14,'FAMILIA','on','FAMILY','TOURS CON LA FAMILIA EN CANCUN','Un Tour divertido y lleno de emociones con tu familia en Canc�n','vacaciones-familia-tours-cancun-xangos','TOURS WITH FAMILY IN CANCUN','TOURS WITH FAMILY IN CANCUN','fun-vacations-holidays-family-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (16,'SOLO ADULTOS','on','ADULTS ONLY','APASIONANTE LUNA DE MIEL EN CANCUN','El amor, una vida nueva y una inolvidable luna de miel en Cancun','luna-de-miel-playa-cancun-xangos','APPEALING HONEYMOON IN CANCUN','APPEALING HONEYMOON IN CANCUN','honeymoon-beach-fun-love-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (21,'GOLF','on','GOLF','LOS MEJORES CAMPOS DE GOLF EN CANCUN','Hoyo en uno! Juega como un profesional! En Canc�n y Riviera Maya.','golf-mejores-campos-cancun-xangos','THE BEST GOLF COURSES IN CANCUN MEXICO','THE BEST GOLF COURSES IN CANCUN MEXICO','golf-best-courses-at-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (22,'ESN�RKEL','on','SNORKELING','SNORKEL EN ARRECIFE CORALINO Y CENOTES MAYAS EN CANCUN, RIVIERA MAYA Y COZUMEL','Vive una aventura llena de diversi�n explorando nuestros arrecifes coralinos','snorkel-arrecife-cenote-cancun-cozumel-xangos','SNORKEL IN CORAL REEF AND MAYAN CENOTES IN CANCUN, RIVIERA MAYA AND COZUMEL','SNORKEL IN CORAL REEF AND MAYAN CENOTES IN CANCUN, RIVIERA MAYA AND COZUMEL','snorkeling-coral-reef-mayan-cenotes-cancun-cozumel-riviera-maya-xangos');
INSERT INTO `categorias` VALUES (24,'CIRCUITOS','on','ROUNDTRIP','UN TOUR, VARIOS DIAS, EXCITANTES DESTINOS, MAGICOS MOMENTOS','Viaja con nosotros y descubre al explorador que llevas dentro. Esc�pate con nosotros!','circuito-varios-destinos-dias-cancun-riviera-maya-xangos','A TOUR, VARIOUS DAYS, EXCITING DESTINATIONS, MAGIC MOMENTS','A TOUR, VARIOUS DAYS, EXCITING DESTINATIONS, MAGIC MOMENTS','roundtrip-some-days-destinations-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (25,'BUCEO','on','SCUBA DIVING','BUCEA EN LOS MEJORES ARRECIFES CORALINOS DEL MUNDO','Avent�rate a vivir la experiencia y el colorido de nuestra vida marina','buceo-arrecife-coral-cancun-xangos','DIVING IN THE BEST CORAL REEFS IN THE WORLD','DIVING IN THE BEST CORAL REEFS IN THE WORLD','diving-reef-coral-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (27,'FAVORITOS','on','MOST RECOMENDED','NUESTRA SELECCION DE LOS TOURS MAS SOLICITADOS','Los Tours m�s solicitados y recomendados por nuestros clientes','tours-mas-recomendados-cancun-xangos','OUR SELECTION OF THE MOST REQUESTED TOURS','OUR SELECTION OF THE MOST REQUESTED TOURS','tours-most-recommended-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (28,'EXPEDICIONES','on','EXPEDITIONS','EXPEDICIONES TURISTICAS EN CANCUN','Prepara tu Aventura por la selva, mar o aire. Est�s listo?','expediciones-selva-mar-aire-cancun-xangos','TOURIST EXPEDITIONS IN CANCUN','TOURIST EXPEDITIONS IN CANCUN','expeditions-jungle-sea-air-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (29,'ECOTURISMO','on','ECOTOURISM','TOURS ECOL�GICOS EN CANC�N','Tours Ecol�gicos en Canc�n con Xangos Tours','tours-ecologicos-cancun-xangos','ECOLOGICAL TOURS IN CANCUN','ECOLOGICAL TOURS IN CANCUN','tours-ecological-cancun-mexico-xangos');
INSERT INTO `categorias` VALUES (36,'OBSERVACION DE AVES','on','BIRD WATCHING','','','circuito-observacion-aves-xangos-cancun','','','circuit-watching-birds-xangos-cancun');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table clientes
#

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `id_operador` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `taxy` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `direccion` varchar(350) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_operador` varchar(300) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `telefono` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_operador`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table clientes
#
LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;

INSERT INTO `clientes` VALUES (10,'webmkt@hbs-delli.mx','123','asda','asd','asd','asdas');
INSERT INTO `clientes` VALUES (11,'1201hs@gmail.com','notiene2015','cancun','conocido, conocido, conocido, conocido','conocido','999999999');
INSERT INTO `clientes` VALUES (12,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido','conocido','9999999999');
INSERT INTO `clientes` VALUES (13,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido, conocido','conocido','9999999999');
INSERT INTO `clientes` VALUES (14,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido, conocido, conocido','conocido','9999999999');
INSERT INTO `clientes` VALUES (15,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido','conocido','9999999999');
INSERT INTO `clientes` VALUES (16,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido','conocido','9999999999');
INSERT INTO `clientes` VALUES (17,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido','conocido','9999999999');
INSERT INTO `clientes` VALUES (18,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido','conocido','9999999999');
INSERT INTO `clientes` VALUES (19,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido','conocido','9999999999');
INSERT INTO `clientes` VALUES (20,'info@sectornetcancin.com','o303030','asdas','asd','asda','asda');
INSERT INTO `clientes` VALUES (21,'fff','ff','f','f','f','f');
INSERT INTO `clientes` VALUES (22,'1201hs@gmail.com','notiene','cancun','conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido, conocido','conocido','999999999');
INSERT INTO `clientes` VALUES (23,'asda','asda','asdasd','asd','asd','asd');
INSERT INTO `clientes` VALUES (24,'xzcxzczxc','zxczxc','zxczxczxczz','zxczx','zxczc','jjj');
INSERT INTO `clientes` VALUES (25,'asd','asdasd','asda','asd','asd','asd');
INSERT INTO `clientes` VALUES (26,'asd','asd','asd','asd','asd','asdasd');
INSERT INTO `clientes` VALUES (27,'asd','asd','asda','sdasd','dasd','asd');
INSERT INTO `clientes` VALUES (28,'ilianaescamilla@gmail.com','golf2017','golf','Calle Caracol no. 30 SM 27 MZ 10 LT21, Benito Juarez','Benito Juarez','6882266');
INSERT INTO `clientes` VALUES (29,'ffdf','sdfs','sdfsdsdf','dsfsdf','sdf','asasd');
INSERT INTO `clientes` VALUES (30,'1201hs@gmail.com','','','conocido','','');
INSERT INTO `clientes` VALUES (31,'1201hs@gmail.com','','','conocido','','');
INSERT INTO `clientes` VALUES (32,'info@sectornetcancun.com','','','asd','','');
INSERT INTO `clientes` VALUES (33,'info@sectornetcancun.com','','','sdasd','','');
INSERT INTO `clientes` VALUES (34,'thundercat64@kfkfkf.com','','','sdasd','','');
INSERT INTO `clientes` VALUES (35,'thundercat64@kfkfkf.com','','','sdasd','','');
INSERT INTO `clientes` VALUES (36,'thundercat64@kfkfkf.com','','','sdasd','','');
INSERT INTO `clientes` VALUES (37,'info@sectornetcancun.com','','','asd','','');
INSERT INTO `clientes` VALUES (38,'thundercat64@kfkfkf.com','','','sdasd','','');
INSERT INTO `clientes` VALUES (39,'1201hs@gmail.com','','','onocido','','');
INSERT INTO `clientes` VALUES (40,'1201hs@gmail.com','','','onocido','','');
INSERT INTO `clientes` VALUES (41,'1201hs@gmail.com','','','conocido','','');
INSERT INTO `clientes` VALUES (42,'asd','','','asda','','');
INSERT INTO `clientes` VALUES (43,'asd','','','asd','','');
INSERT INTO `clientes` VALUES (44,'f','','','fsd','','');
INSERT INTO `clientes` VALUES (45,'1201hs@gmail.com','','','a','','');
INSERT INTO `clientes` VALUES (46,'alejandro.silva@mitec.com.mx','','','conocido','','');
INSERT INTO `clientes` VALUES (47,'alejandro.silva@mitec.com.mx','','','conocido','','');
INSERT INTO `clientes` VALUES (48,'alejandro.silva@mitec.com.mx','','','conocido','','');
INSERT INTO `clientes` VALUES (49,'alejandro.silva@mitec.com.mx','','','conocido','','');
INSERT INTO `clientes` VALUES (50,'1201hs@gmail.com','','','conocido','','');
INSERT INTO `clientes` VALUES (51,'1201hs@gmail.com','','','conocido','','');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table comentario_articulo
#

DROP TABLE IF EXISTS `comentario_articulo`;
CREATE TABLE `comentario_articulo` (
  `id_comentario_articulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) DEFAULT NULL,
  `web` varchar(400) DEFAULT NULL,
  `comentarios` text,
  `email` varchar(255) DEFAULT NULL,
  `articulo` int(4) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `fecha` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_comentario_articulo`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

#
# Dumping data for table comentario_articulo
#
LOCK TABLES `comentario_articulo` WRITE;
/*!40000 ALTER TABLE `comentario_articulo` DISABLE KEYS */;

INSERT INTO `comentario_articulo` VALUES (10,'gabriel vazque','','ateste','',19,'on','15-10-2016');
/*!40000 ALTER TABLE `comentario_articulo` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table compras
#

DROP TABLE IF EXISTS `compras`;
CREATE TABLE `compras` (
  `id_compra` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(600) NOT NULL DEFAULT '',
  `apellido` varchar(500) NOT NULL DEFAULT '',
  `status` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `comentarios` text,
  `hotel_id` varchar(200) DEFAULT NULL,
  `movil` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `tipo_pago` varchar(50) DEFAULT NULL,
  `fecha` varchar(255) DEFAULT NULL,
  `cupon` varchar(255) DEFAULT NULL,
  `total_compra` varchar(100) DEFAULT NULL,
  `codigo_compra` varchar(255) DEFAULT NULL,
  `total_descuento` varchar(50) DEFAULT NULL,
  `subtotal` varchar(255) DEFAULT NULL,
  `recomendacion_al_cliente` text,
  `a_reportar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_compra`)
) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;

#
# Dumping data for table compras
#
LOCK TABLES `compras` WRITE;
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;

INSERT INTO `compras` VALUES (50,'Hugo ','Cedillo','Pagado','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web.','Amansala Chica','7222609882','9984113518','ppr','31-10-2017','0','1600',NULL,'0','1600','','');
INSERT INTO `compras` VALUES (51,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web.','JW Marriott Canc�n Resort & Spa','7222609882','9984113518','spe','31-10-2017','0','1600',NULL,'0','1600',NULL,NULL);
INSERT INTO `compras` VALUES (52,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (53,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (54,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (56,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (57,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (58,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (59,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (60,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (61,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (62,'jesus gabriel','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','3471',NULL,'183','3654',NULL,NULL);
INSERT INTO `compras` VALUES (63,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web','Arrecifes Suites','7222609882','9984113518','spe','31-10-2017','0','1600',NULL,'0','1600',NULL,NULL);
INSERT INTO `compras` VALUES (64,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web.','Azul Beach','9984113518','7222609882','spe','31-10-2017','Promo15','5003',NULL,'883','5886',NULL,NULL);
INSERT INTO `compras` VALUES (65,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web.','Azul Beach','9984113518','7222609882','spe','31-10-2017','Promo15','5003',NULL,'883','5886',NULL,NULL);
INSERT INTO `compras` VALUES (66,'gabriel vazquez','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','2423',NULL,'128','2550',NULL,NULL);
INSERT INTO `compras` VALUES (67,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Aqualuna Hotel by Xperience Hotels','9984113518','9984113517','spe','31-10-2017','0','1962',NULL,'0','1962',NULL,NULL);
INSERT INTO `compras` VALUES (68,'gabriel vazquez','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','2423',NULL,'128','2550',NULL,NULL);
INSERT INTO `compras` VALUES (69,'gabriel vazquez','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','2423',NULL,'128','2550',NULL,NULL);
INSERT INTO `compras` VALUES (70,'gabriel vazquez','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','2423',NULL,'128','2550',NULL,NULL);
INSERT INTO `compras` VALUES (71,'gabriel vazquez','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','2423',NULL,'128','2550',NULL,NULL);
INSERT INTO `compras` VALUES (72,'gabriel vazquez','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','2423',NULL,'128','2550',NULL,NULL);
INSERT INTO `compras` VALUES (73,'gabriel vazquez','vazquez canche','Pendiente','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','2423',NULL,'128','2550',NULL,NULL);
INSERT INTO `compras` VALUES (74,'gabriel vazquez','vazquez canche','Pagado','info@sectornetcancun.com','test','Crown Paradise Club Canc�n','9983498879','9983498879','spe','31-10-2017','Promo5','2423',NULL,'128','2550','','');
INSERT INTO `compras` VALUES (75,'Hugo ','Cedillo','Pagado','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web.','Acanto Boutique Hotel','7222609882','9984113518','spe','31-10-2017','0','4284',NULL,'0','4284','He le&iacute;do y estoy de acuerdo con los t&eacute;rminos y condiciones de esta reservaci&oacute;n. Entiendo que se me solicitar&aacute; una identificaci&oacute;n oficial con fotograf&iacute;a o la tarjeta de cr&eacute;dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est&aacute; segura en este sitio web.','');
INSERT INTO `compras` VALUES (76,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web.','Bel Air Collection Resort & Spa Xpuha Riviera','7222609882','9984113518','spe','31-10-2017','Promo10','7006',NULL,'778','7784',NULL,NULL);
INSERT INTO `compras` VALUES (77,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Aqualuna Hotel by Xperience Hotels','9984113518','9984113517','spe','31-10-2017','0','1962',NULL,'0','1962',NULL,NULL);
INSERT INTO `compras` VALUES (78,'Hugo ','Cedillo','Pagado','hugocedillo@outlook.com','','Riu  Palace Las Am�ricas','9984113518','9984113517','spe','31-10-2017','Promo15','4559',NULL,'805','5364','','');
INSERT INTO `compras` VALUES (79,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web.\r\n','Bah�a Pr�ncipe','9984113518','9984113518','spe','01-11-2017','Promo15','6900',NULL,'1218','8118',NULL,NULL);
INSERT INTO `compras` VALUES (80,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','','H','9984113518','9984113517','spe','02-11-2017','0','2358',NULL,'0','2358',NULL,NULL);
INSERT INTO `compras` VALUES (81,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Emporio Hotel & Suites Canc�n','9984113518','9984113517','spe','02-11-2017','Promo15','2366',NULL,'417','2783',NULL,NULL);
INSERT INTO `compras` VALUES (82,'jesus gabriel vazquez canche','vazquez canche','Pendiente','gabriel@grupoicon.com','tes','City Express Playa del Carmen','9983498879','43453','spe','02-11-2017','0','0',NULL,'0','0',NULL,NULL);
INSERT INTO `compras` VALUES (83,'Hugo ','Cedillo','Reportar','hugocedillo@outlook.com','','Andrea\'s Tulum','9984113518','9984113517','spe','02-11-2017','0','5346',NULL,'0','5346','','1000');
INSERT INTO `compras` VALUES (84,'Hugo ','Cedillo','Pagado','hugocedillo@outlook.com','','Beach Palace','9984113518','9984113517','spe','04-11-2017','0','1620',NULL,'0','1620','','');
INSERT INTO `compras` VALUES (85,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','He le�do y estoy de acuerdo con los t�rminos y condiciones de esta reservaci�n. Entiendo que se me solicitar� una identificaci�n oficial con fotograf�a o la tarjeta de cr�dito utilizada en esta compra al momento de realizar el tour o actividad. Mi privacidad est� segura en este sitio web.','Aqualuna Hotel by Xperience Hotels','7222609882','7222609882','pp','08-11-2017','Promo15','5462',NULL,'964','6426',NULL,NULL);
INSERT INTO `compras` VALUES (86,'stephanie','cedillo ','Pendiente','stephanie_cg20@hotmail.com','','Park Royal Canc�n','7222041193','7222172057','ppr','12-11-2017','Promo15','2975',NULL,'525','3500',NULL,NULL);
INSERT INTO `compras` VALUES (87,'Hugo ','Cedillo','Pendiente','hugocedillo@outlook.com','','Calypso Hotel Canc�n','9984113518','9984113517','spe','12-11-2017','0','1800',NULL,'0','1800',NULL,NULL);
INSERT INTO `compras` VALUES (88,'Hugo ','Cedillo','Pagado','hugocedillo@outlook.com','','Grand Oasis Palm','9984113518','9984113517','spe','13-11-2017','0','6426',NULL,'0','6426','','');
INSERT INTO `compras` VALUES (92,'Juan carlos ','Baleon torres','Pendiente','knootz78@hotmail.com','','Grand park royal','7223934202','7223934202','ppr','15-11-2017','0','11412',NULL,'0','11412',NULL,NULL);
INSERT INTO `compras` VALUES (93,'Juan carlos','Baleon Torres','Pendiente','baleon78@gmail.com','','Grand park oasis','7223934202','7223934202','ppr','15-11-2017','0','9756',NULL,'0','9756',NULL,NULL);
INSERT INTO `compras` VALUES (94,'Juan Carlos','Bale�n Torres','Pendiente','baleon78@gmail.com','','Grand park royal','7223934202','7223934202','ppr','15-11-2017','0','5886',NULL,'0','5886',NULL,NULL);
INSERT INTO `compras` VALUES (95,'Juan Carlos','Torres','Pendiente','baleon78@gmail.com','','','7223934202','7223934202','ppr','15-11-2017','0','0',NULL,'0','0',NULL,NULL);
INSERT INTO `compras` VALUES (96,'Juan Carlos','Torres','Pendiente','baleon78@gmail.com','','','7223934202','7223934202','ppr','15-11-2017','0','2142',NULL,'0','2142',NULL,NULL);
INSERT INTO `compras` VALUES (97,'Hugo','Cedillo','Pagado','hugocedillo@outlook.com','Campo de Prueba. Mensaje para Xangos Tours','Amansala Chica','9984113518','7222609882','spe','27-12-2017','Promo15','3794',NULL,'670','4464','Llevar efectivo para propinas.','');
INSERT INTO `compras` VALUES (98,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Paradisus Canc�n','9984113518','7222609882','pp','09-01-2018','0','4266',NULL,'0','4266',NULL,NULL);
INSERT INTO `compras` VALUES (99,'gabriel vazquez','vazquez','Pendiente','hsh@skkw.com','test','Acanto Boutique Hotel','245','345345','ppr','09-01-2018','0','6246',NULL,'0','6246',NULL,NULL);
INSERT INTO `compras` VALUES (100,'Hugo ','Cedillo','Pagado','hugocedillo@outlook.com','Comentario de Prueba','Aventuras Club Lagoon','7222609882','9984113518','pp','13-01-2018','Promo15','2040',NULL,'360','2400','','');
INSERT INTO `compras` VALUES (101,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','Solicitamos silla de ruedas.','Holiday Inn Canc�n Arenas','9984113518','7222609882','ppr','13-01-2018','Promo10','7133',NULL,'793','7926',NULL,NULL);
INSERT INTO `compras` VALUES (102,'','','Pendiente','','','','','','','','','',NULL,'','',NULL,NULL);
INSERT INTO `compras` VALUES (103,'','','Pendiente','','','','','','','','','',NULL,'','',NULL,NULL);
INSERT INTO `compras` VALUES (104,'','','Pendiente','','','','','','','','','',NULL,'','',NULL,NULL);
INSERT INTO `compras` VALUES (105,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Holiday Inn Canc�n Arenas','9984113518','7222609882','ppr','13-01-2018','Promo10','5783',NULL,'643','6426',NULL,NULL);
INSERT INTO `compras` VALUES (106,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Acanto Boutique Hotel','9984113518','7222609882','spe','13-01-2018','Promo15','5143',NULL,'908','6050',NULL,NULL);
INSERT INTO `compras` VALUES (107,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Barcel� Maya Palace','9984113518','7222609882','pp','24-01-2018','Promo10','63000',NULL,'7000','70000',NULL,NULL);
INSERT INTO `compras` VALUES (108,'Hugo','Cedillo','Pagado','hugocedillo@outlook.com','','Acanto Boutique Hotel','9984113518','7222609882','spe','28-01-2018','Promo15','122933',NULL,'21694','144627','','');
INSERT INTO `compras` VALUES (109,'Hugo','Cedillo','Pagado','hugocedillo@outlook.com','','Allegro Playacar','9984113518','7222609882','pp','30-01-2018','Promo15','138562',NULL,'24452','163014','','');
INSERT INTO `compras` VALUES (110,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Akumal Beach Resort','9984113518','7222609882','ppr','30-01-2018','','',NULL,'','',NULL,NULL);
INSERT INTO `compras` VALUES (111,'','','Pendiente','','','','','','','','','',NULL,'','',NULL,NULL);
INSERT INTO `compras` VALUES (112,'','','Pendiente','','','','','','','','','',NULL,'','',NULL,NULL);
INSERT INTO `compras` VALUES (113,'','','Pendiente','','','','','','','','','',NULL,'','',NULL,NULL);
INSERT INTO `compras` VALUES (114,'','','Pendiente','','','','','','','','','',NULL,'','',NULL,NULL);
INSERT INTO `compras` VALUES (115,'Hugo','Cedillo','Pendiente','hugocedillo@outlook.com','','Azulik','9984113518','7222609882','ppr','30-01-2018','0','3213',NULL,'0','3213',NULL,NULL);
INSERT INTO `compras` VALUES (116,'Hugo','Cedillo','Reportar','hugocedillo@outlook.com','','Aventuras Club Lagoon','9984113518','7222609882','spe','31-01-2018','Promo15','2372',NULL,'419','2790','','500');
INSERT INTO `compras` VALUES (117,'jesus gabriel ','vazquez canche','Pendiente','info@sectornetcancun.com','test','Akumal Beach Resort','9983498879','9983498879','pp','16-02-2018','0','13392',NULL,'0','13392',NULL,NULL);
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table condo-prop-relacion
#

DROP TABLE IF EXISTS `condo-prop-relacion`;
CREATE TABLE `condo-prop-relacion` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_id` int(11) DEFAULT NULL,
  `type_propiedad` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Dumping data for table condo-prop-relacion
#
LOCK TABLES `condo-prop-relacion` WRITE;
/*!40000 ALTER TABLE `condo-prop-relacion` DISABLE KEYS */;

/*!40000 ALTER TABLE `condo-prop-relacion` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table Copia_de_operadores
#

DROP TABLE IF EXISTS `Copia_de_operadores`;
CREATE TABLE `Copia_de_operadores` (
  `id_operador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `no_taxy` int(11) DEFAULT NULL,
  `modelo_taxy` varchar(255) DEFAULT NULL,
  `tipo_operador` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_operador`)
) ENGINE=MyISAM AUTO_INCREMENT=260 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

#
# Dumping data for table Copia_de_operadores
#
LOCK TABLES `Copia_de_operadores` WRITE;
/*!40000 ALTER TABLE `Copia_de_operadores` DISABLE KEYS */;

INSERT INTO `Copia_de_operadores` VALUES (105,'JUAN GABRIEL KANTUN BALAN','on','9841812171','',NULL,918,'',1);
INSERT INTO `Copia_de_operadores` VALUES (106,'WILFRIDO AGUILAR VALERIO ','on','9841820924','',NULL,322,'',1);
INSERT INTO `Copia_de_operadores` VALUES (107,'JORGE MIGUEL KU SUASTE','on','9842172412','',NULL,526,'',1);
INSERT INTO `Copia_de_operadores` VALUES (108,'ANTONIO DE JESUS POOT MARTINES ','on','9841459528','',NULL,799,'',1);
INSERT INTO `Copia_de_operadores` VALUES (109,'HENRRRY MAGNEALI BARABATA','on','9341158801','',NULL,92,'',1);
INSERT INTO `Copia_de_operadores` VALUES (110,'DIDIER OLAGUIVER MAY CITIS','on','9861032498','',NULL,637,'',1);
INSERT INTO `Copia_de_operadores` VALUES (111,'JULIAN SORIANO PERDOMO','on','7828183054','',NULL,682,'',1);
INSERT INTO `Copia_de_operadores` VALUES (112,'JOSE CLEINER YAMA BARZON','on','9841302194','',NULL,668,'',1);
INSERT INTO `Copia_de_operadores` VALUES (113,'LUCIANA CORDOBA BACAB','on','9842176205','',NULL,1021,'',1);
INSERT INTO `Copia_de_operadores` VALUES (114,'GERARDO LOPEZ GONZALEZ','on','9842133180','',NULL,361,'',1);
INSERT INTO `Copia_de_operadores` VALUES (115,'PABLO COP YAMA','on','9831368631','',NULL,800,'',1);
INSERT INTO `Copia_de_operadores` VALUES (116,'RONAL MARIK EK BRAVO','on','9831146888','',NULL,773,'',1);
INSERT INTO `Copia_de_operadores` VALUES (117,'LUIS FERNANDEZ CHE CEN','on','9831147878','',NULL,690,'',1);
INSERT INTO `Copia_de_operadores` VALUES (118,'LUIS ALBERTO DZIB ROMERO','on','9841289133','',NULL,1010,'',1);
INSERT INTO `Copia_de_operadores` VALUES (119,'ISAAC AZAEL MAY HAU','on','9851251473','',NULL,663,'',1);
INSERT INTO `Copia_de_operadores` VALUES (120,'WALTER SALVADOR DE LA CRUZ RODRIGES ','on','9842181475','',NULL,409,'',1);
INSERT INTO `Copia_de_operadores` VALUES (121,'ADRIAN REJON PEREZ','on','9994123435','',NULL,442,'',1);
INSERT INTO `Copia_de_operadores` VALUES (122,'NAHUM SULUB NAVARRO','on','9841429952','',NULL,249,'',1);
INSERT INTO `Copia_de_operadores` VALUES (123,'JULIO CESAR SANCHEZ BARRERA','on','9841882652','',NULL,1040,'',1);
INSERT INTO `Copia_de_operadores` VALUES (125,'APOLONIO MAY DZIB','on','9842073003','',NULL,698,'',1);
INSERT INTO `Copia_de_operadores` VALUES (126,'GERONIMO TUYUB NOH','on','9851158675','',NULL,12,'',1);
INSERT INTO `Copia_de_operadores` VALUES (127,'JOAQUIN ARMIN ESCALANTE','on','9848796110','',NULL,206,'',1);
INSERT INTO `Copia_de_operadores` VALUES (128,'ANGEL BENEDICTO CHIMAL TUK','on','9837007599','',NULL,172,'',1);
INSERT INTO `Copia_de_operadores` VALUES (129,'JUAN GABRIEL CHI RUIZ','on','9841560191','',NULL,175,'',1);
INSERT INTO `Copia_de_operadores` VALUES (130,'OSCAR ARIEL UITZIL ARSEO','on','9991632058','',NULL,453,'',1);
INSERT INTO `Copia_de_operadores` VALUES (131,'FREDERIC REYES DAMA','on','9841059054','',NULL,143,'',1);
INSERT INTO `Copia_de_operadores` VALUES (132,'MARCO ANTONIO PEREZ','on','9841787028','',NULL,867,'',1);
INSERT INTO `Copia_de_operadores` VALUES (133,'MARGARITO CARLOS SANTIAGO','on','9841784635','',NULL,941,'',1);
INSERT INTO `Copia_de_operadores` VALUES (134,'ROBERTO CARLOS SANTIAGO','on','9842386929','',NULL,697,'',1);
INSERT INTO `Copia_de_operadores` VALUES (135,'LUIS ADRIAN GOMES PALACIO','on','9841827258','',NULL,129,'',1);
INSERT INTO `Copia_de_operadores` VALUES (136,'SIXTO MANUEL QUESADA','on','9841658581','',NULL,50,'',1);
INSERT INTO `Copia_de_operadores` VALUES (137,'DIEGO TORRES CALDERON','on','9831389958','',NULL,0,'',1);
INSERT INTO `Copia_de_operadores` VALUES (138,'AURELIO BALAM AY','on','9841412855','',NULL,1020,'',1);
INSERT INTO `Copia_de_operadores` VALUES (139,'MARGARITA LARA SILVESTRE','on','9841831446','',NULL,1048,'',1);
INSERT INTO `Copia_de_operadores` VALUES (140,'URIEL ALEJANDRO BARRERA SANTO','on','9384013443','',NULL,1023,'',1);
INSERT INTO `Copia_de_operadores` VALUES (141,'JOSE AFRAIN EUAN ABAN','on','9851017305','',NULL,1017,'',1);
INSERT INTO `Copia_de_operadores` VALUES (142,'BENJAMIN CAHUICH CHAN','on','9842074622','',NULL,894,'',1);
INSERT INTO `Copia_de_operadores` VALUES (143,'ERICK GEOVANI RAMIREZ POOL','on','9841353363','',NULL,792,'',1);
INSERT INTO `Copia_de_operadores` VALUES (144,'EDUARDO HIPOLITO SANCHEZ BARRERA','on','9992595421','',NULL,576,'',1);
INSERT INTO `Copia_de_operadores` VALUES (145,'DAVID ABIMAEL TEC BALAM','on','9851089221','',NULL,1071,'',1);
INSERT INTO `Copia_de_operadores` VALUES (146,'HECTOR HOGO BARAHONA OLAN','on','9841293378','',NULL,873,'',1);
INSERT INTO `Copia_de_operadores` VALUES (147,'PEDRO NICOLAS LIZAMA BAEZA','on','9841993750','',NULL,804,'',1);
INSERT INTO `Copia_de_operadores` VALUES (148,'RICARDO DE LA ROSA HERNANDEZ','on','9842161460','',NULL,164,'',1);
INSERT INTO `Copia_de_operadores` VALUES (149,'ALDOMAR CANCHE TUYUB','on','9841366627','',NULL,575,'',1);
INSERT INTO `Copia_de_operadores` VALUES (150,'OTILIA ASCENSIO RAMIREZ','on','9321275351','',NULL,437,'',1);
INSERT INTO `Copia_de_operadores` VALUES (151,'AUDOMARO CALDERON PEREZ','on','9961051144','',NULL,548,'',1);
INSERT INTO `Copia_de_operadores` VALUES (152,'ROGELIO HERNANDEZ HERNANDEZ','on','9842169305','',NULL,514,'',1);
INSERT INTO `Copia_de_operadores` VALUES (153,'FRANCISCO DE LA ROSA SI FUENTES','on','9837527885','',NULL,1053,'',1);
INSERT INTO `Copia_de_operadores` VALUES (154,'JUAN VALENTIN PECH YAMA','on','9841684015','',NULL,1051,'',1);
INSERT INTO `Copia_de_operadores` VALUES (155,'JOSE MANUEL MEDINA KUMUL','on','9841878940','',NULL,669,'',1);
INSERT INTO `Copia_de_operadores` VALUES (156,'ELIAS MENDEZ RODRIGUEZ','on','9983931075','',NULL,571,'',1);
INSERT INTO `Copia_de_operadores` VALUES (157,'ONESIMO ALMENDRA RAMON','on','9847458915','',NULL,482,'',1);
INSERT INTO `Copia_de_operadores` VALUES (158,'ARTURO VELAZQUEZ MORALES','on','9841842302','',NULL,656,'',1);
INSERT INTO `Copia_de_operadores` VALUES (159,'ALVARO DZID CHI','on','9848075159','',NULL,56,'',1);
INSERT INTO `Copia_de_operadores` VALUES (160,'GUSTAVO JESUS GOMEZ SANCHEZ','on','9841441877','',NULL,731,'',1);
INSERT INTO `Copia_de_operadores` VALUES (161,'ALEJANDRO AGUILAR GARCIA','on','9841397915','',NULL,790,'',1);
INSERT INTO `Copia_de_operadores` VALUES (162,'ANGEL ENRIQUE CHI CHUC','on','9837003485','',NULL,892,'',1);
INSERT INTO `Copia_de_operadores` VALUES (163,'JOSE RENAN BALLOTE ','on','9841761479','',NULL,39,'',1);
INSERT INTO `Copia_de_operadores` VALUES (164,'JUAN JOSE HERNANDEZ CORTEZ','on','9842419584','',NULL,802,'',1);
INSERT INTO `Copia_de_operadores` VALUES (165,'JOSE GUADALUPE GARCIA','on','9381237305','',NULL,754,'',1);
INSERT INTO `Copia_de_operadores` VALUES (166,'ANTONIO MEX CUXIN ','on','9842091091','',NULL,278,'',1);
INSERT INTO `Copia_de_operadores` VALUES (167,'JOSE ADUARDO PECH ','on','9831844577','',NULL,480,'',1);
INSERT INTO `Copia_de_operadores` VALUES (168,'RODRIGO MARTINEZ ARCHUNDIA','on','9841770694','',NULL,55,'',1);
INSERT INTO `Copia_de_operadores` VALUES (169,'LUIS ANGEL HERNANDEZ CASANOBA','on','9841069752','',NULL,638,'',1);
INSERT INTO `Copia_de_operadores` VALUES (170,'CARLOS REUGIO ESCALANTE MARTINEZ ','on','6648004983','',NULL,944,'',1);
INSERT INTO `Copia_de_operadores` VALUES (171,'ALEJANDRO LEONARDO TAH CIH','on','9841658453','',NULL,378,'',1);
INSERT INTO `Copia_de_operadores` VALUES (172,'IVAN ALBERTO BALLIN CERVERA','on','9841457046','',NULL,47,'',1);
INSERT INTO `Copia_de_operadores` VALUES (173,'JOSE CARLOS PECH GOMEZ','on','9841367757','',NULL,455,'',1);
INSERT INTO `Copia_de_operadores` VALUES (174,'JUAN GABRIEL SANCHEZ BARRERA','on','9993633644','',NULL,1102,'',1);
INSERT INTO `Copia_de_operadores` VALUES (175,'LUIS MIGUEL CHIMAL MAY','on','9841361303','',NULL,349,'',1);
INSERT INTO `Copia_de_operadores` VALUES (176,'EZEQUIEL TUN CHE ','on','9842174005','',NULL,396,'',1);
INSERT INTO `Copia_de_operadores` VALUES (177,'ROBERTO CAUICH COB','on','9842030112','',NULL,454,'',1);
INSERT INTO `Copia_de_operadores` VALUES (178,'LUIS ALBERTO YAM CHAY','on','9851148816','',NULL,784,'',1);
INSERT INTO `Copia_de_operadores` VALUES (179,'ANTONIO TUYUB NOH','on','9841145598','',NULL,0,'',1);
INSERT INTO `Copia_de_operadores` VALUES (180,'ROBERTO MARTIN REYES MELCHOR','on','9841724355','',NULL,433,'',1);
INSERT INTO `Copia_de_operadores` VALUES (181,'CARLOS IGUASU ESCALANTE FLOTA','on','9831804782','',NULL,0,'',1);
INSERT INTO `Copia_de_operadores` VALUES (182,'CARLOS ROBERTO NOVELO CANUL','on','9842172150','',NULL,981,'',1);
INSERT INTO `Copia_de_operadores` VALUES (183,'KEVIN ALEXANDER ALVAREZ SANCHEZ','on','9983968060','',NULL,964,'',1);
INSERT INTO `Copia_de_operadores` VALUES (184,'ADRIAN BORJES VILLA','on','5574327098','',NULL,604,'',1);
INSERT INTO `Copia_de_operadores` VALUES (185,'MARGARITO CHI BALAM','on','9841784635','',NULL,941,'',1);
INSERT INTO `Copia_de_operadores` VALUES (186,'LUIS ARTURO VELASQUES MORALES','on','9841842302','',NULL,656,'',1);
INSERT INTO `Copia_de_operadores` VALUES (187,'JOSE DARIO CANCHE ABAN','on','9842561418','',NULL,127,'',1);
INSERT INTO `Copia_de_operadores` VALUES (188,'SANTIAGO JULIAN CISNERO MARTINES','on','9841799033','',NULL,900,'',1);
INSERT INTO `Copia_de_operadores` VALUES (189,'SANTIAGO ALBARADO PROMOTOR','on','9831645568','',NULL,640,'',1);
INSERT INTO `Copia_de_operadores` VALUES (190,'LORENSO BALAM POC','on','9841408648','',NULL,85,'',1);
INSERT INTO `Copia_de_operadores` VALUES (191,'NOE ALFARO MENDEZ ','on','9841672787','',NULL,175,'',1);
INSERT INTO `Copia_de_operadores` VALUES (192,'BRAULIO CANCHE TUYUB','on','9842027793','',NULL,494,'',1);
INSERT INTO `Copia_de_operadores` VALUES (193,'HEIDI DURAM LAMBERT','on','9841148553','',NULL,212,'',1);
INSERT INTO `Copia_de_operadores` VALUES (194,'JOSE ARCENIO LORIA CAMPOS','on','9841805469','',NULL,298,'',1);
INSERT INTO `Copia_de_operadores` VALUES (195,'GUSTAVO ANGEL YAM CHAY','on','9841816826','',NULL,239,'',1);
INSERT INTO `Copia_de_operadores` VALUES (197,'OSCAR ALEJANDRO SANTO PI�A','on','9838097335','',NULL,0,'',1);
INSERT INTO `Copia_de_operadores` VALUES (198,'RAMON BORGES DZUL','on','9994866683','',NULL,343,'',1);
INSERT INTO `Copia_de_operadores` VALUES (199,'SALVADOR TUK UITZIL','on','9848761531','',NULL,267,'',1);
INSERT INTO `Copia_de_operadores` VALUES (200,'DAVID ROJAS ARSILA','on','9831585708','',NULL,1127,'',1);
INSERT INTO `Copia_de_operadores` VALUES (201,'JOSE CANDELARIO UCAN MAY','on','9831430479','',NULL,312,'',1);
INSERT INTO `Copia_de_operadores` VALUES (202,'DANIEL CRUZ RODRIGUEZ','on','9821048629','',NULL,0,'',1);
INSERT INTO `Copia_de_operadores` VALUES (205,'MARTIN REYES QUETZ','on','9841340499','',NULL,761,'',1);
INSERT INTO `Copia_de_operadores` VALUES (206,'MARIA VICTORIA GONSALEZ RODRIGUEZ','on','9841761385','',NULL,0,'',1);
INSERT INTO `Copia_de_operadores` VALUES (207,'KEVIN ALAIN ECHEVERRIA GONZALEZ','on','9841644556','',NULL,0,'',1);
INSERT INTO `Copia_de_operadores` VALUES (208,'EDUARDO ALBERTO LIMA SANCHEZ','on','9841137147','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (209,'OSCAR MANUEL DIAZ JIMENEZ ','on','5544926821','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (210,'JUAN MANUEL CHIMAL ','on','9848791954','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (212,'HUGO RIVERA ORTEGA','on','5546817363','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (214,'SANTOS RAMON VALDEZ MENDEZ','on','9831488852','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (215,'PEDRO CHUC CAUICH','on','9841456619','',NULL,0,'',1);
INSERT INTO `Copia_de_operadores` VALUES (216,'JESUS ENRIQUE GARCIA ARCOS ','on','9841112983','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (217,'JORGE LUIS BARRIOS CASTA�EDA','on','9841352093','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (218,'SAUL RUEDA RAMIREZ','on','9847454290','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (219,'SANTOS DAMIAN CANCHE POOL','on','9841282053','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (220,'MANUEL ARMIN DZUL DZUL','on','9841067911','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (221,'DIDIER ANDRADE VAZQUEZ MARTIN','on','9841334300','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (222,'REY DAVID AY CIAU','on','9842409937','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (223,'JOSE MARIA SANCHEZ ALTAMIRANO','on','9848770560','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (224,'SAMUEL ORTIZ NEGRETE','on','9841336052','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (225,'ROBERTO MAY ABAM','on','9842046959','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (226,'ELIASAR MORENO LOPEz','on','9831073261','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (227,'GRACIANO CEN MAY','on','9842156128','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (229,'JOSE ROBERTO COHUO CASTILLO ','on','9841069418','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (231,'ALAN GERARDO VALDEZ COUOH','on','9988949039','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (232,'ALVINO ENRIQUE MATEO','on','9837002625','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (233,'EDGAR ISSAC EZQUIVEL VERNAL','on','5521837626','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (234,'DAVID NOE BOJORQUES HERNANDEZ','on','9842166399','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (235,'ELEAZAR MORENO LOPEZ ','on','9831073261','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (236,'DARWIN  ALEXANDER ROSADO GARCIA','on','9341297759','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (237,'RAUL ALFONSO MORALES PACHECO','on','9841347003','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (238,'LUIS ALBERTO VASQUEZ BORGAZ','on','9848044412','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (239,'MAURICIO DE JESUS AGUILAR PONCE','on','9842124159','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (240,'JOSE EZEQUIEL COCOM DZIB','on','9848043071','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (241,'GORJE LUIS VARRIOS CASTA�EDA ','on','9841362093','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (242,'EDDY BALTAZAR PACHECO DZUL','on','9842182093','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (243,'EDUARDO DANIEL REQUENA RAMIREZ','on','9841300254','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (244,'FELIPE JULIO CRUZ CABALLERO','on','9831132844','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (245,'ERICK HERNANDEZ GARCIA','on','9841299372','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (246,'VICTOR HUGO LUNA MENDOZA','on','9831547890','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (247,'ARTURO HERNANDEZ ALCANTARA','on','9931090627','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (248,'JERONIMO TUYUB NOH','on','9851158675','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (249,'HONORATO ABAM PAT','on','9842083179','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (250,'HONORATO ABAM PAT','on','9842083179','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (251,'GEREMIAS MARTINES ARA','on','9841457378','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (252,'RAFAEL MAY COCON','on','9851099714','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (253,'JESUS AVILES RODRIGUES','on','9831142376','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (254,'MANUEL ALEXANDER VARGAS AGUILAR','on','9831621749','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (255,'MANUEL ALEXANDER VARGAS AGUILAR','on','9831621749','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (256,'ROSALINO JIMENES CORDOBA','on','2293432830','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (257,'GABRIEL VARGEZ PUCH','on','9841655805','',NULL,0,'',2);
INSERT INTO `Copia_de_operadores` VALUES (258,'JOSE EFRAIN EUAN ABAN','on','9851017300','',NULL,1017,'',1);
INSERT INTO `Copia_de_operadores` VALUES (259,'ERICK FRANCISCO DIAZ ORTIZ','on','9841765292','',NULL,0,'',2);
/*!40000 ALTER TABLE `Copia_de_operadores` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table correos_newsletter
#

DROP TABLE IF EXISTS `correos_newsletter`;
CREATE TABLE `correos_newsletter` (
  `id_correo` int(4) NOT NULL AUTO_INCREMENT,
  `correo` varchar(200) COLLATE utf8_swedish_ci NOT NULL,
  `status` varchar(3) COLLATE utf8_swedish_ci NOT NULL,
  PRIMARY KEY (`id_correo`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

#
# Dumping data for table correos_newsletter
#
LOCK TABLES `correos_newsletter` WRITE;
/*!40000 ALTER TABLE `correos_newsletter` DISABLE KEYS */;

INSERT INTO `correos_newsletter` VALUES (1,'info@sectornetcancun.com','');
INSERT INTO `correos_newsletter` VALUES (2,'gabriel_ing_sistemas@hotmail.com','');
INSERT INTO `correos_newsletter` VALUES (3,'thundercat65@hotmail.com','');
INSERT INTO `correos_newsletter` VALUES (5,'hugozedillo@gmail.com','');
INSERT INTO `correos_newsletter` VALUES (6,'vazquez1072@gmail.com','');
INSERT INTO `correos_newsletter` VALUES (7,'hcedillo@fibmex.com','');
INSERT INTO `correos_newsletter` VALUES (11,'hugocedillo@outlook.com','');
INSERT INTO `correos_newsletter` VALUES (12,'mvesrs@gmail.com','');
INSERT INTO `correos_newsletter` VALUES (13,'hcedillo@xangos.com.mx','');
/*!40000 ALTER TABLE `correos_newsletter` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table cupon
#

DROP TABLE IF EXISTS `cupon`;
CREATE TABLE `cupon` (
  `id_cupon` int(11) NOT NULL AUTO_INCREMENT,
  `clave` varchar(255) DEFAULT NULL,
  `concepto` text,
  `status` varchar(5) DEFAULT NULL,
  `porcentaje` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_cupon`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Dumping data for table cupon
#
LOCK TABLES `cupon` WRITE;
/*!40000 ALTER TABLE `cupon` DISABLE KEYS */;

INSERT INTO `cupon` VALUES (3,'Promo5','Clientes Repetitivos','on',5);
INSERT INTO `cupon` VALUES (4,'Promo10','Xangos Crew','on',10);
INSERT INTO `cupon` VALUES (5,'Promo20','Cupones Casa','on',20);
INSERT INTO `cupon` VALUES (6,'Promo15','Clientes VIP','on',15);
/*!40000 ALTER TABLE `cupon` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table edicion_condominos
#

DROP TABLE IF EXISTS `edicion_condominos`;
CREATE TABLE `edicion_condominos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_propietario` varchar(500) DEFAULT NULL,
  `nombre_arrendador` varchar(800) DEFAULT NULL,
  `telefono_propietario` varchar(255) DEFAULT NULL,
  `telefono_arrendador` varchar(255) DEFAULT NULL,
  `correo_propietario` varchar(255) DEFAULT NULL,
  `correo_arrendador` varchar(255) DEFAULT NULL,
  `observaciones` text,
  `tipo_condomino` int(11) DEFAULT NULL,
  `condomino_id` int(11) DEFAULT NULL,
  `movil_propietario` varchar(255) DEFAULT NULL,
  `movil_arrendador` varchar(255) DEFAULT NULL,
  `editado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

#
# Dumping data for table edicion_condominos
#
LOCK TABLES `edicion_condominos` WRITE;
/*!40000 ALTER TABLE `edicion_condominos` DISABLE KEYS */;

INSERT INTO `edicion_condominos` VALUES (33,'Jesus Gabriel ','mmmm','0','0','info@sectornetcancun.com','','',5,45,'9983498879','9983498879',0);
INSERT INTO `edicion_condominos` VALUES (39,'1','','','','','','',0,23,'','',0);
INSERT INTO `edicion_condominos` VALUES (40,'Marta Saborido Rebes','','','','mvesrs@gmail.com','','',4,48,'9981923103','9981923103',0);
/*!40000 ALTER TABLE `edicion_condominos` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table estado_cuenta
#

DROP TABLE IF EXISTS `estado_cuenta`;
CREATE TABLE `estado_cuenta` (
  `id_estado_cuenta` int(11) NOT NULL AUTO_INCREMENT,
  `condomino_id` int(11) DEFAULT NULL,
  `condominio_id` int(11) DEFAULT NULL,
  `titulo` varchar(1000) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `observaciones` text,
  `mes` int(11) DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_estado_cuenta`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

#
# Dumping data for table estado_cuenta
#
LOCK TABLES `estado_cuenta` WRITE;
/*!40000 ALTER TABLE `estado_cuenta` DISABLE KEYS */;

INSERT INTO `estado_cuenta` VALUES (38,48,112,'Estado de Cuenta Diciembre 2016','on','',12,2016);
INSERT INTO `estado_cuenta` VALUES (39,49,112,'Estado de Cuenta Diciembre 2016','on','',12,2016);
INSERT INTO `estado_cuenta` VALUES (40,53,112,'Estado de Cuenta Diciembre 2016','on','',12,2016);
INSERT INTO `estado_cuenta` VALUES (41,50,112,'Estado de Cuenta Diciembre 2016','on','',12,2016);
/*!40000 ALTER TABLE `estado_cuenta` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table estado_financiero
#

DROP TABLE IF EXISTS `estado_financiero`;
CREATE TABLE `estado_financiero` (
  `id_estado_financiero` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(1000) DEFAULT NULL,
  `condominio_id` int(11) DEFAULT NULL,
  `observaciones` text,
  `status` varchar(3) DEFAULT NULL,
  `mes` int(11) DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_estado_financiero`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

#
# Dumping data for table estado_financiero
#
LOCK TABLES `estado_financiero` WRITE;
/*!40000 ALTER TABLE `estado_financiero` DISABLE KEYS */;

INSERT INTO `estado_financiero` VALUES (14,'Reporte de Cobranza Dic 2016',112,'','on',12,2016);
INSERT INTO `estado_financiero` VALUES (15,'Estados Financieros Diciembre 2016',112,'','on',12,2016);
/*!40000 ALTER TABLE `estado_financiero` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table imagen
#

DROP TABLE IF EXISTS `imagen`;
CREATE TABLE `imagen` (
  `id_imagen` int(10) NOT NULL AUTO_INCREMENT,
  `id_content` int(10) NOT NULL COMMENT 'quiero saber a que id pertenece ',
  `imagen` varchar(100) NOT NULL,
  `seccion` varchar(200) NOT NULL,
  `numero_imagen` int(100) NOT NULL,
  `seccion_seccion` varchar(100) NOT NULL COMMENT 'Ejemplo si es logo, galeria, frente etc de una seccion ejemplo familia',
  PRIMARY KEY (`id_imagen`)
) ENGINE=MyISAM AUTO_INCREMENT=1938 DEFAULT CHARSET=latin1;

#
# Dumping data for table imagen
#
LOCK TABLES `imagen` WRITE;
/*!40000 ALTER TABLE `imagen` DISABLE KEYS */;

INSERT INTO `imagen` VALUES (5,18,'icono_18_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (11,2,'logo_2_1.png','tour',1,'logo');
INSERT INTO `imagen` VALUES (12,2,'galeria_tour_2_1.png','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (13,2,'galeria_tour_2_2.png','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (14,2,'galeria_tour_2_3.png','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (15,2,'galeria_tour_2_4.png','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (17,4,'galeria_tour_4_1.png','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (20,4,'logo_4_1.png','tour',1,'logo');
INSERT INTO `imagen` VALUES (21,5,'logo_5_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (55,57,'galeria_tour_57_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (56,57,'galeria_tour_57_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (57,57,'galeria_tour_57_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (59,56,'galeria_tour_56_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (60,56,'galeria_tour_56_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (61,56,'galeria_tour_56_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (63,54,'galeria_tour_54_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (64,54,'galeria_tour_54_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (65,54,'galeria_tour_54_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (66,54,'galeria_tour_54_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (68,54,'galeria_tour_54_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (81,18,'galeria_tour_18_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (82,18,'galeria_tour_18_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (83,18,'galeria_tour_18_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (84,18,'galeria_tour_18_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (85,18,'galeria_tour_18_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (86,18,'galeria_tour_18_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (126,64,'galeria_tour_64_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (127,64,'galeria_tour_64_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (128,64,'galeria_tour_64_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (129,64,'galeria_tour_64_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (131,64,'galeria_tour_64_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (133,64,'galeria_tour_64_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (163,55,'galeria_tour_55_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (167,53,'galeria_tour_53_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (168,53,'galeria_tour_53_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (171,45,'galeria_tour_45_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (179,47,'galeria_tour_47_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (183,47,'galeria_tour_47_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (184,47,'galeria_tour_47_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (194,66,'galeria_tour_66_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (195,66,'galeria_tour_66_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (196,66,'galeria_tour_66_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (197,67,'logo_67_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (198,67,'galeria_tour_67_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (199,67,'galeria_tour_67_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (200,67,'galeria_tour_67_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (202,68,'galeria_tour_68_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (203,68,'galeria_tour_68_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (204,68,'galeria_tour_68_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (212,71,'galeria_tour_71_3.png','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (223,74,'galeria_tour_74_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (224,74,'galeria_tour_74_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (225,75,'galeria_tour_75_1.png','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (226,75,'galeria_tour_75_2.png','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (227,75,'galeria_tour_75_3.png','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (228,75,'galeria_tour_75_4.png','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (229,75,'galeria_tour_75_5.png','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (261,0,'banner__1.jpg','destino',1,'banner');
INSERT INTO `imagen` VALUES (264,8,'banner_8_1.jpg','destino',1,'banner');
INSERT INTO `imagen` VALUES (270,19,'logo_19_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (271,11,'logo_11_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (276,50,'logo_50_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (285,45,'logo_45_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (290,68,'logo_68_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (292,66,'logo_66_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (293,9,'logo_9_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (297,58,'logo_58_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (301,60,'logo_60_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (304,65,'logo_65_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (309,57,'logo_57_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (310,56,'logo_56_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (312,70,'logo_70_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (314,69,'logo_69_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (323,53,'logo_53_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (325,54,'logo_54_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (327,64,'logo_64_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (332,39,'logo_39_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (335,43,'logo_43_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (336,18,'logo_18_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (341,71,'logo_71_1.png','tour',1,'logo');
INSERT INTO `imagen` VALUES (346,38,'logo_38_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (349,48,'logo_48_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (350,47,'logo_47_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (351,46,'logo_46_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (353,52,'logo_52_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (354,51,'logo_51_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (356,49,'logo_49_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (371,19,'banner_19_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (372,19,'galeria_tour_19_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (379,69,'banner_69_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (385,69,'galeria_tour_69_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (386,69,'galeria_tour_69_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (387,69,'galeria_tour_69_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (388,69,'galeria_tour_69_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (414,41,'logo_41_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (441,55,'banner_55_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (442,55,'logo_55_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (443,55,'galeria_tour_55_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (444,55,'galeria_tour_55_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (459,31,'logo_31_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (473,22,'galeria_tour_22_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (474,22,'galeria_tour_22_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (476,22,'galeria_tour_22_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (477,22,'galeria_tour_22_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (503,14,'galeria_tour_14_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (507,14,'galeria_tour_14_8.JPG','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (509,14,'galeria_tour_14_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (521,15,'galeria_tour_15_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (522,15,'galeria_tour_15_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (523,15,'galeria_tour_15_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (524,15,'galeria_tour_15_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (525,15,'galeria_tour_15_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (526,15,'logo_15_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (527,15,'galeria_tour_15_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (528,15,'galeria_tour_15_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (529,15,'galeria_tour_15_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (530,15,'galeria_tour_15_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (531,15,'galeria_tour_15_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (533,22,'logo_22_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (545,13,'galeria_tour_13_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (556,13,'logo_13_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (557,13,'galeria_tour_13_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (560,45,'galeria_tour_45_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (561,45,'galeria_tour_45_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (562,45,'galeria_tour_45_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (563,45,'galeria_tour_45_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (564,45,'galeria_tour_45_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (570,48,'galeria_tour_48_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (571,48,'galeria_tour_48_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (572,48,'galeria_tour_48_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (573,48,'galeria_tour_48_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (574,48,'galeria_tour_48_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (580,47,'galeria_tour_47_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (581,47,'galeria_tour_47_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (582,46,'galeria_tour_46_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (583,46,'galeria_tour_46_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (584,46,'galeria_tour_46_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (585,46,'galeria_tour_46_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (586,46,'galeria_tour_46_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (587,46,'galeria_tour_46_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (645,72,'galeria_tour_72_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (646,72,'galeria_tour_72_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (647,72,'galeria_tour_72_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (649,72,'galeria_tour_72_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (650,72,'galeria_tour_72_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (665,79,'galeria_tour_79_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (667,79,'galeria_tour_79_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (670,79,'galeria_tour_79_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (671,79,'galeria_tour_79_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (672,79,'galeria_tour_79_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (673,79,'logo_79_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (683,73,'galeria_tour_73_1.png','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (684,73,'galeria_tour_73_2.png','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (685,73,'galeria_tour_73_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (686,73,'galeria_tour_73_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (687,73,'galeria_tour_73_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (688,73,'logo_73_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (690,58,'galeria_tour_58_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (691,58,'galeria_tour_58_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (692,58,'galeria_tour_58_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (693,58,'galeria_tour_58_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (694,58,'galeria_tour_58_5.png','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (698,58,'galeria_tour_58_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (700,58,'galeria_tour_58_11.png','tour',11,'galeria_tour');
INSERT INTO `imagen` VALUES (702,9,'galeria_tour_9_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (703,9,'galeria_tour_9_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (704,9,'galeria_tour_9_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (705,9,'galeria_tour_9_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (706,9,'galeria_tour_9_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (707,9,'galeria_tour_9_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (708,9,'galeria_tour_9_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (709,9,'galeria_tour_9_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (710,9,'galeria_tour_9_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (720,9,'galeria_tour_9_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (721,9,'galeria_tour_9_11.jpg','tour',11,'galeria_tour');
INSERT INTO `imagen` VALUES (722,9,'galeria_tour_9_12.jpg','tour',12,'galeria_tour');
INSERT INTO `imagen` VALUES (734,55,'galeria_tour_55_4.JPG','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (735,55,'galeria_tour_55_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (739,77,'galeria_tour_77_2.png','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (740,77,'galeria_tour_77_3.png','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (741,77,'galeria_tour_77_4.png','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (742,77,'galeria_tour_77_5.png','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (743,77,'galeria_tour_77_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (744,77,'galeria_tour_77_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (745,77,'galeria_tour_77_8.png','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (767,77,'logo_77_1.png','tour',1,'logo');
INSERT INTO `imagen` VALUES (776,77,'galeria_tour_77_33.jpg','tour',33,'galeria_tour');
INSERT INTO `imagen` VALUES (778,72,'logo_72_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (783,38,'galeria_tour_38_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (784,38,'galeria_tour_38_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (786,38,'galeria_tour_38_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (787,38,'galeria_tour_38_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (788,38,'galeria_tour_38_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (789,38,'galeria_tour_38_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (790,38,'galeria_tour_38_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (827,38,'banner_38_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (837,6,'banner_6_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (838,6,'logo_6_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (839,6,'galeria_tour_6_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (840,6,'galeria_tour_6_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (841,6,'galeria_tour_6_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (842,6,'galeria_tour_6_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (843,6,'galeria_tour_6_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (845,6,'galeria_tour_6_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (846,6,'galeria_tour_6_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (847,6,'galeria_tour_6_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (848,6,'galeria_tour_6_11.jpg','tour',11,'galeria_tour');
INSERT INTO `imagen` VALUES (849,6,'galeria_tour_6_12.jpg','tour',12,'galeria_tour');
INSERT INTO `imagen` VALUES (850,6,'galeria_tour_6_13.jpg','tour',13,'galeria_tour');
INSERT INTO `imagen` VALUES (851,6,'galeria_tour_6_14.jpg','tour',14,'galeria_tour');
INSERT INTO `imagen` VALUES (852,6,'galeria_tour_6_15.jpg','tour',15,'galeria_tour');
INSERT INTO `imagen` VALUES (853,6,'galeria_tour_6_16.jpg','tour',16,'galeria_tour');
INSERT INTO `imagen` VALUES (862,77,'banner_77_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (863,73,'galeria_tour_73_6.png','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (868,73,'banner_73_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (899,3,'banner_3_1.jpg','destino',1,'banner');
INSERT INTO `imagen` VALUES (906,39,'galeria_tour_39_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (908,39,'galeria_tour_39_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (911,39,'galeria_tour_39_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (912,39,'galeria_tour_39_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (913,40,'logo_40_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (914,40,'galeria_tour_40_1.0a0d51','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (915,40,'galeria_tour_40_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (916,40,'galeria_tour_40_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (918,40,'galeria_tour_40_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (931,24,'galeria_tour_24_12.jpg','tour',12,'galeria_tour');
INSERT INTO `imagen` VALUES (932,24,'galeria_tour_24_13.jpg','tour',13,'galeria_tour');
INSERT INTO `imagen` VALUES (933,24,'galeria_tour_24_14.jpg','tour',14,'galeria_tour');
INSERT INTO `imagen` VALUES (934,24,'galeria_tour_24_15.jpg','tour',15,'galeria_tour');
INSERT INTO `imagen` VALUES (943,24,'banner_24_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (944,24,'logo_24_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (952,24,'galeria_tour_24_17.jpg','tour',17,'galeria_tour');
INSERT INTO `imagen` VALUES (953,24,'galeria_tour_24_18.jpg','tour',18,'galeria_tour');
INSERT INTO `imagen` VALUES (961,76,'banner_76_1.png','tour',1,'banner');
INSERT INTO `imagen` VALUES (962,76,'logo_76_1.png','tour',1,'logo');
INSERT INTO `imagen` VALUES (977,76,'galeria_tour_76_4.png','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (978,76,'galeria_tour_76_5.png','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (979,76,'galeria_tour_76_6.png','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (980,76,'galeria_tour_76_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (985,59,'galeria_tour_59_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (986,59,'galeria_tour_59_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (987,59,'banner_59_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (988,59,'galeria_tour_59_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (992,59,'galeria_tour_59_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (997,59,'logo_59_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (998,59,'galeria_tour_59_11.jpg','tour',11,'galeria_tour');
INSERT INTO `imagen` VALUES (1000,16,'logo_16_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1001,16,'galeria_tour_16_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1002,16,'galeria_tour_16_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1003,16,'galeria_tour_16_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1007,3,'icono_3_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1008,23,'icono_23_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1009,10,'icono_10_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1012,19,'icono_19_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1013,4,'icono_4_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1015,31,'icono_31_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1016,11,'icono_11_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1022,5,'icono_5_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1025,2,'icono_2_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1036,17,'banner_17_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1038,17,'logo_17_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1048,17,'galeria_tour_17_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (1052,17,'galeria_tour_17_14.jpg','tour',14,'galeria_tour');
INSERT INTO `imagen` VALUES (1054,17,'galeria_tour_17_16.jpg','tour',16,'galeria_tour');
INSERT INTO `imagen` VALUES (1055,17,'galeria_tour_17_17.jpg','tour',17,'galeria_tour');
INSERT INTO `imagen` VALUES (1058,17,'galeria_tour_17_20.jpg','tour',20,'galeria_tour');
INSERT INTO `imagen` VALUES (1061,17,'galeria_tour_17_23.jpg','tour',23,'galeria_tour');
INSERT INTO `imagen` VALUES (1062,17,'galeria_tour_17_24.jpg','tour',24,'galeria_tour');
INSERT INTO `imagen` VALUES (1064,17,'galeria_tour_17_26.jpg','tour',26,'galeria_tour');
INSERT INTO `imagen` VALUES (1068,12,'banner_12_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1079,12,'galeria_tour_12_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1081,12,'galeria_tour_12_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1082,12,'galeria_tour_12_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (1083,12,'galeria_tour_12_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (1084,12,'galeria_tour_12_11.jpg','tour',11,'galeria_tour');
INSERT INTO `imagen` VALUES (1085,12,'galeria_tour_12_12.jpg','tour',12,'galeria_tour');
INSERT INTO `imagen` VALUES (1087,12,'galeria_tour_12_13.jpg','tour',13,'galeria_tour');
INSERT INTO `imagen` VALUES (1088,12,'galeria_tour_12_14.jpg','tour',14,'galeria_tour');
INSERT INTO `imagen` VALUES (1089,12,'galeria_tour_12_15.jpg','tour',15,'galeria_tour');
INSERT INTO `imagen` VALUES (1090,12,'galeria_tour_12_16.jpg','tour',16,'galeria_tour');
INSERT INTO `imagen` VALUES (1091,12,'galeria_tour_12_17.jpg','tour',17,'galeria_tour');
INSERT INTO `imagen` VALUES (1092,12,'logo_12_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1093,12,'galeria_tour_12_18.jpg','tour',18,'galeria_tour');
INSERT INTO `imagen` VALUES (1094,12,'galeria_tour_12_19.jpg','tour',19,'galeria_tour');
INSERT INTO `imagen` VALUES (1095,12,'galeria_tour_12_20.jpg','tour',20,'galeria_tour');
INSERT INTO `imagen` VALUES (1096,12,'galeria_tour_12_21.jpg','tour',21,'galeria_tour');
INSERT INTO `imagen` VALUES (1097,12,'galeria_tour_12_22.jpg','tour',22,'galeria_tour');
INSERT INTO `imagen` VALUES (1106,62,'galeria_tour_62_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1107,62,'galeria_tour_62_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1108,62,'galeria_tour_62_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1109,62,'galeria_tour_62_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1113,62,'galeria_tour_62_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (1114,62,'galeria_tour_62_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (1128,62,'galeria_tour_62_22.jpg','tour',22,'galeria_tour');
INSERT INTO `imagen` VALUES (1129,62,'banner_62_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1140,62,'logo_62_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1143,13,'banner_13_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1145,13,'galeria_tour_13_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1146,13,'galeria_tour_13_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (1147,13,'galeria_tour_13_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (1148,13,'galeria_tour_13_11.jpg','tour',11,'galeria_tour');
INSERT INTO `imagen` VALUES (1149,13,'galeria_tour_13_12.jpg','tour',12,'galeria_tour');
INSERT INTO `imagen` VALUES (1157,7,'banner_7_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1159,7,'galeria_tour_7_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1161,7,'galeria_tour_7_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1162,7,'galeria_tour_7_4.0726e8','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1163,7,'galeria_tour_7_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1164,7,'galeria_tour_7_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1165,7,'galeria_tour_7_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1167,7,'galeria_tour_7_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (1170,7,'logo_7_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1172,8,'icono_8_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1180,32,'banner_32_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1181,32,'logo_32_1.jpeg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1182,32,'galeria_tour_32_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1183,32,'galeria_tour_32_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1184,32,'galeria_tour_32_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1185,32,'galeria_tour_32_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1186,32,'galeria_tour_32_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1187,32,'galeria_tour_32_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1188,32,'galeria_tour_32_7.jpeg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1190,10,'banner_10_1.Secreto','tour',1,'banner');
INSERT INTO `imagen` VALUES (1206,10,'galeria_tour_10_11.jpg','tour',11,'galeria_tour');
INSERT INTO `imagen` VALUES (1207,10,'galeria_tour_10_12.jpg','tour',12,'galeria_tour');
INSERT INTO `imagen` VALUES (1208,10,'galeria_tour_10_13.jpg','tour',13,'galeria_tour');
INSERT INTO `imagen` VALUES (1209,10,'galeria_tour_10_14.jpg','tour',14,'galeria_tour');
INSERT INTO `imagen` VALUES (1210,10,'galeria_tour_10_15.jpg','tour',15,'galeria_tour');
INSERT INTO `imagen` VALUES (1211,10,'galeria_tour_10_16.jpg','tour',16,'galeria_tour');
INSERT INTO `imagen` VALUES (1212,10,'galeria_tour_10_17.jpg','tour',17,'galeria_tour');
INSERT INTO `imagen` VALUES (1228,44,'banner_44_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1229,44,'logo_44_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1230,44,'galeria_tour_44_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1233,44,'galeria_tour_44_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1234,44,'galeria_tour_44_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1235,44,'galeria_tour_44_6.web','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1236,44,'galeria_tour_44_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1245,41,'galeria_tour_41_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1246,41,'galeria_tour_41_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1247,41,'galeria_tour_41_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1248,41,'galeria_tour_41_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1249,41,'galeria_tour_41_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1256,41,'banner_41_1.jpeg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1257,31,'banner_31_1.jpeg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1260,31,'galeria_tour_31_5.png','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1261,31,'galeria_tour_31_6.png','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1263,31,'galeria_tour_31_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1264,10,'logo_10_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1265,72,'banner_72_1.JPG','tour',1,'banner');
INSERT INTO `imagen` VALUES (1266,1,'listado_1_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1290,9,'listado_9_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1291,7,'listado_7_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1292,8,'listado_8_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1293,11,'listado_11_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1294,6,'listado_6_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1295,5,'listado_5_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1297,13,'listado_13_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1299,2,'listado_2_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1301,12,'listado_12_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1302,10,'listado_10_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1303,15,'listado_15_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1304,4,'listado_4_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1305,3,'listado_3_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1307,14,'listado_14_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1308,18,'listado_18_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1310,34,'icono_34_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1311,26,'icono_26_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1318,53,'banner_53_1.png','tour',1,'banner');
INSERT INTO `imagen` VALUES (1319,53,'galeria_tour_53_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1320,53,'galeria_tour_53_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1321,53,'galeria_tour_53_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1322,53,'galeria_tour_53_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1323,53,'galeria_tour_53_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1326,43,'banner_43_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1328,43,'galeria_tour_43_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1330,43,'galeria_tour_43_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1331,43,'galeria_tour_43_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1332,43,'galeria_tour_43_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1333,43,'galeria_tour_43_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1335,78,'banner_78_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1336,78,'logo_78_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1337,78,'galeria_tour_78_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1338,78,'galeria_tour_78_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1339,78,'galeria_tour_78_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1340,78,'galeria_tour_78_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1341,78,'galeria_tour_78_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1342,78,'galeria_tour_78_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1343,78,'galeria_tour_78_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1349,14,'galeria_tour_14_13.jpg','tour',13,'galeria_tour');
INSERT INTO `imagen` VALUES (1356,14,'galeria_tour_14_14.jpg','tour',14,'galeria_tour');
INSERT INTO `imagen` VALUES (1357,14,'banner_14_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1360,14,'galeria_tour_14_16.jpg','tour',16,'galeria_tour');
INSERT INTO `imagen` VALUES (1362,14,'logo_14_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1366,61,'banner_61_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1367,61,'logo_61_1.png','tour',1,'logo');
INSERT INTO `imagen` VALUES (1371,61,'galeria_tour_61_4.png','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1373,61,'galeria_tour_61_6.png','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1375,61,'galeria_tour_61_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1376,61,'galeria_tour_61_9.png','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (1382,61,'galeria_tour_61_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (1383,61,'galeria_tour_61_11.jpg','tour',11,'galeria_tour');
INSERT INTO `imagen` VALUES (1384,61,'galeria_tour_61_12.jpg','tour',12,'galeria_tour');
INSERT INTO `imagen` VALUES (1385,61,'galeria_tour_61_13.jpg','tour',13,'galeria_tour');
INSERT INTO `imagen` VALUES (1386,61,'galeria_tour_61_14.jpg','tour',14,'galeria_tour');
INSERT INTO `imagen` VALUES (1387,61,'galeria_tour_61_15.jpg','tour',15,'galeria_tour');
INSERT INTO `imagen` VALUES (1388,61,'galeria_tour_61_16.jpg','tour',16,'galeria_tour');
INSERT INTO `imagen` VALUES (1389,61,'galeria_tour_61_17.jpg','tour',17,'galeria_tour');
INSERT INTO `imagen` VALUES (1392,45,'banner_45_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1393,48,'banner_48_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1395,47,'banner_47_1.com-58157','tour',1,'banner');
INSERT INTO `imagen` VALUES (1397,46,'banner_46_1.com-58157','tour',1,'banner');
INSERT INTO `imagen` VALUES (1398,9,'banner_9_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1401,50,'banner_50_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1409,50,'galeria_tour_50_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1410,50,'galeria_tour_50_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1411,50,'galeria_tour_50_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1412,50,'galeria_tour_50_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1413,50,'galeria_tour_50_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1414,50,'galeria_tour_50_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1415,49,'galeria_tour_49_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1416,49,'galeria_tour_49_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1417,49,'galeria_tour_49_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1418,49,'galeria_tour_49_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1419,49,'galeria_tour_49_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1421,51,'galeria_tour_51_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1422,51,'galeria_tour_51_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1423,51,'galeria_tour_51_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1426,52,'banner_52_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1427,52,'galeria_tour_52_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1428,52,'galeria_tour_52_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1429,52,'galeria_tour_52_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1430,52,'galeria_tour_52_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1431,52,'galeria_tour_52_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1432,52,'galeria_tour_52_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1436,51,'galeria_tour_51_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1437,51,'banner_51_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1438,49,'banner_49_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1439,51,'galeria_tour_51_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1440,51,'galeria_tour_51_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1442,34,'banner_34_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1443,34,'logo_34_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1444,34,'galeria_tour_34_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1445,34,'galeria_tour_34_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1446,34,'galeria_tour_34_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1447,34,'galeria_tour_34_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1449,34,'galeria_tour_34_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1451,63,'galeria_tour_63_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1452,63,'galeria_tour_63_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1453,63,'galeria_tour_63_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1454,63,'galeria_tour_63_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1455,63,'galeria_tour_63_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1456,63,'galeria_tour_63_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1457,63,'galeria_tour_63_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (1467,63,'galeria_tour_63_20.jpg','tour',20,'galeria_tour');
INSERT INTO `imagen` VALUES (1468,63,'logo_63_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1476,89,'banner_89_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1477,89,'logo_89_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1478,89,'galeria_tour_89_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1479,89,'galeria_tour_89_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1480,89,'galeria_tour_89_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1481,89,'galeria_tour_89_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1482,89,'galeria_tour_89_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1483,89,'galeria_tour_89_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1485,90,'galeria_tour_90_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1486,90,'galeria_tour_90_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1487,90,'galeria_tour_90_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1488,90,'galeria_tour_90_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1489,90,'logo_90_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1490,90,'galeria_tour_90_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1491,90,'galeria_tour_90_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1492,90,'galeria_tour_90_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1493,90,'galeria_tour_90_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1496,23,'galeria_tour_23_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1497,23,'galeria_tour_23_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1498,23,'galeria_tour_23_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1499,23,'galeria_tour_23_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1500,23,'galeria_tour_23_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1501,23,'banner_23_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1502,23,'logo_23_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1503,23,'galeria_tour_23_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1504,23,'galeria_tour_23_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1505,23,'galeria_tour_23_8.jpg','tour',8,'galeria_tour');
INSERT INTO `imagen` VALUES (1506,23,'galeria_tour_23_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (1507,23,'galeria_tour_23_10.jpg','tour',10,'galeria_tour');
INSERT INTO `imagen` VALUES (1515,39,'banner_39_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1516,40,'banner_40_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1559,60,'galeria_tour_60_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1560,60,'galeria_tour_60_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1563,60,'galeria_tour_60_6.jpg','tour',6,'galeria_tour');
INSERT INTO `imagen` VALUES (1576,60,'galeria_tour_60_7.jpg','tour',7,'galeria_tour');
INSERT INTO `imagen` VALUES (1578,60,'galeria_tour_60_9.jpg','tour',9,'galeria_tour');
INSERT INTO `imagen` VALUES (1580,60,'banner_60_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1627,16,'iconito_16_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1629,1,'icono_1_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1630,1,'iconito_1_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1631,9,'icono_9_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1638,16,'banner_16_1.jpg','categoria',1,'banner');
INSERT INTO `imagen` VALUES (1642,7,'iconito_7_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1648,24,'icono_24_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1652,16,'icono_16_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1674,22,'iconito_22_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1682,28,'iconito_28_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1686,25,'iconito_25_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1697,13,'iconito_13_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1700,14,'iconito_14_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1703,24,'iconito_24_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1706,9,'iconito_9_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1712,28,'icono_28_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1719,13,'icono_13_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1721,22,'icono_22_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1725,25,'icono_25_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1726,7,'icono_7_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1728,14,'icono_14_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1729,14,'icono_14_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1733,28,'icono_28_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1736,22,'icono_22_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1737,25,'icono_25_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1738,7,'icono_7_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1741,13,'icono_13_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1747,26,'listado_26_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1748,25,'listado_25_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1752,28,'listado_28_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1753,22,'listado_22_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1758,31,'listado_31_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1762,32,'listado_32_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1766,23,'listado_23_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1767,24,'listado_24_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1768,34,'listado_34_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1772,23,'galeria_23_1.jpg','articulo',1,'galeria');
INSERT INTO `imagen` VALUES (1773,23,'galeria_23_2.jpg','articulo',2,'galeria');
INSERT INTO `imagen` VALUES (1783,28,'galeria_28_1.jpg','articulo',1,'galeria');
INSERT INTO `imagen` VALUES (1784,28,'galeria_28_2.jpg','articulo',2,'galeria');
INSERT INTO `imagen` VALUES (1785,28,'galeria_28_3.jpg','articulo',3,'galeria');
INSERT INTO `imagen` VALUES (1790,28,'principal_28_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1793,25,'principal_25_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1794,24,'principal_24_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1795,24,'galeria_24_4.jpg','articulo',4,'galeria');
INSERT INTO `imagen` VALUES (1796,24,'galeria_24_5.jpg','articulo',5,'galeria');
INSERT INTO `imagen` VALUES (1808,31,'principal_31_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1809,31,'galeria_31_1.jpg','articulo',1,'galeria');
INSERT INTO `imagen` VALUES (1810,31,'galeria_31_2.jpg','articulo',2,'galeria');
INSERT INTO `imagen` VALUES (1818,32,'principal_32_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1824,22,'galeria_22_6.jpg','articulo',6,'galeria');
INSERT INTO `imagen` VALUES (1826,29,'banner_29_1.jpg','categoria',1,'banner');
INSERT INTO `imagen` VALUES (1827,21,'banner_21_1.jpg','categoria',1,'banner');
INSERT INTO `imagen` VALUES (1828,21,'icono_21_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1829,29,'icono_29_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1830,29,'iconito_29_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1831,21,'iconito_21_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1833,34,'principal_34_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1841,36,'icono_36_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1842,36,'iconito_36_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1843,27,'icono_27_1.jpg','categoria',1,'icono');
INSERT INTO `imagen` VALUES (1844,27,'iconito_27_1.jpg','categoria',1,'iconito');
INSERT INTO `imagen` VALUES (1845,26,'galeria_26_7.jpg','articulo',7,'galeria');
INSERT INTO `imagen` VALUES (1846,26,'galeria_26_8.jpg','articulo',8,'galeria');
INSERT INTO `imagen` VALUES (1847,22,'principal_22_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1848,22,'galeria_22_9.jpg','articulo',9,'galeria');
INSERT INTO `imagen` VALUES (1849,22,'galeria_22_10.jpg','articulo',10,'galeria');
INSERT INTO `imagen` VALUES (1850,23,'principal_23_1.JPG','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1851,25,'galeria_25_11.jpg','articulo',11,'galeria');
INSERT INTO `imagen` VALUES (1852,25,'galeria_25_12.jpg','articulo',12,'galeria');
INSERT INTO `imagen` VALUES (1853,25,'galeria_25_13.jpg','articulo',13,'galeria');
INSERT INTO `imagen` VALUES (1854,19,'principal_19_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1855,19,'galeria_19_14.jpg','articulo',14,'galeria');
INSERT INTO `imagen` VALUES (1856,19,'galeria_19_15.jpg','articulo',15,'galeria');
INSERT INTO `imagen` VALUES (1857,19,'galeria_19_16.jpg','articulo',16,'galeria');
INSERT INTO `imagen` VALUES (1858,19,'galeria_19_17.jpg','articulo',17,'galeria');
INSERT INTO `imagen` VALUES (1859,26,'principal_26_1.jpg','articulo',1,'principal');
INSERT INTO `imagen` VALUES (1860,32,'galeria_32_18.jpg','articulo',18,'galeria');
INSERT INTO `imagen` VALUES (1861,32,'galeria_32_19.jpg','articulo',19,'galeria');
INSERT INTO `imagen` VALUES (1862,32,'galeria_32_20.jpg','articulo',20,'galeria');
INSERT INTO `imagen` VALUES (1863,20,'banner_20_1.jpg','destino',1,'banner');
INSERT INTO `imagen` VALUES (1865,27,'banner_27_1.jpg','destino',1,'banner');
INSERT INTO `imagen` VALUES (1866,29,'banner_29_1.jpg','destino',1,'banner');
INSERT INTO `imagen` VALUES (1867,30,'banner_30_1.jpg','destino',1,'banner');
INSERT INTO `imagen` VALUES (1868,30,'listado_30_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1869,20,'listado_20_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1870,21,'banner_21_1.jpg','destino',1,'banner');
INSERT INTO `imagen` VALUES (1871,27,'listado_27_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1872,29,'listado_29_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1873,21,'listado_21_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1874,33,'listado_33_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1875,35,'listado_35_1.jpg','destino',1,'listado');
INSERT INTO `imagen` VALUES (1876,21,'icono_21_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1877,35,'icono_35_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1878,30,'icono_30_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1879,20,'icono_20_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1880,29,'icono_29_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1881,27,'icono_27_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1882,30,'banner_30_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1883,30,'logo_30_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1884,30,'galeria_tour_30_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1885,30,'galeria_tour_30_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1886,30,'galeria_tour_30_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1887,30,'galeria_tour_30_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1888,30,'galeria_tour_30_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1889,37,'banner_37_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1890,37,'logo_37_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1891,37,'galeria_tour_37_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1892,37,'galeria_tour_37_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1894,35,'galeria_tour_35_1.JPG','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1895,35,'galeria_tour_35_2.JPG','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1896,35,'galeria_tour_35_3.JPG','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1897,35,'galeria_tour_35_4.JPG','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1898,29,'banner_29_1.jpg','tour',1,'banner');
INSERT INTO `imagen` VALUES (1900,29,'galeria_tour_29_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1902,29,'galeria_tour_29_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1903,29,'galeria_tour_29_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1904,29,'galeria_tour_29_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1906,27,'galeria_tour_27_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1907,27,'galeria_tour_27_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1908,27,'galeria_tour_27_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1909,27,'galeria_tour_27_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1910,27,'galeria_tour_27_5.jpg','tour',5,'galeria_tour');
INSERT INTO `imagen` VALUES (1911,36,'logo_36_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1912,36,'galeria_tour_36_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1913,36,'galeria_tour_36_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1914,36,'galeria_tour_36_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1915,21,'logo_21_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1916,21,'galeria_tour_21_1.jpg','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1917,21,'galeria_tour_21_2.jpg','tour',2,'galeria_tour');
INSERT INTO `imagen` VALUES (1918,21,'galeria_tour_21_3.jpg','tour',3,'galeria_tour');
INSERT INTO `imagen` VALUES (1919,21,'galeria_tour_21_4.jpg','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1920,27,'logo_27_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1921,35,'logo_35_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1922,29,'logo_29_1.jpg','tour',1,'logo');
INSERT INTO `imagen` VALUES (1923,36,'icono_36_1.png','amenidad',1,'icono');
INSERT INTO `imagen` VALUES (1926,91,'galeria_tour_91_1.png','tour',1,'galeria_tour');
INSERT INTO `imagen` VALUES (1927,91,'galeria_tour_91_4.png','tour',4,'galeria_tour');
INSERT INTO `imagen` VALUES (1937,91,'logo_91_1.jpg','tour',1,'logo');
/*!40000 ALTER TABLE `imagen` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table mayakoba
#

DROP TABLE IF EXISTS `mayakoba`;
CREATE TABLE `mayakoba` (
  `id_mayakoba` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_esp` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `descripcion_esp` text COLLATE utf8_unicode_ci,
  `descripcion_eng` text COLLATE utf8_unicode_ci,
  `tag_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tag_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_eng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_mayakoba`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table mayakoba
#
LOCK TABLES `mayakoba` WRITE;
/*!40000 ALTER TABLE `mayakoba` DISABLE KEYS */;

INSERT INTO `mayakoba` VALUES (1,'OHL Classic at Mayakoba','El entorno �nico de Riviera Maya, entre lagos y la belleza natural y junto con el talento y la competitividad de jugadores en la PGA TOUR.\r\nHacen que el OHL Classic at Mayakoba sea uno de los eventos m�s cautivadores y atractivos del circuito.\r\n\r\n<h3>�Sea  VIP  en  el  OHL  Classic  at  Mayakoba!</h3>\r\n','Set amidst natural waterways and lakes, the Camaleon Golf Course offers a challeng ing 18hole golf course.\r\nThe fairways present wide targets, the greens hold approach shots well and water is present on around half the holes, adding  interest and making strategy key to a successful game.\r\n\r\n<h3>�Become a VIP at the OHL Classic at Mayakoba!</h3>\r\n\r\n','','','classic-mayakoba','OHL Classic at Mayakoba');
/*!40000 ALTER TABLE `mayakoba` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table menu
#

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id_menu` int(5) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `icono` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  `opcional` varchar(40) NOT NULL,
  `status` varchar(4) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

#
# Dumping data for table menu
#
LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;

INSERT INTO `menu` VALUES (1,'Usuarios','<i class=\"fa fa-group\"></i>','','','on',NULL);
INSERT INTO `menu` VALUES (2,'Ubicaciones','<i class=\"fa fa-file-text-o\"></i>','','','on',NULL);
INSERT INTO `menu` VALUES (3,'Traslados','<i class=\"fa fa-wrench\"></i>','','','',NULL);
INSERT INTO `menu` VALUES (4,'Operadores','<i class=\"fa fa-map-marker\"></i>','','','on',1);
INSERT INTO `menu` VALUES (5,'Proveedores','<i class=\"fa fa-building-o\"></i>','','','',NULL);
INSERT INTO `menu` VALUES (6,'Amenidades','<i class=\"fa fa-legal\"></i>','','','',11);
INSERT INTO `menu` VALUES (7,'Cupones','<i class=\"fa fa-tags\"></i>','','','',4);
INSERT INTO `menu` VALUES (8,'Categor�as','<i class=\"fa fa-check-square-o\"></i>','','','',6);
INSERT INTO `menu` VALUES (9,'Circuitos','<i class=\"fa fa-circle-o\"></i>','','','',7);
INSERT INTO `menu` VALUES (10,'Tours','<i class=\"fa fa-file-text-o\"></i>','','','',NULL);
INSERT INTO `menu` VALUES (11,'Reservaciones','<i class=\"fa fa-shopping-cart\"></i>','/reservaciones','','',NULL);
INSERT INTO `menu` VALUES (12,'Actividades','<i class=\"fa fa-group\"></i>','','','',8);
INSERT INTO `menu` VALUES (13,'Correos','<i class=\"fa fa-folder\"></i>','/correos','','',9);
INSERT INTO `menu` VALUES (14,'EstadosFinancieros','<i class=\"fa fa-bar-chart\"></i>','/informes-financieros','','',3);
INSERT INTO `menu` VALUES (15,'Venta de Garage','<i class=\"fa fa-home\"></i>','rentar-vender','','',10);
INSERT INTO `menu` VALUES (16,'Actualizar Contacto','<i class=\"fa fa-user\"></i>','/datos-contacto','','',12);
INSERT INTO `menu` VALUES (17,'Recibos de Pago','<i class=\"fa fa-print\"></i>','/recibo-pago','','',2);
INSERT INTO `menu` VALUES (18,'Tablero de Avisos','<i class=\"fa fa-bars\"></i>','','','',6666);
INSERT INTO `menu` VALUES (65,'Actas  Asambleas','<i class=\"fa fa-file-text-o\"></i>','','','',NULL);
INSERT INTO `menu` VALUES (66,'Tablero de Avisos','<i class=\"fa fa-bars\"></i>','/tablero-avisos','','',5);
INSERT INTO `menu` VALUES (67,'Actualizacion Condominos','','/actualizar-condominos','','',NULL);
INSERT INTO `menu` VALUES (68,'Recibos de Pago','<i class=\"fa fa-file-text-o\"></i>','','','',NULL);
INSERT INTO `menu` VALUES (69,'Tablero de Avisos','','/tablero-avisos','','',NULL);
INSERT INTO `menu` VALUES (70,'Blog','<i class=\"fa fa-files-o\"></i>','','','',NULL);
INSERT INTO `menu` VALUES (71,'Real Estate','<span class=\"typcn typcn-home-outline\"></span>','','','',NULL);
INSERT INTO `menu` VALUES (72,'Hoteles','<i class=\"fa fa-hotel\"></i>','','','',NULL);
INSERT INTO `menu` VALUES (73,'Talento Lead','','/talento-lead','','',NULL);
INSERT INTO `menu` VALUES (74,'Ayuda','<i class=\"fa fa-question\"></i>','/faq','','',NULL);
INSERT INTO `menu` VALUES (75,'Management Lead','','/management-lead','','',NULL);
INSERT INTO `menu` VALUES (76,'Reglamentos Condominios','<i class=\"fa fa-file-text-o\"></i>','','','',NULL);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table menuNivelDos
#

DROP TABLE IF EXISTS `menuNivelDos`;
CREATE TABLE `menuNivelDos` (
  `id_menuNivelDos` int(10) NOT NULL AUTO_INCREMENT,
  `submenu_id` int(10) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `carpeta` varchar(100) NOT NULL,
  PRIMARY KEY (`id_menuNivelDos`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Dumping data for table menuNivelDos
#
LOCK TABLES `menuNivelDos` WRITE;
/*!40000 ALTER TABLE `menuNivelDos` DISABLE KEYS */;

INSERT INTO `menuNivelDos` VALUES (1,25,'Agregar','on','/blog-articulos');
INSERT INTO `menuNivelDos` VALUES (2,25,'Modificar','on','/editar-articulos');
INSERT INTO `menuNivelDos` VALUES (3,26,'Agregar','on','/blog-categorias');
INSERT INTO `menuNivelDos` VALUES (4,26,'Modificar','on','/editar-categorias');
/*!40000 ALTER TABLE `menuNivelDos` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table menuNivelUno
#

DROP TABLE IF EXISTS `menuNivelUno`;
CREATE TABLE `menuNivelUno` (
  `id_menuNivelUno` int(10) NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) NOT NULL,
  `icono` varchar(200) NOT NULL,
  `carpeta` varchar(200) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `status` varchar(10) NOT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menuNivelUno`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

#
# Dumping data for table menuNivelUno
#
LOCK TABLES `menuNivelUno` WRITE;
/*!40000 ALTER TABLE `menuNivelUno` DISABLE KEYS */;

INSERT INTO `menuNivelUno` VALUES (1,8,'<i class=\"fa fa-th-list\"></i>','categorias_blog','Categorias','',NULL);
INSERT INTO `menuNivelUno` VALUES (2,8,'<i class=\"fa fa-th-list\"></i>','subcategorias_blog','Subcategorias','',NULL);
INSERT INTO `menuNivelUno` VALUES (3,8,'<i class=\"fa fa-file-text-o\"></i>','articulos_blog','Articulos','',NULL);
INSERT INTO `menuNivelUno` VALUES (4,8,'<i class=\"fa fa-file-text-o\"></i>','etiquetas','Etiquetas','',NULL);
INSERT INTO `menuNivelUno` VALUES (5,8,'','categorias','Modificar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (6,8,'','alta-categorias','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (7,6,'','altas_amenidades','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (8,72,'','hoteles','Modificar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (9,72,'','alta-hotel','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (10,1,'<i class=\"fa fa-user\"></i>','usuarios','Consulta','on',NULL);
INSERT INTO `menuNivelUno` VALUES (11,1,'<i class=\"fa fa-user-plus\"></i>','altas_usuarios','Alta','on',NULL);
INSERT INTO `menuNivelUno` VALUES (12,1,'<i class=\"fa fa-user\"></i>','tipo_usuario','Perfiles','on',NULL);
INSERT INTO `menuNivelUno` VALUES (13,4,'','alta-operador','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (14,4,'','operadores','Lista','on',NULL);
INSERT INTO `menuNivelUno` VALUES (15,10,'','alta-tour','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (16,10,'','tour','Modificar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (17,12,'','agregar-actividades','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (18,2,'','alta-ubicacion','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (19,2,'','ubicaciones','Listado','on',NULL);
INSERT INTO `menuNivelUno` VALUES (20,6,'','amenidades','Modificar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (21,65,'','actas-asambleas','Modificar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (22,12,'','actividades','Modificar ','on',NULL);
INSERT INTO `menuNivelUno` VALUES (23,68,'','alta-recibo-pago','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (24,68,'','editar-recibo-pago','Modificar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (25,70,'<i class=\"fa fa-file-text\"></i>','','Articulos','on',NULL);
INSERT INTO `menuNivelUno` VALUES (26,70,'<i class=\"fa fa-file-text\"></i>','','Categorias','on',NULL);
INSERT INTO `menuNivelUno` VALUES (27,70,'<i class=\"fa fa-comments\"></i>','comentarios-blog','Comentarios ','on',NULL);
INSERT INTO `menuNivelUno` VALUES (28,71,'','alta-propiedad','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (29,71,'','propiedades','Editar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (30,76,'<i class=\"fa fa-file-text\"></i>','alta-reglamentos','Alta','on',NULL);
INSERT INTO `menuNivelUno` VALUES (31,76,'<i class=\"fa fa-file-text\"></i>','lista-reglamentos','Modificar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (32,7,'','altas-cupon','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (33,7,'','cupones','Modificar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (34,9,'','alta-circuito','Agregar','on',NULL);
INSERT INTO `menuNivelUno` VALUES (35,9,'','circuitos','Modificar','on',NULL);
/*!40000 ALTER TABLE `menuNivelUno` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table mercados
#

DROP TABLE IF EXISTS `mercados`;
CREATE TABLE `mercados` (
  `id_mercado` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_mercado` varchar(255) DEFAULT NULL,
  `status_mercado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_mercado`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Dumping data for table mercados
#
LOCK TABLES `mercados` WRITE;
/*!40000 ALTER TABLE `mercados` DISABLE KEYS */;

INSERT INTO `mercados` VALUES (1,'Hotelero','on');
INSERT INTO `mercados` VALUES (2,'Retail','on');
INSERT INTO `mercados` VALUES (3,'Mixto','on');
INSERT INTO `mercados` VALUES (4,'Maquila / Export','on');
/*!40000 ALTER TABLE `mercados` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table mes
#

DROP TABLE IF EXISTS `mes`;
CREATE TABLE `mes` (
  `id_mes` int(11) NOT NULL AUTO_INCREMENT,
  `mes_no` varchar(11) DEFAULT NULL,
  `mes_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_mes`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Dumping data for table mes
#
LOCK TABLES `mes` WRITE;
/*!40000 ALTER TABLE `mes` DISABLE KEYS */;

/*!40000 ALTER TABLE `mes` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table municipios
#

DROP TABLE IF EXISTS `municipios`;
CREATE TABLE `municipios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `estado_id` int(11) NOT NULL COMMENT 'Relaci�n con estados',
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estado_id` (`estado_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tabla de Municipios de la Republica Mexicana';

#
# Dumping data for table municipios
#
LOCK TABLES `municipios` WRITE;
/*!40000 ALTER TABLE `municipios` DISABLE KEYS */;

/*!40000 ALTER TABLE `municipios` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table operacion
#

DROP TABLE IF EXISTS `operacion`;
CREATE TABLE `operacion` (
  `id_operacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_operacion`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Dumping data for table operacion
#
LOCK TABLES `operacion` WRITE;
/*!40000 ALTER TABLE `operacion` DISABLE KEYS */;

INSERT INTO `operacion` VALUES (1,'Venta','on');
INSERT INTO `operacion` VALUES (2,'Renta','on');
INSERT INTO `operacion` VALUES (3,'Venta / Renta','on');
INSERT INTO `operacion` VALUES (4,'Renta Vacacional','on');
/*!40000 ALTER TABLE `operacion` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table operadores
#

DROP TABLE IF EXISTS `operadores`;
CREATE TABLE `operadores` (
  `id_operador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `no_taxy` int(11) DEFAULT NULL,
  `modelo_taxy` varchar(255) DEFAULT NULL,
  `tipo_operador` int(11) DEFAULT NULL,
  `asignado` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_operador`)
) ENGINE=MyISAM AUTO_INCREMENT=260 DEFAULT CHARSET=latin1;

#
# Dumping data for table operadores
#
LOCK TABLES `operadores` WRITE;
/*!40000 ALTER TABLE `operadores` DISABLE KEYS */;

INSERT INTO `operadores` VALUES (105,'JUAN GABRIEL KANTUN BALAN','on','9841812171','',NULL,918,'',1,'off');
INSERT INTO `operadores` VALUES (106,'WILFRIDO AGUILAR VALERIO ','on','9841820924','',NULL,322,'',1,'off');
INSERT INTO `operadores` VALUES (107,'JORGE MIGUEL KU SUASTE','on','9842172412','',NULL,526,'',1,'off');
INSERT INTO `operadores` VALUES (108,'ANTONIO DE JESUS POOT MARTINES ','on','9841459528','',NULL,799,'',1,'off');
INSERT INTO `operadores` VALUES (109,'HENRRRY MAGNEALI BARABATA','on','9341158801','',NULL,92,'',1,'off');
INSERT INTO `operadores` VALUES (110,'DIDIER OLAGUIVER MAY CITIS','on','9861032498','',NULL,637,'',1,'off');
INSERT INTO `operadores` VALUES (111,'JULIAN SORIANO PERDOMO','on','7828183054','',NULL,682,'',1,'off');
INSERT INTO `operadores` VALUES (112,'JOSE CLEINER YAMA BARZON','on','9841302194','',NULL,668,'',1,'off');
INSERT INTO `operadores` VALUES (113,'LUCIANA CORDOBA BACAB','on','9842176205','',NULL,1021,'',1,'off');
INSERT INTO `operadores` VALUES (114,'GERARDO LOPEZ GONZALEZ','on','9842133180','',NULL,361,'',1,'off');
INSERT INTO `operadores` VALUES (115,'PABLO COP YAMA','on','9831368631','',NULL,800,'',1,'off');
INSERT INTO `operadores` VALUES (116,'RONAL MARIK EK BRAVO','on','9831146888','',NULL,773,'',1,'off');
INSERT INTO `operadores` VALUES (117,'LUIS FERNANDEZ CHE CEN','on','9831147878','',NULL,690,'',1,'off');
INSERT INTO `operadores` VALUES (118,'LUIS ALBERTO DZIB ROMERO','on','9841289133','',NULL,1010,'',1,'off');
INSERT INTO `operadores` VALUES (119,'ISAAC AZAEL MAY HAU','on','9851251473','',NULL,663,'',1,'off');
INSERT INTO `operadores` VALUES (120,'WALTER SALVADOR DE LA CRUZ RODRIGES ','on','9842181475','',NULL,409,'',1,'off');
INSERT INTO `operadores` VALUES (121,'ADRIAN REJON PEREZ','on','9994123435','',NULL,442,'',1,'off');
INSERT INTO `operadores` VALUES (122,'NAHUM SULUB NAVARRO','on','9841429952','',NULL,249,'',1,'off');
INSERT INTO `operadores` VALUES (123,'JULIO CESAR SANCHEZ BARRERA','on','9841882652','',NULL,1040,'',1,'off');
INSERT INTO `operadores` VALUES (125,'APOLONIO MAY DZIB','on','9842073003','',NULL,698,'',1,'off');
INSERT INTO `operadores` VALUES (126,'GERONIMO TUYUB NOH','on','9851158675','',NULL,12,'',1,'off');
INSERT INTO `operadores` VALUES (127,'JOAQUIN ARMIN ESCALANTE','on','9848796110','',NULL,206,'',1,'off');
INSERT INTO `operadores` VALUES (128,'ANGEL BENEDICTO CHIMAL TUK','on','9837007599','',NULL,172,'',1,'off');
INSERT INTO `operadores` VALUES (129,'JUAN GABRIEL CHI RUIZ','on','9841560191','',NULL,175,'',1,'off');
INSERT INTO `operadores` VALUES (130,'OSCAR ARIEL UITZIL ARSEO','on','9991632058','',NULL,453,'',1,'off');
INSERT INTO `operadores` VALUES (131,'FREDERIC REYES DAMA','on','9841059054','',NULL,143,'',1,'off');
INSERT INTO `operadores` VALUES (132,'MARCO ANTONIO PEREZ','on','9841787028','',NULL,867,'',1,'off');
INSERT INTO `operadores` VALUES (133,'MARGARITO CARLOS SANTIAGO','on','9841784635','',NULL,941,'',1,'off');
INSERT INTO `operadores` VALUES (134,'ROBERTO CARLOS SANTIAGO','on','9842386929','',NULL,697,'',1,'off');
INSERT INTO `operadores` VALUES (135,'LUIS ADRIAN GOMES PALACIO','on','9841827258','',NULL,129,'',1,'off');
INSERT INTO `operadores` VALUES (136,'SIXTO MANUEL QUESADA','on','9841658581','',NULL,50,'',1,'off');
INSERT INTO `operadores` VALUES (137,'DIEGO TORRES CALDERON','on','9831389958','',NULL,0,'',1,'off');
INSERT INTO `operadores` VALUES (138,'AURELIO BALAM AY','on','9841412855','',NULL,1020,'',1,'off');
INSERT INTO `operadores` VALUES (139,'MARGARITA LARA SILVESTRE','on','9841831446','',NULL,1048,'',1,'off');
INSERT INTO `operadores` VALUES (140,'URIEL ALEJANDRO BARRERA SANTO','on','9384013443','',NULL,1023,'',1,'off');
INSERT INTO `operadores` VALUES (141,'JOSE AFRAIN EUAN ABAN','on','9851017305','',NULL,1017,'',1,'off');
INSERT INTO `operadores` VALUES (142,'BENJAMIN CAHUICH CHAN','on','9842074622','',NULL,894,'',1,'off');
INSERT INTO `operadores` VALUES (143,'ERICK GEOVANI RAMIREZ POOL','on','9841353363','',NULL,792,'',1,'off');
INSERT INTO `operadores` VALUES (144,'EDUARDO HIPOLITO SANCHEZ BARRERA','on','9992595421','',NULL,576,'',1,'off');
INSERT INTO `operadores` VALUES (145,'DAVID ABIMAEL TEC BALAM','on','9851089221','',NULL,1071,'',1,'off');
INSERT INTO `operadores` VALUES (146,'HECTOR HOGO BARAHONA OLAN','on','9841293378','',NULL,873,'',1,'off');
INSERT INTO `operadores` VALUES (147,'PEDRO NICOLAS LIZAMA BAEZA','on','9841993750','',NULL,804,'',1,'off');
INSERT INTO `operadores` VALUES (148,'RICARDO DE LA ROSA HERNANDEZ','on','9842161460','',NULL,164,'',1,'off');
INSERT INTO `operadores` VALUES (149,'ALDOMAR CANCHE TUYUB','on','9841366627','',NULL,575,'',1,'off');
INSERT INTO `operadores` VALUES (150,'OTILIA ASCENSIO RAMIREZ','on','9321275351','',NULL,437,'',1,'off');
INSERT INTO `operadores` VALUES (151,'AUDOMARO CALDERON PEREZ','on','9961051144','',NULL,548,'',1,'off');
INSERT INTO `operadores` VALUES (152,'ROGELIO HERNANDEZ HERNANDEZ','on','9842169305','',NULL,514,'',1,'off');
INSERT INTO `operadores` VALUES (153,'FRANCISCO DE LA ROSA SI FUENTES','on','9837527885','',NULL,1053,'',1,'off');
INSERT INTO `operadores` VALUES (154,'JUAN VALENTIN PECH YAMA','on','9841684015','',NULL,1051,'',1,'off');
INSERT INTO `operadores` VALUES (155,'JOSE MANUEL MEDINA KUMUL','on','9841878940','',NULL,669,'',1,'off');
INSERT INTO `operadores` VALUES (156,'ELIAS MENDEZ RODRIGUEZ','on','9983931075','',NULL,571,'',1,'off');
INSERT INTO `operadores` VALUES (157,'ONESIMO ALMENDRA RAMON','on','9847458915','',NULL,482,'',1,'off');
INSERT INTO `operadores` VALUES (158,'ARTURO VELAZQUEZ MORALES','on','9841842302','',NULL,656,'',1,'off');
INSERT INTO `operadores` VALUES (159,'ALVARO DZID CHI','on','9848075159','',NULL,56,'',1,'off');
INSERT INTO `operadores` VALUES (160,'GUSTAVO JESUS GOMEZ SANCHEZ','on','9841441877','',NULL,731,'',1,'off');
INSERT INTO `operadores` VALUES (161,'ALEJANDRO AGUILAR GARCIA','on','9841397915','',NULL,790,'',1,'off');
INSERT INTO `operadores` VALUES (162,'ANGEL ENRIQUE CHI CHUC','on','9837003485','',NULL,892,'',1,'off');
INSERT INTO `operadores` VALUES (163,'JOSE RENAN BALLOTE ','on','9841761479','',NULL,39,'',1,'off');
INSERT INTO `operadores` VALUES (164,'JUAN JOSE HERNANDEZ CORTEZ','on','9842419584','',NULL,802,'',1,'off');
INSERT INTO `operadores` VALUES (165,'JOSE GUADALUPE GARCIA','on','9381237305','',NULL,754,'',1,'off');
INSERT INTO `operadores` VALUES (166,'ANTONIO MEX CUXIN ','on','9842091091','',NULL,278,'',1,'off');
INSERT INTO `operadores` VALUES (167,'JOSE ADUARDO PECH ','on','9831844577','',NULL,480,'',1,'off');
INSERT INTO `operadores` VALUES (168,'RODRIGO MARTINEZ ARCHUNDIA','on','9841770694','',NULL,55,'',1,'off');
INSERT INTO `operadores` VALUES (169,'LUIS ANGEL HERNANDEZ CASANOBA','on','9841069752','',NULL,638,'',1,'off');
INSERT INTO `operadores` VALUES (170,'CARLOS REUGIO ESCALANTE MARTINEZ ','on','6648004983','',NULL,944,'',1,'off');
INSERT INTO `operadores` VALUES (171,'ALEJANDRO LEONARDO TAH CIH','on','9841658453','',NULL,378,'',1,'off');
INSERT INTO `operadores` VALUES (172,'IVAN ALBERTO BALLIN CERVERA','on','9841457046','',NULL,47,'',1,'off');
INSERT INTO `operadores` VALUES (173,'JOSE CARLOS PECH GOMEZ','on','9841367757','',NULL,455,'',1,'off');
INSERT INTO `operadores` VALUES (174,'JUAN GABRIEL SANCHEZ BARRERA','on','9993633644','',NULL,1102,'',1,'off');
INSERT INTO `operadores` VALUES (175,'LUIS MIGUEL CHIMAL MAY','on','9841361303','',NULL,349,'',1,'off');
INSERT INTO `operadores` VALUES (176,'EZEQUIEL TUN CHE ','on','9842174005','',NULL,396,'',1,'off');
INSERT INTO `operadores` VALUES (177,'ROBERTO CAUICH COB','on','9842030112','',NULL,454,'',1,'off');
INSERT INTO `operadores` VALUES (178,'LUIS ALBERTO YAM CHAY','on','9851148816','',NULL,784,'',1,'off');
INSERT INTO `operadores` VALUES (179,'ANTONIO TUYUB NOH','on','9841145598','',NULL,0,'',1,'off');
INSERT INTO `operadores` VALUES (180,'ROBERTO MARTIN REYES MELCHOR','on','9841724355','',NULL,433,'',1,'off');
INSERT INTO `operadores` VALUES (181,'CARLOS IGUASU ESCALANTE FLOTA','on','9831804782','',NULL,0,'',1,'off');
INSERT INTO `operadores` VALUES (182,'CARLOS ROBERTO NOVELO CANUL','on','9842172150','',NULL,981,'',1,'off');
INSERT INTO `operadores` VALUES (183,'KEVIN ALEXANDER ALVAREZ SANCHEZ','on','9983968060','',NULL,964,'',1,'off');
INSERT INTO `operadores` VALUES (184,'ADRIAN BORJES VILLA','on','5574327098','',NULL,604,'',1,'off');
INSERT INTO `operadores` VALUES (185,'MARGARITO CHI BALAM','on','9841784635','',NULL,941,'',1,'off');
INSERT INTO `operadores` VALUES (186,'LUIS ARTURO VELASQUES MORALES','on','9841842302','',NULL,656,'',1,'off');
INSERT INTO `operadores` VALUES (187,'JOSE DARIO CANCHE ABAN','on','9842561418','',NULL,127,'',1,'off');
INSERT INTO `operadores` VALUES (188,'SANTIAGO JULIAN CISNERO MARTINES','on','9841799033','',NULL,900,'',1,'off');
INSERT INTO `operadores` VALUES (189,'SANTIAGO ALBARADO PROMOTOR','on','9831645568','',NULL,640,'',1,'off');
INSERT INTO `operadores` VALUES (190,'LORENSO BALAM POC','on','9841408648','',NULL,85,'',1,'off');
INSERT INTO `operadores` VALUES (191,'NOE ALFARO MENDEZ ','on','9841672787','',NULL,175,'',1,'off');
INSERT INTO `operadores` VALUES (192,'BRAULIO CANCHE TUYUB','on','9842027793','',NULL,494,'',1,'off');
INSERT INTO `operadores` VALUES (193,'HEIDI DURAM LAMBERT','on','9841148553','',NULL,212,'',1,'off');
INSERT INTO `operadores` VALUES (194,'JOSE ARCENIO LORIA CAMPOS','on','9841805469','',NULL,298,'',1,'off');
INSERT INTO `operadores` VALUES (195,'GUSTAVO ANGEL YAM CHAY','on','9841816826','',NULL,239,'',1,'off');
INSERT INTO `operadores` VALUES (197,'OSCAR ALEJANDRO SANTO PI�A','on','9838097335','',NULL,0,'',1,'off');
INSERT INTO `operadores` VALUES (198,'RAMON BORGES DZUL','on','9994866683','',NULL,343,'',1,'off');
INSERT INTO `operadores` VALUES (199,'SALVADOR TUK UITZIL','on','9848761531','',NULL,267,'',1,'off');
INSERT INTO `operadores` VALUES (200,'DAVID ROJAS ARSILA','on','9831585708','',NULL,1127,'',1,'off');
INSERT INTO `operadores` VALUES (201,'JOSE CANDELARIO UCAN MAY','on','9831430479','',NULL,312,'',1,'off');
INSERT INTO `operadores` VALUES (202,'DANIEL CRUZ RODRIGUEZ','on','9821048629','',NULL,0,'',1,'off');
INSERT INTO `operadores` VALUES (205,'MARTIN REYES QUETZ','on','9841340499','',NULL,761,'',1,'off');
INSERT INTO `operadores` VALUES (206,'MARIA VICTORIA GONSALEZ RODRIGUEZ','on','9841761385','',NULL,0,'',1,'off');
INSERT INTO `operadores` VALUES (207,'KEVIN ALAIN ECHEVERRIA GONZALEZ','on','9841644556','',NULL,0,'',1,'off');
INSERT INTO `operadores` VALUES (208,'EDUARDO ALBERTO LIMA SANCHEZ','on','9841137147','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (209,'OSCAR MANUEL DIAZ JIMENEZ ','on','5544926821','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (210,'JUAN MANUEL CHIMAL ','on','9848791954','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (212,'HUGO RIVERA ORTEGA','on','5546817363','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (214,'SANTOS RAMON VALDEZ MENDEZ','on','9831488852','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (216,'JESUS ENRIQUE GARCIA ARCOS ','on','9841112983','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (217,'JORGE LUIS BARRIOS CASTA�EDA','on','9841352093','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (218,'SAUL RUEDA RAMIREZ','on','9847454290','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (219,'SANTOS DAMIAN CANCHE POOL','on','9841282053','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (220,'MANUEL ARMIN DZUL DZUL','on','9841067911','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (221,'DIDIER ANDRADE VAZQUEZ MARTIN','on','9841334300','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (222,'REY DAVID AY CIAU','on','9842409937','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (223,'JOSE MARIA SANCHEZ ALTAMIRANO','on','9848770560','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (224,'SAMUEL ORTIZ NEGRETE','on','9841336052','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (225,'ROBERTO MAY ABAM','on','9842046959','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (226,'ELIASAR MORENO LOPEz','on','9831073261','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (227,'GRACIANO CEN MAY','on','9842156128','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (229,'JOSE ROBERTO COHUO CASTILLO ','on','9841069418','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (231,'ALAN GERARDO VALDEZ COUOH','on','9988949039','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (232,'ALVINO ENRIQUE MATEO','on','9837002625','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (233,'EDGAR ISSAC EZQUIVEL VERNAL','on','5521837626','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (234,'DAVID NOE BOJORQUES HERNANDEZ','on','9842166399','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (235,'ELEAZAR MORENO LOPEZ ','on','9831073261','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (236,'DARWIN  ALEXANDER ROSADO GARCIA','on','9341297759','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (237,'RAUL ALFONSO MORALES PACHECO','on','9841347003','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (238,'LUIS ALBERTO VASQUEZ BORGAZ','on','9848044412','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (239,'MAURICIO DE JESUS AGUILAR PONCE','on','9842124159','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (240,'JOSE EZEQUIEL COCOM DZIB','on','9848043071','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (241,'GORJE LUIS VARRIOS CASTA�EDA ','on','9841362093','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (242,'EDDY BALTAZAR PACHECO DZUL','on','9842182093','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (243,'EDUARDO DANIEL REQUENA RAMIREZ','on','9841300254','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (244,'FELIPE JULIO CRUZ CABALLERO','on','9831132844','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (245,'ERICK HERNANDEZ GARCIA','on','9841299372','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (246,'VICTOR HUGO LUNA MENDOZA','on','9831547890','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (247,'ARTURO HERNANDEZ ALCANTARA','on','9931090627','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (248,'JERONIMO TUYUB NOH','on','9851158675','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (249,'HONORATO ABAM PAT','on','9842083179','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (250,'HONORATO ABAM PAT','on','9842083179','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (251,'GEREMIAS MARTINES ARA','on','9841457378','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (252,'RAFAEL MAY COCON','on','9851099714','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (253,'JESUS AVILES RODRIGUES','on','9831142376','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (254,'MANUEL ALEXANDER VARGAS AGUILAR','on','9831621749','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (255,'MANUEL ALEXANDER VARGAS AGUILAR','on','9831621749','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (256,'ROSALINO JIMENES CORDOBA','on','2293432830','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (257,'GABRIEL VARGEZ PUCH','on','9841655805','',NULL,0,'',2,'off');
INSERT INTO `operadores` VALUES (258,'JOSE EFRAIN EUAN ABAN','on','9851017300','',NULL,1017,'',1,'off');
INSERT INTO `operadores` VALUES (259,'ERICK FRANCISCO DIAZ ORTIZ','on','9841765292','',NULL,0,'',2,'off');
/*!40000 ALTER TABLE `operadores` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table operadoresc
#

DROP TABLE IF EXISTS `operadoresc`;
CREATE TABLE `operadoresc` (
  `id_operadorc` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `telefono` varchar(500) DEFAULT NULL,
  `email` varchar(800) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `no_taxy` int(11) DEFAULT NULL,
  `modelo_taxy` varchar(255) DEFAULT NULL,
  `tipo_operador` int(11) DEFAULT NULL,
  `correo_contacto_principal` varchar(255) DEFAULT NULL,
  `telefono_ventas` varchar(255) DEFAULT NULL,
  `correo_contabilidad` varchar(255) DEFAULT NULL,
  `telefono_contabilidad` varchar(255) DEFAULT NULL,
  `telefono_operaciones` varchar(255) DEFAULT NULL,
  `nombre_personal_clave` varchar(255) DEFAULT NULL,
  `correo_personal_clave` varchar(255) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_operadorc`)
) ENGINE=MyISAM AUTO_INCREMENT=150 DEFAULT CHARSET=latin1;

#
# Dumping data for table operadoresc
#
LOCK TABLES `operadoresc` WRITE;
/*!40000 ALTER TABLE `operadoresc` DISABLE KEYS */;

/*!40000 ALTER TABLE `operadoresc` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table propiedad
#

DROP TABLE IF EXISTS `propiedad`;
CREATE TABLE `propiedad` (
  `id_propiedad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_propiedad` varchar(600) DEFAULT NULL,
  `tipo_propiedad` int(3) DEFAULT NULL,
  `precio_propiedad` varchar(255) DEFAULT NULL,
  `moneda` int(3) DEFAULT NULL,
  `operacion` int(3) DEFAULT NULL,
  `recamaras` decimal(10,2) DEFAULT NULL,
  `banios` decimal(10,2) DEFAULT NULL,
  `area_terreno` decimal(10,2) DEFAULT NULL,
  `area_construccion` decimal(10,2) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `status` varchar(3) DEFAULT NULL,
  `tag_title` varchar(255) DEFAULT NULL,
  `tag_description` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `destacada` varchar(3) DEFAULT NULL,
  `nombre_encargado` varchar(300) DEFAULT NULL,
  `telefono_encargado` varchar(50) DEFAULT NULL,
  `correo_encargado` varchar(50) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_propiedad`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

#
# Dumping data for table propiedad
#
LOCK TABLES `propiedad` WRITE;
/*!40000 ALTER TABLE `propiedad` DISABLE KEYS */;

INSERT INTO `propiedad` VALUES (2,'Hermosa Residencia en Cancun',2,'1,200,000.00',1,1,5,3.5,1500,900,23,'Cancun','Hermosa residencia en Cancun . Excelente ubicaci�n. ','on','','Residencia de Lujo en Cancun','casas-venta-cancun','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1606');
INSERT INTO `propiedad` VALUES (5,'Residencia en Playa del Carmen',2,'1,700,000.00',1,1,5,4,2500,1200,23,'Playa del Carmen','Hermosa Residencia en Playa del Carmen','on','','Adquiere esta bella propiedad. Es tu mejor inversi�n.','casas-venta-playa-del-carmen-mexico','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1609');
INSERT INTO `propiedad` VALUES (6,'Hermosa Residencia en M�rida',2,'1,000,000.00',1,4,6,4,2000,950,31,'Merida','Hermosa Residencia en M�rida','on','','Adquiere esta propiedad. Urge venderla.','casas-venta-merida-mexico','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1605');
INSERT INTO `propiedad` VALUES (7,'Casa de Playa en Canc�n',2,'1,000,000.00',1,1,5,3.5,1000,850,23,'Canc�n','Hermosa Propiedad. Estr�nela hoy mismo!','on','','Casas en Venta y Renta en Cancun','casas-venta-cancun-mexico','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1602');
INSERT INTO `propiedad` VALUES (8,'Gran Oportunidad de Inversion',8,'1,500,000.00',1,1,3,2.5,450,300,15,'Toluca','','on','','Adquiera esta inversion','casa-venta-toluca','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1608');
INSERT INTO `propiedad` VALUES (9,'Casa en Renta en Canc�n',8,'1,500,000.00',1,1,4,3,300,250,23,'Cancun','','on','','Renta de Casa en Canc�n. Todos los servicios incluidos.','renta-casa-cancun','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1607');
INSERT INTO `propiedad` VALUES (10,'Casa Renta Vacacional',2,'1,000,000.00',1,3,4,2.5,520,350,23,'Cancun','Casa para Renta Vacacional en Canc�n','on','','Casas para Renta Vacacional en Cancun','renta-vacacional-cancun','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1603');
INSERT INTO `propiedad` VALUES (11,'Gran Oportunidad de Inversion',5,'1,000,000.00',1,3,4,3,2000,1000,23,'Cancun','Oportunidad en Preventa. espacios Comerciales disponibles con todos los servicios.','on','','Locales Comerciales en Cancun','locales-comerciales-cancun','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1604');
INSERT INTO `propiedad` VALUES (12,'Tu Casa en Venta en este Espacio',2,'0.00',1,1,3,2,500,350,23,'Canc�n','Este espacio esta reservado para ti','on','','Casas en Renta y Venta en Jalisco','casas-renta-venta-quintana-roo','on','Marta Saborido Rebes','9981923103','hcedillo@fibmex.com','CUN1601');
/*!40000 ALTER TABLE `propiedad` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table r_actividad_tour
#

DROP TABLE IF EXISTS `r_actividad_tour`;
CREATE TABLE `r_actividad_tour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) DEFAULT NULL,
  `actividad_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3922 DEFAULT CHARSET=latin1;

#
# Dumping data for table r_actividad_tour
#
LOCK TABLES `r_actividad_tour` WRITE;
/*!40000 ALTER TABLE `r_actividad_tour` DISABLE KEYS */;

INSERT INTO `r_actividad_tour` VALUES (11,5,13);
INSERT INTO `r_actividad_tour` VALUES (111,28,13);
INSERT INTO `r_actividad_tour` VALUES (216,42,20);
INSERT INTO `r_actividad_tour` VALUES (2974,83,4);
INSERT INTO `r_actividad_tour` VALUES (2975,83,7);
INSERT INTO `r_actividad_tour` VALUES (2976,83,1);
INSERT INTO `r_actividad_tour` VALUES (3175,52,31);
INSERT INTO `r_actividad_tour` VALUES (3176,52,32);
INSERT INTO `r_actividad_tour` VALUES (3404,77,15);
INSERT INTO `r_actividad_tour` VALUES (3405,77,19);
INSERT INTO `r_actividad_tour` VALUES (3406,58,24);
INSERT INTO `r_actividad_tour` VALUES (3407,38,7);
INSERT INTO `r_actividad_tour` VALUES (3408,38,11);
INSERT INTO `r_actividad_tour` VALUES (3409,38,15);
INSERT INTO `r_actividad_tour` VALUES (3410,38,10);
INSERT INTO `r_actividad_tour` VALUES (3411,38,19);
INSERT INTO `r_actividad_tour` VALUES (3414,39,7);
INSERT INTO `r_actividad_tour` VALUES (3415,39,15);
INSERT INTO `r_actividad_tour` VALUES (3416,40,7);
INSERT INTO `r_actividad_tour` VALUES (3419,51,16);
INSERT INTO `r_actividad_tour` VALUES (3426,43,15);
INSERT INTO `r_actividad_tour` VALUES (3427,43,16);
INSERT INTO `r_actividad_tour` VALUES (3428,15,11);
INSERT INTO `r_actividad_tour` VALUES (3429,15,15);
INSERT INTO `r_actividad_tour` VALUES (3430,33,15);
INSERT INTO `r_actividad_tour` VALUES (3431,45,31);
INSERT INTO `r_actividad_tour` VALUES (3432,45,16);
INSERT INTO `r_actividad_tour` VALUES (3435,48,31);
INSERT INTO `r_actividad_tour` VALUES (3436,48,16);
INSERT INTO `r_actividad_tour` VALUES (3437,47,31);
INSERT INTO `r_actividad_tour` VALUES (3438,47,16);
INSERT INTO `r_actividad_tour` VALUES (3439,46,31);
INSERT INTO `r_actividad_tour` VALUES (3440,46,16);
INSERT INTO `r_actividad_tour` VALUES (3443,41,2);
INSERT INTO `r_actividad_tour` VALUES (3444,41,15);
INSERT INTO `r_actividad_tour` VALUES (3445,41,8);
INSERT INTO `r_actividad_tour` VALUES (3446,31,2);
INSERT INTO `r_actividad_tour` VALUES (3447,31,15);
INSERT INTO `r_actividad_tour` VALUES (3448,31,8);
INSERT INTO `r_actividad_tour` VALUES (3449,19,11);
INSERT INTO `r_actividad_tour` VALUES (3450,19,2);
INSERT INTO `r_actividad_tour` VALUES (3451,19,15);
INSERT INTO `r_actividad_tour` VALUES (3452,19,3);
INSERT INTO `r_actividad_tour` VALUES (3456,11,2);
INSERT INTO `r_actividad_tour` VALUES (3459,72,15);
INSERT INTO `r_actividad_tour` VALUES (3460,44,11);
INSERT INTO `r_actividad_tour` VALUES (3461,44,15);
INSERT INTO `r_actividad_tour` VALUES (3462,44,22);
INSERT INTO `r_actividad_tour` VALUES (3463,44,21);
INSERT INTO `r_actividad_tour` VALUES (3470,79,11);
INSERT INTO `r_actividad_tour` VALUES (3471,79,29);
INSERT INTO `r_actividad_tour` VALUES (3472,79,2);
INSERT INTO `r_actividad_tour` VALUES (3473,79,15);
INSERT INTO `r_actividad_tour` VALUES (3474,79,10);
INSERT INTO `r_actividad_tour` VALUES (3475,79,3);
INSERT INTO `r_actividad_tour` VALUES (3481,69,19);
INSERT INTO `r_actividad_tour` VALUES (3482,73,11);
INSERT INTO `r_actividad_tour` VALUES (3483,73,15);
INSERT INTO `r_actividad_tour` VALUES (3484,73,18);
INSERT INTO `r_actividad_tour` VALUES (3485,73,21);
INSERT INTO `r_actividad_tour` VALUES (3494,66,26);
INSERT INTO `r_actividad_tour` VALUES (3495,66,15);
INSERT INTO `r_actividad_tour` VALUES (3496,66,19);
INSERT INTO `r_actividad_tour` VALUES (3497,9,16);
INSERT INTO `r_actividad_tour` VALUES (3499,60,27);
INSERT INTO `r_actividad_tour` VALUES (3500,60,29);
INSERT INTO `r_actividad_tour` VALUES (3515,76,29);
INSERT INTO `r_actividad_tour` VALUES (3516,76,21);
INSERT INTO `r_actividad_tour` VALUES (3517,76,34);
INSERT INTO `r_actividad_tour` VALUES (3518,23,4);
INSERT INTO `r_actividad_tour` VALUES (3519,61,30);
INSERT INTO `r_actividad_tour` VALUES (3522,59,11);
INSERT INTO `r_actividad_tour` VALUES (3523,59,26);
INSERT INTO `r_actividad_tour` VALUES (3524,59,15);
INSERT INTO `r_actividad_tour` VALUES (3525,59,22);
INSERT INTO `r_actividad_tour` VALUES (3526,16,15);
INSERT INTO `r_actividad_tour` VALUES (3527,16,22);
INSERT INTO `r_actividad_tour` VALUES (3528,17,11);
INSERT INTO `r_actividad_tour` VALUES (3529,17,5);
INSERT INTO `r_actividad_tour` VALUES (3530,17,15);
INSERT INTO `r_actividad_tour` VALUES (3531,17,22);
INSERT INTO `r_actividad_tour` VALUES (3532,17,10);
INSERT INTO `r_actividad_tour` VALUES (3533,17,19);
INSERT INTO `r_actividad_tour` VALUES (3534,17,3);
INSERT INTO `r_actividad_tour` VALUES (3541,12,11);
INSERT INTO `r_actividad_tour` VALUES (3542,12,15);
INSERT INTO `r_actividad_tour` VALUES (3543,12,18);
INSERT INTO `r_actividad_tour` VALUES (3544,12,10);
INSERT INTO `r_actividad_tour` VALUES (3545,12,19);
INSERT INTO `r_actividad_tour` VALUES (3546,12,3);
INSERT INTO `r_actividad_tour` VALUES (3547,6,11);
INSERT INTO `r_actividad_tour` VALUES (3548,6,29);
INSERT INTO `r_actividad_tour` VALUES (3549,6,5);
INSERT INTO `r_actividad_tour` VALUES (3550,6,15);
INSERT INTO `r_actividad_tour` VALUES (3551,6,17);
INSERT INTO `r_actividad_tour` VALUES (3552,6,30);
INSERT INTO `r_actividad_tour` VALUES (3553,6,10);
INSERT INTO `r_actividad_tour` VALUES (3554,6,21);
INSERT INTO `r_actividad_tour` VALUES (3555,6,16);
INSERT INTO `r_actividad_tour` VALUES (3556,6,19);
INSERT INTO `r_actividad_tour` VALUES (3557,53,11);
INSERT INTO `r_actividad_tour` VALUES (3558,53,15);
INSERT INTO `r_actividad_tour` VALUES (3559,53,17);
INSERT INTO `r_actividad_tour` VALUES (3560,53,18);
INSERT INTO `r_actividad_tour` VALUES (3561,53,19);
INSERT INTO `r_actividad_tour` VALUES (3562,53,3);
INSERT INTO `r_actividad_tour` VALUES (3563,78,31);
INSERT INTO `r_actividad_tour` VALUES (3564,78,32);
INSERT INTO `r_actividad_tour` VALUES (3574,54,2);
INSERT INTO `r_actividad_tour` VALUES (3575,54,15);
INSERT INTO `r_actividad_tour` VALUES (3576,54,17);
INSERT INTO `r_actividad_tour` VALUES (3577,54,10);
INSERT INTO `r_actividad_tour` VALUES (3578,54,3);
INSERT INTO `r_actividad_tour` VALUES (3579,68,3);
INSERT INTO `r_actividad_tour` VALUES (3605,24,11);
INSERT INTO `r_actividad_tour` VALUES (3606,24,15);
INSERT INTO `r_actividad_tour` VALUES (3607,24,18);
INSERT INTO `r_actividad_tour` VALUES (3608,24,10);
INSERT INTO `r_actividad_tour` VALUES (3609,24,19);
INSERT INTO `r_actividad_tour` VALUES (3610,24,3);
INSERT INTO `r_actividad_tour` VALUES (3613,32,11);
INSERT INTO `r_actividad_tour` VALUES (3614,32,15);
INSERT INTO `r_actividad_tour` VALUES (3615,32,17);
INSERT INTO `r_actividad_tour` VALUES (3616,32,19);
INSERT INTO `r_actividad_tour` VALUES (3617,32,3);
INSERT INTO `r_actividad_tour` VALUES (3620,49,16);
INSERT INTO `r_actividad_tour` VALUES (3621,50,16);
INSERT INTO `r_actividad_tour` VALUES (3622,70,15);
INSERT INTO `r_actividad_tour` VALUES (3623,70,19);
INSERT INTO `r_actividad_tour` VALUES (3624,34,31);
INSERT INTO `r_actividad_tour` VALUES (3625,34,32);
INSERT INTO `r_actividad_tour` VALUES (3629,14,11);
INSERT INTO `r_actividad_tour` VALUES (3630,14,15);
INSERT INTO `r_actividad_tour` VALUES (3631,14,22);
INSERT INTO `r_actividad_tour` VALUES (3632,30,16);
INSERT INTO `r_actividad_tour` VALUES (3633,37,15);
INSERT INTO `r_actividad_tour` VALUES (3641,36,11);
INSERT INTO `r_actividad_tour` VALUES (3642,36,2);
INSERT INTO `r_actividad_tour` VALUES (3643,36,3);
INSERT INTO `r_actividad_tour` VALUES (3644,21,11);
INSERT INTO `r_actividad_tour` VALUES (3645,21,2);
INSERT INTO `r_actividad_tour` VALUES (3646,27,31);
INSERT INTO `r_actividad_tour` VALUES (3647,27,16);
INSERT INTO `r_actividad_tour` VALUES (3648,35,19);
INSERT INTO `r_actividad_tour` VALUES (3649,29,2);
INSERT INTO `r_actividad_tour` VALUES (3650,29,17);
INSERT INTO `r_actividad_tour` VALUES (3651,29,10);
INSERT INTO `r_actividad_tour` VALUES (3652,29,3);
INSERT INTO `r_actividad_tour` VALUES (3691,13,11);
INSERT INTO `r_actividad_tour` VALUES (3692,13,15);
INSERT INTO `r_actividad_tour` VALUES (3693,13,17);
INSERT INTO `r_actividad_tour` VALUES (3694,13,18);
INSERT INTO `r_actividad_tour` VALUES (3695,13,3);
INSERT INTO `r_actividad_tour` VALUES (3731,90,11);
INSERT INTO `r_actividad_tour` VALUES (3732,90,2);
INSERT INTO `r_actividad_tour` VALUES (3733,90,15);
INSERT INTO `r_actividad_tour` VALUES (3734,90,17);
INSERT INTO `r_actividad_tour` VALUES (3735,90,22);
INSERT INTO `r_actividad_tour` VALUES (3736,90,10);
INSERT INTO `r_actividad_tour` VALUES (3737,90,16);
INSERT INTO `r_actividad_tour` VALUES (3738,90,19);
INSERT INTO `r_actividad_tour` VALUES (3745,89,11);
INSERT INTO `r_actividad_tour` VALUES (3746,89,27);
INSERT INTO `r_actividad_tour` VALUES (3747,89,15);
INSERT INTO `r_actividad_tour` VALUES (3748,89,30);
INSERT INTO `r_actividad_tour` VALUES (3749,89,22);
INSERT INTO `r_actividad_tour` VALUES (3848,63,11);
INSERT INTO `r_actividad_tour` VALUES (3849,63,29);
INSERT INTO `r_actividad_tour` VALUES (3850,63,15);
INSERT INTO `r_actividad_tour` VALUES (3851,63,17);
INSERT INTO `r_actividad_tour` VALUES (3852,63,22);
INSERT INTO `r_actividad_tour` VALUES (3853,63,19);
INSERT INTO `r_actividad_tour` VALUES (3913,91,11);
INSERT INTO `r_actividad_tour` VALUES (3914,91,27);
INSERT INTO `r_actividad_tour` VALUES (3915,91,29);
INSERT INTO `r_actividad_tour` VALUES (3916,91,15);
INSERT INTO `r_actividad_tour` VALUES (3917,91,17);
INSERT INTO `r_actividad_tour` VALUES (3918,91,30);
INSERT INTO `r_actividad_tour` VALUES (3919,91,22);
INSERT INTO `r_actividad_tour` VALUES (3920,91,19);
INSERT INTO `r_actividad_tour` VALUES (3921,91,3);
/*!40000 ALTER TABLE `r_actividad_tour` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table r_aldea_tour
#

DROP TABLE IF EXISTS `r_aldea_tour`;
CREATE TABLE `r_aldea_tour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tour_id` int(11) DEFAULT NULL,
  `aldea_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2881 DEFAULT CHARSET=latin1;

#
# Dumping data for table r_aldea_tour
#
LOCK TABLES `r_aldea_tour` WRITE;
/*!40000 ALTER TABLE `r_aldea_tour` DISABLE KEYS */;

INSERT INTO `r_aldea_tour` VALUES (2173,83,1);
INSERT INTO `r_aldea_tour` VALUES (2174,83,9);
INSERT INTO `r_aldea_tour` VALUES (2407,52,1);
INSERT INTO `r_aldea_tour` VALUES (2408,52,12);
INSERT INTO `r_aldea_tour` VALUES (2550,90,1);
INSERT INTO `r_aldea_tour` VALUES (2551,90,9);
INSERT INTO `r_aldea_tour` VALUES (2552,90,7);
INSERT INTO `r_aldea_tour` VALUES (2553,90,10);
INSERT INTO `r_aldea_tour` VALUES (2554,90,4);
INSERT INTO `r_aldea_tour` VALUES (2580,77,20);
INSERT INTO `r_aldea_tour` VALUES (2581,77,28);
INSERT INTO `r_aldea_tour` VALUES (2582,58,21);
INSERT INTO `r_aldea_tour` VALUES (2583,38,20);
INSERT INTO `r_aldea_tour` VALUES (2584,38,23);
INSERT INTO `r_aldea_tour` VALUES (2585,38,35);
INSERT INTO `r_aldea_tour` VALUES (2588,39,20);
INSERT INTO `r_aldea_tour` VALUES (2589,39,23);
INSERT INTO `r_aldea_tour` VALUES (2590,40,20);
INSERT INTO `r_aldea_tour` VALUES (2591,40,21);
INSERT INTO `r_aldea_tour` VALUES (2592,40,23);
INSERT INTO `r_aldea_tour` VALUES (2595,51,20);
INSERT INTO `r_aldea_tour` VALUES (2601,22,26);
INSERT INTO `r_aldea_tour` VALUES (2602,22,35);
INSERT INTO `r_aldea_tour` VALUES (2605,43,26);
INSERT INTO `r_aldea_tour` VALUES (2606,43,35);
INSERT INTO `r_aldea_tour` VALUES (2607,15,26);
INSERT INTO `r_aldea_tour` VALUES (2608,15,35);
INSERT INTO `r_aldea_tour` VALUES (2609,33,25);
INSERT INTO `r_aldea_tour` VALUES (2610,45,20);
INSERT INTO `r_aldea_tour` VALUES (2611,45,30);
INSERT INTO `r_aldea_tour` VALUES (2614,48,20);
INSERT INTO `r_aldea_tour` VALUES (2615,48,30);
INSERT INTO `r_aldea_tour` VALUES (2616,47,20);
INSERT INTO `r_aldea_tour` VALUES (2617,47,30);
INSERT INTO `r_aldea_tour` VALUES (2618,46,20);
INSERT INTO `r_aldea_tour` VALUES (2619,46,30);
INSERT INTO `r_aldea_tour` VALUES (2621,41,20);
INSERT INTO `r_aldea_tour` VALUES (2622,41,30);
INSERT INTO `r_aldea_tour` VALUES (2623,41,32);
INSERT INTO `r_aldea_tour` VALUES (2624,41,23);
INSERT INTO `r_aldea_tour` VALUES (2625,31,20);
INSERT INTO `r_aldea_tour` VALUES (2626,31,30);
INSERT INTO `r_aldea_tour` VALUES (2627,31,32);
INSERT INTO `r_aldea_tour` VALUES (2628,31,23);
INSERT INTO `r_aldea_tour` VALUES (2629,19,20);
INSERT INTO `r_aldea_tour` VALUES (2630,19,30);
INSERT INTO `r_aldea_tour` VALUES (2631,19,32);
INSERT INTO `r_aldea_tour` VALUES (2632,19,23);
INSERT INTO `r_aldea_tour` VALUES (2637,11,20);
INSERT INTO `r_aldea_tour` VALUES (2638,11,30);
INSERT INTO `r_aldea_tour` VALUES (2639,11,32);
INSERT INTO `r_aldea_tour` VALUES (2640,11,23);
INSERT INTO `r_aldea_tour` VALUES (2645,72,20);
INSERT INTO `r_aldea_tour` VALUES (2646,72,30);
INSERT INTO `r_aldea_tour` VALUES (2647,72,32);
INSERT INTO `r_aldea_tour` VALUES (2648,72,23);
INSERT INTO `r_aldea_tour` VALUES (2649,44,27);
INSERT INTO `r_aldea_tour` VALUES (2650,44,35);
INSERT INTO `r_aldea_tour` VALUES (2655,79,20);
INSERT INTO `r_aldea_tour` VALUES (2656,79,30);
INSERT INTO `r_aldea_tour` VALUES (2657,79,32);
INSERT INTO `r_aldea_tour` VALUES (2658,79,23);
INSERT INTO `r_aldea_tour` VALUES (2663,69,21);
INSERT INTO `r_aldea_tour` VALUES (2664,73,35);
INSERT INTO `r_aldea_tour` VALUES (2670,66,20);
INSERT INTO `r_aldea_tour` VALUES (2671,66,22);
INSERT INTO `r_aldea_tour` VALUES (2672,9,20);
INSERT INTO `r_aldea_tour` VALUES (2674,60,30);
INSERT INTO `r_aldea_tour` VALUES (2675,60,32);
INSERT INTO `r_aldea_tour` VALUES (2676,60,23);
INSERT INTO `r_aldea_tour` VALUES (2677,10,20);
INSERT INTO `r_aldea_tour` VALUES (2678,10,30);
INSERT INTO `r_aldea_tour` VALUES (2679,10,32);
INSERT INTO `r_aldea_tour` VALUES (2680,10,23);
INSERT INTO `r_aldea_tour` VALUES (2681,55,20);
INSERT INTO `r_aldea_tour` VALUES (2682,55,22);
INSERT INTO `r_aldea_tour` VALUES (2695,76,20);
INSERT INTO `r_aldea_tour` VALUES (2696,76,30);
INSERT INTO `r_aldea_tour` VALUES (2697,76,32);
INSERT INTO `r_aldea_tour` VALUES (2698,76,23);
INSERT INTO `r_aldea_tour` VALUES (2699,23,20);
INSERT INTO `r_aldea_tour` VALUES (2700,61,32);
INSERT INTO `r_aldea_tour` VALUES (2701,61,23);
INSERT INTO `r_aldea_tour` VALUES (2702,61,35);
INSERT INTO `r_aldea_tour` VALUES (2705,59,30);
INSERT INTO `r_aldea_tour` VALUES (2706,59,23);
INSERT INTO `r_aldea_tour` VALUES (2707,59,24);
INSERT INTO `r_aldea_tour` VALUES (2708,16,24);
INSERT INTO `r_aldea_tour` VALUES (2709,17,23);
INSERT INTO `r_aldea_tour` VALUES (2710,17,24);
INSERT INTO `r_aldea_tour` VALUES (2712,12,24);
INSERT INTO `r_aldea_tour` VALUES (2713,6,20);
INSERT INTO `r_aldea_tour` VALUES (2714,6,30);
INSERT INTO `r_aldea_tour` VALUES (2715,6,32);
INSERT INTO `r_aldea_tour` VALUES (2716,6,23);
INSERT INTO `r_aldea_tour` VALUES (2717,7,20);
INSERT INTO `r_aldea_tour` VALUES (2718,7,30);
INSERT INTO `r_aldea_tour` VALUES (2719,7,32);
INSERT INTO `r_aldea_tour` VALUES (2720,7,23);
INSERT INTO `r_aldea_tour` VALUES (2721,53,20);
INSERT INTO `r_aldea_tour` VALUES (2722,53,30);
INSERT INTO `r_aldea_tour` VALUES (2723,53,32);
INSERT INTO `r_aldea_tour` VALUES (2724,53,23);
INSERT INTO `r_aldea_tour` VALUES (2725,78,20);
INSERT INTO `r_aldea_tour` VALUES (2726,78,30);
INSERT INTO `r_aldea_tour` VALUES (2727,78,32);
INSERT INTO `r_aldea_tour` VALUES (2728,78,23);
INSERT INTO `r_aldea_tour` VALUES (2737,54,20);
INSERT INTO `r_aldea_tour` VALUES (2738,54,30);
INSERT INTO `r_aldea_tour` VALUES (2739,54,32);
INSERT INTO `r_aldea_tour` VALUES (2740,54,23);
INSERT INTO `r_aldea_tour` VALUES (2741,68,21);
INSERT INTO `r_aldea_tour` VALUES (2746,24,20);
INSERT INTO `r_aldea_tour` VALUES (2747,24,30);
INSERT INTO `r_aldea_tour` VALUES (2748,24,32);
INSERT INTO `r_aldea_tour` VALUES (2749,24,23);
INSERT INTO `r_aldea_tour` VALUES (2752,32,35);
INSERT INTO `r_aldea_tour` VALUES (2755,49,20);
INSERT INTO `r_aldea_tour` VALUES (2756,50,20);
INSERT INTO `r_aldea_tour` VALUES (2757,70,21);
INSERT INTO `r_aldea_tour` VALUES (2758,34,20);
INSERT INTO `r_aldea_tour` VALUES (2761,14,26);
INSERT INTO `r_aldea_tour` VALUES (2762,14,35);
INSERT INTO `r_aldea_tour` VALUES (2763,30,20);
INSERT INTO `r_aldea_tour` VALUES (2764,37,20);
INSERT INTO `r_aldea_tour` VALUES (2773,36,20);
INSERT INTO `r_aldea_tour` VALUES (2774,36,30);
INSERT INTO `r_aldea_tour` VALUES (2775,36,32);
INSERT INTO `r_aldea_tour` VALUES (2776,36,23);
INSERT INTO `r_aldea_tour` VALUES (2777,21,20);
INSERT INTO `r_aldea_tour` VALUES (2778,21,30);
INSERT INTO `r_aldea_tour` VALUES (2779,21,32);
INSERT INTO `r_aldea_tour` VALUES (2780,21,23);
INSERT INTO `r_aldea_tour` VALUES (2781,27,20);
INSERT INTO `r_aldea_tour` VALUES (2782,27,30);
INSERT INTO `r_aldea_tour` VALUES (2783,35,20);
INSERT INTO `r_aldea_tour` VALUES (2784,35,22);
INSERT INTO `r_aldea_tour` VALUES (2785,29,20);
INSERT INTO `r_aldea_tour` VALUES (2786,29,30);
INSERT INTO `r_aldea_tour` VALUES (2787,29,32);
INSERT INTO `r_aldea_tour` VALUES (2788,29,23);
INSERT INTO `r_aldea_tour` VALUES (2793,13,20);
INSERT INTO `r_aldea_tour` VALUES (2794,13,25);
INSERT INTO `r_aldea_tour` VALUES (2795,13,23);
INSERT INTO `r_aldea_tour` VALUES (2796,13,35);
INSERT INTO `r_aldea_tour` VALUES (2806,89,28);
INSERT INTO `r_aldea_tour` VALUES (2807,89,35);
INSERT INTO `r_aldea_tour` VALUES (2830,63,29);
INSERT INTO `r_aldea_tour` VALUES (2831,63,35);
INSERT INTO `r_aldea_tour` VALUES (2874,91,20);
INSERT INTO `r_aldea_tour` VALUES (2875,91,26);
INSERT INTO `r_aldea_tour` VALUES (2876,91,25);
INSERT INTO `r_aldea_tour` VALUES (2877,91,27);
INSERT INTO `r_aldea_tour` VALUES (2878,91,29);
INSERT INTO `r_aldea_tour` VALUES (2879,91,24);
INSERT INTO `r_aldea_tour` VALUES (2880,91,35);
/*!40000 ALTER TABLE `r_aldea_tour` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table r_amenidad_tour
#

DROP TABLE IF EXISTS `r_amenidad_tour`;
CREATE TABLE `r_amenidad_tour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amenidad_id` int(11) DEFAULT NULL,
  `tour_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6146 DEFAULT CHARSET=latin1;

#
# Dumping data for table r_amenidad_tour
#
LOCK TABLES `r_amenidad_tour` WRITE;
/*!40000 ALTER TABLE `r_amenidad_tour` DISABLE KEYS */;

INSERT INTO `r_amenidad_tour` VALUES (3,4,4);
INSERT INTO `r_amenidad_tour` VALUES (8,6,5);
INSERT INTO `r_amenidad_tour` VALUES (9,8,5);
INSERT INTO `r_amenidad_tour` VALUES (10,19,5);
INSERT INTO `r_amenidad_tour` VALUES (35,20,8);
INSERT INTO `r_amenidad_tour` VALUES (260,6,28);
INSERT INTO `r_amenidad_tour` VALUES (261,8,28);
INSERT INTO `r_amenidad_tour` VALUES (262,19,28);
INSERT INTO `r_amenidad_tour` VALUES (324,5,20);
INSERT INTO `r_amenidad_tour` VALUES (325,8,20);
INSERT INTO `r_amenidad_tour` VALUES (326,13,20);
INSERT INTO `r_amenidad_tour` VALUES (327,10,20);
INSERT INTO `r_amenidad_tour` VALUES (328,2,20);
INSERT INTO `r_amenidad_tour` VALUES (474,23,42);
INSERT INTO `r_amenidad_tour` VALUES (475,13,42);
INSERT INTO `r_amenidad_tour` VALUES (476,10,42);
INSERT INTO `r_amenidad_tour` VALUES (477,2,42);
INSERT INTO `r_amenidad_tour` VALUES (955,23,74);
INSERT INTO `r_amenidad_tour` VALUES (956,4,74);
INSERT INTO `r_amenidad_tour` VALUES (957,36,74);
INSERT INTO `r_amenidad_tour` VALUES (958,35,74);
INSERT INTO `r_amenidad_tour` VALUES (959,13,74);
INSERT INTO `r_amenidad_tour` VALUES (960,5,74);
INSERT INTO `r_amenidad_tour` VALUES (961,2,74);
INSERT INTO `r_amenidad_tour` VALUES (1297,7,18);
INSERT INTO `r_amenidad_tour` VALUES (1298,21,18);
INSERT INTO `r_amenidad_tour` VALUES (1299,8,18);
INSERT INTO `r_amenidad_tour` VALUES (1300,19,18);
INSERT INTO `r_amenidad_tour` VALUES (1508,33,65);
INSERT INTO `r_amenidad_tour` VALUES (2626,23,57);
INSERT INTO `r_amenidad_tour` VALUES (2627,8,57);
INSERT INTO `r_amenidad_tour` VALUES (2628,5,57);
INSERT INTO `r_amenidad_tour` VALUES (2629,10,57);
INSERT INTO `r_amenidad_tour` VALUES (2630,2,57);
INSERT INTO `r_amenidad_tour` VALUES (2631,23,56);
INSERT INTO `r_amenidad_tour` VALUES (2632,5,56);
INSERT INTO `r_amenidad_tour` VALUES (2633,10,56);
INSERT INTO `r_amenidad_tour` VALUES (4931,4,83);
INSERT INTO `r_amenidad_tour` VALUES (4932,7,83);
INSERT INTO `r_amenidad_tour` VALUES (4933,27,83);
INSERT INTO `r_amenidad_tour` VALUES (5266,4,52);
INSERT INTO `r_amenidad_tour` VALUES (5267,7,52);
INSERT INTO `r_amenidad_tour` VALUES (5268,27,52);
INSERT INTO `r_amenidad_tour` VALUES (5269,8,52);
INSERT INTO `r_amenidad_tour` VALUES (5270,3,52);
INSERT INTO `r_amenidad_tour` VALUES (5564,23,77);
INSERT INTO `r_amenidad_tour` VALUES (5565,20,77);
INSERT INTO `r_amenidad_tour` VALUES (5566,30,77);
INSERT INTO `r_amenidad_tour` VALUES (5567,11,77);
INSERT INTO `r_amenidad_tour` VALUES (5568,13,77);
INSERT INTO `r_amenidad_tour` VALUES (5569,10,77);
INSERT INTO `r_amenidad_tour` VALUES (5570,2,77);
INSERT INTO `r_amenidad_tour` VALUES (5571,8,58);
INSERT INTO `r_amenidad_tour` VALUES (5572,13,58);
INSERT INTO `r_amenidad_tour` VALUES (5573,3,58);
INSERT INTO `r_amenidad_tour` VALUES (5574,23,38);
INSERT INTO `r_amenidad_tour` VALUES (5575,4,38);
INSERT INTO `r_amenidad_tour` VALUES (5576,7,38);
INSERT INTO `r_amenidad_tour` VALUES (5577,8,38);
INSERT INTO `r_amenidad_tour` VALUES (5578,13,38);
INSERT INTO `r_amenidad_tour` VALUES (5581,23,39);
INSERT INTO `r_amenidad_tour` VALUES (5582,8,39);
INSERT INTO `r_amenidad_tour` VALUES (5583,13,39);
INSERT INTO `r_amenidad_tour` VALUES (5584,3,39);
INSERT INTO `r_amenidad_tour` VALUES (5585,23,40);
INSERT INTO `r_amenidad_tour` VALUES (5586,8,40);
INSERT INTO `r_amenidad_tour` VALUES (5587,10,40);
INSERT INTO `r_amenidad_tour` VALUES (5588,3,40);
INSERT INTO `r_amenidad_tour` VALUES (5599,4,51);
INSERT INTO `r_amenidad_tour` VALUES (5600,7,51);
INSERT INTO `r_amenidad_tour` VALUES (5601,27,51);
INSERT INTO `r_amenidad_tour` VALUES (5602,8,51);
INSERT INTO `r_amenidad_tour` VALUES (5603,3,51);
INSERT INTO `r_amenidad_tour` VALUES (5620,23,22);
INSERT INTO `r_amenidad_tour` VALUES (5621,8,22);
INSERT INTO `r_amenidad_tour` VALUES (5622,13,22);
INSERT INTO `r_amenidad_tour` VALUES (5623,2,22);
INSERT INTO `r_amenidad_tour` VALUES (5629,4,43);
INSERT INTO `r_amenidad_tour` VALUES (5630,21,43);
INSERT INTO `r_amenidad_tour` VALUES (5631,8,43);
INSERT INTO `r_amenidad_tour` VALUES (5632,13,43);
INSERT INTO `r_amenidad_tour` VALUES (5633,19,43);
INSERT INTO `r_amenidad_tour` VALUES (5634,4,15);
INSERT INTO `r_amenidad_tour` VALUES (5635,7,15);
INSERT INTO `r_amenidad_tour` VALUES (5636,8,15);
INSERT INTO `r_amenidad_tour` VALUES (5637,13,15);
INSERT INTO `r_amenidad_tour` VALUES (5638,5,15);
INSERT INTO `r_amenidad_tour` VALUES (5639,19,15);
INSERT INTO `r_amenidad_tour` VALUES (5640,23,33);
INSERT INTO `r_amenidad_tour` VALUES (5641,8,33);
INSERT INTO `r_amenidad_tour` VALUES (5642,11,33);
INSERT INTO `r_amenidad_tour` VALUES (5643,7,45);
INSERT INTO `r_amenidad_tour` VALUES (5644,8,45);
INSERT INTO `r_amenidad_tour` VALUES (5647,7,48);
INSERT INTO `r_amenidad_tour` VALUES (5648,8,48);
INSERT INTO `r_amenidad_tour` VALUES (5649,7,47);
INSERT INTO `r_amenidad_tour` VALUES (5650,8,47);
INSERT INTO `r_amenidad_tour` VALUES (5651,7,46);
INSERT INTO `r_amenidad_tour` VALUES (5652,8,46);
INSERT INTO `r_amenidad_tour` VALUES (5658,23,41);
INSERT INTO `r_amenidad_tour` VALUES (5659,8,41);
INSERT INTO `r_amenidad_tour` VALUES (5660,5,41);
INSERT INTO `r_amenidad_tour` VALUES (5661,10,41);
INSERT INTO `r_amenidad_tour` VALUES (5662,2,41);
INSERT INTO `r_amenidad_tour` VALUES (5663,8,31);
INSERT INTO `r_amenidad_tour` VALUES (5664,35,31);
INSERT INTO `r_amenidad_tour` VALUES (5665,13,31);
INSERT INTO `r_amenidad_tour` VALUES (5666,10,31);
INSERT INTO `r_amenidad_tour` VALUES (5667,2,31);
INSERT INTO `r_amenidad_tour` VALUES (5668,23,19);
INSERT INTO `r_amenidad_tour` VALUES (5669,8,19);
INSERT INTO `r_amenidad_tour` VALUES (5670,13,19);
INSERT INTO `r_amenidad_tour` VALUES (5671,5,19);
INSERT INTO `r_amenidad_tour` VALUES (5672,10,19);
INSERT INTO `r_amenidad_tour` VALUES (5673,2,19);
INSERT INTO `r_amenidad_tour` VALUES (5680,8,11);
INSERT INTO `r_amenidad_tour` VALUES (5685,23,72);
INSERT INTO `r_amenidad_tour` VALUES (5686,36,72);
INSERT INTO `r_amenidad_tour` VALUES (5687,13,72);
INSERT INTO `r_amenidad_tour` VALUES (5688,5,72);
INSERT INTO `r_amenidad_tour` VALUES (5689,34,44);
INSERT INTO `r_amenidad_tour` VALUES (5690,20,44);
INSERT INTO `r_amenidad_tour` VALUES (5691,8,44);
INSERT INTO `r_amenidad_tour` VALUES (5692,13,44);
INSERT INTO `r_amenidad_tour` VALUES (5693,2,44);
INSERT INTO `r_amenidad_tour` VALUES (5700,23,79);
INSERT INTO `r_amenidad_tour` VALUES (5701,4,79);
INSERT INTO `r_amenidad_tour` VALUES (5702,8,79);
INSERT INTO `r_amenidad_tour` VALUES (5703,35,79);
INSERT INTO `r_amenidad_tour` VALUES (5704,13,79);
INSERT INTO `r_amenidad_tour` VALUES (5705,2,79);
INSERT INTO `r_amenidad_tour` VALUES (5709,23,69);
INSERT INTO `r_amenidad_tour` VALUES (5710,8,69);
INSERT INTO `r_amenidad_tour` VALUES (5711,13,69);
INSERT INTO `r_amenidad_tour` VALUES (5712,5,69);
INSERT INTO `r_amenidad_tour` VALUES (5713,23,73);
INSERT INTO `r_amenidad_tour` VALUES (5714,4,73);
INSERT INTO `r_amenidad_tour` VALUES (5715,13,73);
INSERT INTO `r_amenidad_tour` VALUES (5716,2,73);
INSERT INTO `r_amenidad_tour` VALUES (5735,4,66);
INSERT INTO `r_amenidad_tour` VALUES (5736,7,66);
INSERT INTO `r_amenidad_tour` VALUES (5737,14,66);
INSERT INTO `r_amenidad_tour` VALUES (5738,27,66);
INSERT INTO `r_amenidad_tour` VALUES (5739,11,66);
INSERT INTO `r_amenidad_tour` VALUES (5740,26,66);
INSERT INTO `r_amenidad_tour` VALUES (5741,13,66);
INSERT INTO `r_amenidad_tour` VALUES (5742,4,9);
INSERT INTO `r_amenidad_tour` VALUES (5743,7,9);
INSERT INTO `r_amenidad_tour` VALUES (5744,27,9);
INSERT INTO `r_amenidad_tour` VALUES (5745,8,9);
INSERT INTO `r_amenidad_tour` VALUES (5746,3,9);
INSERT INTO `r_amenidad_tour` VALUES (5749,23,60);
INSERT INTO `r_amenidad_tour` VALUES (5750,8,60);
INSERT INTO `r_amenidad_tour` VALUES (5751,13,60);
INSERT INTO `r_amenidad_tour` VALUES (5752,10,60);
INSERT INTO `r_amenidad_tour` VALUES (5753,2,60);
INSERT INTO `r_amenidad_tour` VALUES (5754,8,10);
INSERT INTO `r_amenidad_tour` VALUES (5755,13,10);
INSERT INTO `r_amenidad_tour` VALUES (5756,7,55);
INSERT INTO `r_amenidad_tour` VALUES (5757,27,55);
INSERT INTO `r_amenidad_tour` VALUES (5758,21,55);
INSERT INTO `r_amenidad_tour` VALUES (5759,8,55);
INSERT INTO `r_amenidad_tour` VALUES (5760,11,55);
INSERT INTO `r_amenidad_tour` VALUES (5761,10,55);
INSERT INTO `r_amenidad_tour` VALUES (5781,23,76);
INSERT INTO `r_amenidad_tour` VALUES (5782,20,76);
INSERT INTO `r_amenidad_tour` VALUES (5783,11,76);
INSERT INTO `r_amenidad_tour` VALUES (5784,10,76);
INSERT INTO `r_amenidad_tour` VALUES (5785,2,76);
INSERT INTO `r_amenidad_tour` VALUES (5786,23,23);
INSERT INTO `r_amenidad_tour` VALUES (5787,8,23);
INSERT INTO `r_amenidad_tour` VALUES (5788,35,23);
INSERT INTO `r_amenidad_tour` VALUES (5789,13,23);
INSERT INTO `r_amenidad_tour` VALUES (5790,23,61);
INSERT INTO `r_amenidad_tour` VALUES (5791,28,61);
INSERT INTO `r_amenidad_tour` VALUES (5792,11,61);
INSERT INTO `r_amenidad_tour` VALUES (5793,13,61);
INSERT INTO `r_amenidad_tour` VALUES (5796,23,59);
INSERT INTO `r_amenidad_tour` VALUES (5797,8,59);
INSERT INTO `r_amenidad_tour` VALUES (5798,26,59);
INSERT INTO `r_amenidad_tour` VALUES (5799,13,59);
INSERT INTO `r_amenidad_tour` VALUES (5800,2,59);
INSERT INTO `r_amenidad_tour` VALUES (5801,23,16);
INSERT INTO `r_amenidad_tour` VALUES (5802,8,16);
INSERT INTO `r_amenidad_tour` VALUES (5803,2,16);
INSERT INTO `r_amenidad_tour` VALUES (5804,7,17);
INSERT INTO `r_amenidad_tour` VALUES (5805,14,17);
INSERT INTO `r_amenidad_tour` VALUES (5806,34,17);
INSERT INTO `r_amenidad_tour` VALUES (5807,8,17);
INSERT INTO `r_amenidad_tour` VALUES (5808,35,17);
INSERT INTO `r_amenidad_tour` VALUES (5809,19,17);
INSERT INTO `r_amenidad_tour` VALUES (5817,4,12);
INSERT INTO `r_amenidad_tour` VALUES (5818,20,12);
INSERT INTO `r_amenidad_tour` VALUES (5819,8,12);
INSERT INTO `r_amenidad_tour` VALUES (5820,26,12);
INSERT INTO `r_amenidad_tour` VALUES (5821,13,12);
INSERT INTO `r_amenidad_tour` VALUES (5822,5,12);
INSERT INTO `r_amenidad_tour` VALUES (5823,2,12);
INSERT INTO `r_amenidad_tour` VALUES (5824,34,6);
INSERT INTO `r_amenidad_tour` VALUES (5825,8,6);
INSERT INTO `r_amenidad_tour` VALUES (5826,35,6);
INSERT INTO `r_amenidad_tour` VALUES (5827,19,6);
INSERT INTO `r_amenidad_tour` VALUES (5828,4,7);
INSERT INTO `r_amenidad_tour` VALUES (5829,7,7);
INSERT INTO `r_amenidad_tour` VALUES (5830,14,7);
INSERT INTO `r_amenidad_tour` VALUES (5831,34,7);
INSERT INTO `r_amenidad_tour` VALUES (5832,8,7);
INSERT INTO `r_amenidad_tour` VALUES (5833,35,7);
INSERT INTO `r_amenidad_tour` VALUES (5834,19,7);
INSERT INTO `r_amenidad_tour` VALUES (5835,23,53);
INSERT INTO `r_amenidad_tour` VALUES (5836,8,53);
INSERT INTO `r_amenidad_tour` VALUES (5837,35,53);
INSERT INTO `r_amenidad_tour` VALUES (5838,13,53);
INSERT INTO `r_amenidad_tour` VALUES (5839,2,53);
INSERT INTO `r_amenidad_tour` VALUES (5840,4,78);
INSERT INTO `r_amenidad_tour` VALUES (5841,7,78);
INSERT INTO `r_amenidad_tour` VALUES (5842,27,78);
INSERT INTO `r_amenidad_tour` VALUES (5843,8,78);
INSERT INTO `r_amenidad_tour` VALUES (5844,29,78);
INSERT INTO `r_amenidad_tour` VALUES (5845,19,78);
INSERT INTO `r_amenidad_tour` VALUES (5846,3,78);
INSERT INTO `r_amenidad_tour` VALUES (5860,4,54);
INSERT INTO `r_amenidad_tour` VALUES (5861,7,54);
INSERT INTO `r_amenidad_tour` VALUES (5862,14,54);
INSERT INTO `r_amenidad_tour` VALUES (5863,34,54);
INSERT INTO `r_amenidad_tour` VALUES (5864,8,54);
INSERT INTO `r_amenidad_tour` VALUES (5865,35,54);
INSERT INTO `r_amenidad_tour` VALUES (5866,2,54);
INSERT INTO `r_amenidad_tour` VALUES (5867,23,68);
INSERT INTO `r_amenidad_tour` VALUES (5868,35,68);
INSERT INTO `r_amenidad_tour` VALUES (5869,13,68);
INSERT INTO `r_amenidad_tour` VALUES (5870,2,68);
INSERT INTO `r_amenidad_tour` VALUES (5890,8,24);
INSERT INTO `r_amenidad_tour` VALUES (5891,35,24);
INSERT INTO `r_amenidad_tour` VALUES (5892,13,24);
INSERT INTO `r_amenidad_tour` VALUES (5893,2,24);
INSERT INTO `r_amenidad_tour` VALUES (5896,8,32);
INSERT INTO `r_amenidad_tour` VALUES (5897,13,32);
INSERT INTO `r_amenidad_tour` VALUES (5898,2,32);
INSERT INTO `r_amenidad_tour` VALUES (5907,4,49);
INSERT INTO `r_amenidad_tour` VALUES (5908,7,49);
INSERT INTO `r_amenidad_tour` VALUES (5909,27,49);
INSERT INTO `r_amenidad_tour` VALUES (5910,8,49);
INSERT INTO `r_amenidad_tour` VALUES (5911,3,49);
INSERT INTO `r_amenidad_tour` VALUES (5912,4,50);
INSERT INTO `r_amenidad_tour` VALUES (5913,7,50);
INSERT INTO `r_amenidad_tour` VALUES (5914,27,50);
INSERT INTO `r_amenidad_tour` VALUES (5915,8,50);
INSERT INTO `r_amenidad_tour` VALUES (5916,3,50);
INSERT INTO `r_amenidad_tour` VALUES (5917,23,70);
INSERT INTO `r_amenidad_tour` VALUES (5918,4,70);
INSERT INTO `r_amenidad_tour` VALUES (5919,7,70);
INSERT INTO `r_amenidad_tour` VALUES (5920,13,70);
INSERT INTO `r_amenidad_tour` VALUES (5921,3,70);
INSERT INTO `r_amenidad_tour` VALUES (5922,4,34);
INSERT INTO `r_amenidad_tour` VALUES (5923,7,34);
INSERT INTO `r_amenidad_tour` VALUES (5924,8,34);
INSERT INTO `r_amenidad_tour` VALUES (5925,29,34);
INSERT INTO `r_amenidad_tour` VALUES (5926,3,34);
INSERT INTO `r_amenidad_tour` VALUES (5933,7,14);
INSERT INTO `r_amenidad_tour` VALUES (5934,21,14);
INSERT INTO `r_amenidad_tour` VALUES (5935,8,14);
INSERT INTO `r_amenidad_tour` VALUES (5936,11,14);
INSERT INTO `r_amenidad_tour` VALUES (5937,13,14);
INSERT INTO `r_amenidad_tour` VALUES (5938,19,14);
INSERT INTO `r_amenidad_tour` VALUES (5939,7,30);
INSERT INTO `r_amenidad_tour` VALUES (5940,8,30);
INSERT INTO `r_amenidad_tour` VALUES (5941,23,37);
INSERT INTO `r_amenidad_tour` VALUES (5942,8,37);
INSERT INTO `r_amenidad_tour` VALUES (5943,5,37);
INSERT INTO `r_amenidad_tour` VALUES (5944,3,37);
INSERT INTO `r_amenidad_tour` VALUES (5961,23,36);
INSERT INTO `r_amenidad_tour` VALUES (5962,8,36);
INSERT INTO `r_amenidad_tour` VALUES (5963,13,36);
INSERT INTO `r_amenidad_tour` VALUES (5964,5,36);
INSERT INTO `r_amenidad_tour` VALUES (5965,10,36);
INSERT INTO `r_amenidad_tour` VALUES (5966,2,36);
INSERT INTO `r_amenidad_tour` VALUES (5967,8,21);
INSERT INTO `r_amenidad_tour` VALUES (5968,13,21);
INSERT INTO `r_amenidad_tour` VALUES (5969,5,21);
INSERT INTO `r_amenidad_tour` VALUES (5970,10,21);
INSERT INTO `r_amenidad_tour` VALUES (5971,7,27);
INSERT INTO `r_amenidad_tour` VALUES (5972,8,27);
INSERT INTO `r_amenidad_tour` VALUES (5973,7,35);
INSERT INTO `r_amenidad_tour` VALUES (5974,34,35);
INSERT INTO `r_amenidad_tour` VALUES (5975,21,35);
INSERT INTO `r_amenidad_tour` VALUES (5976,30,35);
INSERT INTO `r_amenidad_tour` VALUES (5977,8,35);
INSERT INTO `r_amenidad_tour` VALUES (5978,35,35);
INSERT INTO `r_amenidad_tour` VALUES (5979,11,35);
INSERT INTO `r_amenidad_tour` VALUES (5980,4,29);
INSERT INTO `r_amenidad_tour` VALUES (5981,7,29);
INSERT INTO `r_amenidad_tour` VALUES (5982,14,29);
INSERT INTO `r_amenidad_tour` VALUES (5983,34,29);
INSERT INTO `r_amenidad_tour` VALUES (5984,8,29);
INSERT INTO `r_amenidad_tour` VALUES (5985,35,29);
INSERT INTO `r_amenidad_tour` VALUES (5986,19,29);
INSERT INTO `r_amenidad_tour` VALUES (6015,8,13);
INSERT INTO `r_amenidad_tour` VALUES (6016,13,13);
INSERT INTO `r_amenidad_tour` VALUES (6017,2,13);
INSERT INTO `r_amenidad_tour` VALUES (6038,30,90);
INSERT INTO `r_amenidad_tour` VALUES (6039,8,90);
INSERT INTO `r_amenidad_tour` VALUES (6040,35,90);
INSERT INTO `r_amenidad_tour` VALUES (6041,13,90);
INSERT INTO `r_amenidad_tour` VALUES (6042,31,90);
INSERT INTO `r_amenidad_tour` VALUES (6043,19,90);
INSERT INTO `r_amenidad_tour` VALUES (6048,30,89);
INSERT INTO `r_amenidad_tour` VALUES (6049,8,89);
INSERT INTO `r_amenidad_tour` VALUES (6050,13,89);
INSERT INTO `r_amenidad_tour` VALUES (6051,31,89);
INSERT INTO `r_amenidad_tour` VALUES (6052,19,89);
INSERT INTO `r_amenidad_tour` VALUES (6105,30,63);
INSERT INTO `r_amenidad_tour` VALUES (6106,8,63);
INSERT INTO `r_amenidad_tour` VALUES (6107,13,63);
INSERT INTO `r_amenidad_tour` VALUES (6108,31,63);
INSERT INTO `r_amenidad_tour` VALUES (6109,19,63);
INSERT INTO `r_amenidad_tour` VALUES (6110,2,63);
INSERT INTO `r_amenidad_tour` VALUES (6141,14,91);
INSERT INTO `r_amenidad_tour` VALUES (6142,8,91);
INSERT INTO `r_amenidad_tour` VALUES (6143,13,91);
INSERT INTO `r_amenidad_tour` VALUES (6144,31,91);
INSERT INTO `r_amenidad_tour` VALUES (6145,19,91);
/*!40000 ALTER TABLE `r_amenidad_tour` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table r_categoria_tour
#

DROP TABLE IF EXISTS `r_categoria_tour`;
CREATE TABLE `r_categoria_tour` (
  `id_r_c_tc` int(10) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) DEFAULT NULL,
  `tour_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_r_c_tc`)
) ENGINE=MyISAM AUTO_INCREMENT=3219 DEFAULT CHARSET=latin1;

#
# Dumping data for table r_categoria_tour
#
LOCK TABLES `r_categoria_tour` WRITE;
/*!40000 ALTER TABLE `r_categoria_tour` DISABLE KEYS */;

INSERT INTO `r_categoria_tour` VALUES (3,10,4);
INSERT INTO `r_categoria_tour` VALUES (7,15,5);
INSERT INTO `r_categoria_tour` VALUES (40,11,8);
INSERT INTO `r_categoria_tour` VALUES (41,17,8);
INSERT INTO `r_categoria_tour` VALUES (270,10,28);
INSERT INTO `r_categoria_tour` VALUES (272,15,28);
INSERT INTO `r_categoria_tour` VALUES (274,5,28);
INSERT INTO `r_categoria_tour` VALUES (1408,1,57);
INSERT INTO `r_categoria_tour` VALUES (1409,1,56);
INSERT INTO `r_categoria_tour` VALUES (2598,9,83);
INSERT INTO `r_categoria_tour` VALUES (2744,14,52);
INSERT INTO `r_categoria_tour` VALUES (2745,7,52);
INSERT INTO `r_categoria_tour` VALUES (2881,1,77);
INSERT INTO `r_categoria_tour` VALUES (2882,29,77);
INSERT INTO `r_categoria_tour` VALUES (2883,22,77);
INSERT INTO `r_categoria_tour` VALUES (2884,1,58);
INSERT INTO `r_categoria_tour` VALUES (2885,28,58);
INSERT INTO `r_categoria_tour` VALUES (2886,14,58);
INSERT INTO `r_categoria_tour` VALUES (2887,1,38);
INSERT INTO `r_categoria_tour` VALUES (2888,25,38);
INSERT INTO `r_categoria_tour` VALUES (2889,29,38);
INSERT INTO `r_categoria_tour` VALUES (2890,22,38);
INSERT INTO `r_categoria_tour` VALUES (2891,28,38);
INSERT INTO `r_categoria_tour` VALUES (2892,14,38);
INSERT INTO `r_categoria_tour` VALUES (2895,25,39);
INSERT INTO `r_categoria_tour` VALUES (2896,25,40);
INSERT INTO `r_categoria_tour` VALUES (2899,7,51);
INSERT INTO `r_categoria_tour` VALUES (2906,9,22);
INSERT INTO `r_categoria_tour` VALUES (2907,14,22);
INSERT INTO `r_categoria_tour` VALUES (2910,9,43);
INSERT INTO `r_categoria_tour` VALUES (2911,14,43);
INSERT INTO `r_categoria_tour` VALUES (2912,9,15);
INSERT INTO `r_categoria_tour` VALUES (2913,14,15);
INSERT INTO `r_categoria_tour` VALUES (2914,9,33);
INSERT INTO `r_categoria_tour` VALUES (2915,1,33);
INSERT INTO `r_categoria_tour` VALUES (2916,14,33);
INSERT INTO `r_categoria_tour` VALUES (2917,16,45);
INSERT INTO `r_categoria_tour` VALUES (2918,7,45);
INSERT INTO `r_categoria_tour` VALUES (2921,16,48);
INSERT INTO `r_categoria_tour` VALUES (2922,7,48);
INSERT INTO `r_categoria_tour` VALUES (2923,16,47);
INSERT INTO `r_categoria_tour` VALUES (2924,7,47);
INSERT INTO `r_categoria_tour` VALUES (2925,16,46);
INSERT INTO `r_categoria_tour` VALUES (2926,7,46);
INSERT INTO `r_categoria_tour` VALUES (2929,1,41);
INSERT INTO `r_categoria_tour` VALUES (2930,1,31);
INSERT INTO `r_categoria_tour` VALUES (2931,1,19);
INSERT INTO `r_categoria_tour` VALUES (2933,1,11);
INSERT INTO `r_categoria_tour` VALUES (2936,29,72);
INSERT INTO `r_categoria_tour` VALUES (2937,28,72);
INSERT INTO `r_categoria_tour` VALUES (2938,14,72);
INSERT INTO `r_categoria_tour` VALUES (2939,9,44);
INSERT INTO `r_categoria_tour` VALUES (2940,28,44);
INSERT INTO `r_categoria_tour` VALUES (2945,1,79);
INSERT INTO `r_categoria_tour` VALUES (2946,29,79);
INSERT INTO `r_categoria_tour` VALUES (2947,28,79);
INSERT INTO `r_categoria_tour` VALUES (2948,27,79);
INSERT INTO `r_categoria_tour` VALUES (2952,29,69);
INSERT INTO `r_categoria_tour` VALUES (2953,22,69);
INSERT INTO `r_categoria_tour` VALUES (2954,14,69);
INSERT INTO `r_categoria_tour` VALUES (2955,1,73);
INSERT INTO `r_categoria_tour` VALUES (2956,29,73);
INSERT INTO `r_categoria_tour` VALUES (2957,28,73);
INSERT INTO `r_categoria_tour` VALUES (2966,22,66);
INSERT INTO `r_categoria_tour` VALUES (2967,14,66);
INSERT INTO `r_categoria_tour` VALUES (2968,27,66);
INSERT INTO `r_categoria_tour` VALUES (2969,14,9);
INSERT INTO `r_categoria_tour` VALUES (2970,27,9);
INSERT INTO `r_categoria_tour` VALUES (2971,7,9);
INSERT INTO `r_categoria_tour` VALUES (2975,9,60);
INSERT INTO `r_categoria_tour` VALUES (2976,1,10);
INSERT INTO `r_categoria_tour` VALUES (2977,22,55);
INSERT INTO `r_categoria_tour` VALUES (2978,14,55);
INSERT INTO `r_categoria_tour` VALUES (2987,29,76);
INSERT INTO `r_categoria_tour` VALUES (2988,1,23);
INSERT INTO `r_categoria_tour` VALUES (2989,27,23);
INSERT INTO `r_categoria_tour` VALUES (2990,28,61);
INSERT INTO `r_categoria_tour` VALUES (2991,36,61);
INSERT INTO `r_categoria_tour` VALUES (2996,9,59);
INSERT INTO `r_categoria_tour` VALUES (2997,1,59);
INSERT INTO `r_categoria_tour` VALUES (2998,14,59);
INSERT INTO `r_categoria_tour` VALUES (2999,9,16);
INSERT INTO `r_categoria_tour` VALUES (3000,29,16);
INSERT INTO `r_categoria_tour` VALUES (3001,14,16);
INSERT INTO `r_categoria_tour` VALUES (3002,9,17);
INSERT INTO `r_categoria_tour` VALUES (3003,22,17);
INSERT INTO `r_categoria_tour` VALUES (3004,14,17);
INSERT INTO `r_categoria_tour` VALUES (3005,27,17);
INSERT INTO `r_categoria_tour` VALUES (3006,13,17);
INSERT INTO `r_categoria_tour` VALUES (3013,9,12);
INSERT INTO `r_categoria_tour` VALUES (3014,1,12);
INSERT INTO `r_categoria_tour` VALUES (3015,29,12);
INSERT INTO `r_categoria_tour` VALUES (3016,22,12);
INSERT INTO `r_categoria_tour` VALUES (3017,28,12);
INSERT INTO `r_categoria_tour` VALUES (3018,14,12);
INSERT INTO `r_categoria_tour` VALUES (3019,22,6);
INSERT INTO `r_categoria_tour` VALUES (3020,14,6);
INSERT INTO `r_categoria_tour` VALUES (3021,27,6);
INSERT INTO `r_categoria_tour` VALUES (3022,13,6);
INSERT INTO `r_categoria_tour` VALUES (3023,1,7);
INSERT INTO `r_categoria_tour` VALUES (3024,22,7);
INSERT INTO `r_categoria_tour` VALUES (3025,14,7);
INSERT INTO `r_categoria_tour` VALUES (3026,27,7);
INSERT INTO `r_categoria_tour` VALUES (3027,13,7);
INSERT INTO `r_categoria_tour` VALUES (3028,1,53);
INSERT INTO `r_categoria_tour` VALUES (3029,29,53);
INSERT INTO `r_categoria_tour` VALUES (3030,22,53);
INSERT INTO `r_categoria_tour` VALUES (3031,14,53);
INSERT INTO `r_categoria_tour` VALUES (3032,13,53);
INSERT INTO `r_categoria_tour` VALUES (3033,1,78);
INSERT INTO `r_categoria_tour` VALUES (3034,27,78);
INSERT INTO `r_categoria_tour` VALUES (3035,13,78);
INSERT INTO `r_categoria_tour` VALUES (3036,7,78);
INSERT INTO `r_categoria_tour` VALUES (3046,1,54);
INSERT INTO `r_categoria_tour` VALUES (3047,14,54);
INSERT INTO `r_categoria_tour` VALUES (3048,27,54);
INSERT INTO `r_categoria_tour` VALUES (3049,13,54);
INSERT INTO `r_categoria_tour` VALUES (3050,1,68);
INSERT INTO `r_categoria_tour` VALUES (3051,29,68);
INSERT INTO `r_categoria_tour` VALUES (3061,1,24);
INSERT INTO `r_categoria_tour` VALUES (3062,29,24);
INSERT INTO `r_categoria_tour` VALUES (3063,22,24);
INSERT INTO `r_categoria_tour` VALUES (3064,14,24);
INSERT INTO `r_categoria_tour` VALUES (3067,1,32);
INSERT INTO `r_categoria_tour` VALUES (3072,7,49);
INSERT INTO `r_categoria_tour` VALUES (3073,7,50);
INSERT INTO `r_categoria_tour` VALUES (3074,29,70);
INSERT INTO `r_categoria_tour` VALUES (3075,22,70);
INSERT INTO `r_categoria_tour` VALUES (3076,16,34);
INSERT INTO `r_categoria_tour` VALUES (3077,7,34);
INSERT INTO `r_categoria_tour` VALUES (3080,9,14);
INSERT INTO `r_categoria_tour` VALUES (3081,14,14);
INSERT INTO `r_categoria_tour` VALUES (3082,14,30);
INSERT INTO `r_categoria_tour` VALUES (3083,27,30);
INSERT INTO `r_categoria_tour` VALUES (3084,7,30);
INSERT INTO `r_categoria_tour` VALUES (3085,28,37);
INSERT INTO `r_categoria_tour` VALUES (3086,14,37);
INSERT INTO `r_categoria_tour` VALUES (3097,1,36);
INSERT INTO `r_categoria_tour` VALUES (3098,1,21);
INSERT INTO `r_categoria_tour` VALUES (3099,14,21);
INSERT INTO `r_categoria_tour` VALUES (3100,16,27);
INSERT INTO `r_categoria_tour` VALUES (3101,7,27);
INSERT INTO `r_categoria_tour` VALUES (3102,22,35);
INSERT INTO `r_categoria_tour` VALUES (3103,28,35);
INSERT INTO `r_categoria_tour` VALUES (3104,14,35);
INSERT INTO `r_categoria_tour` VALUES (3105,1,29);
INSERT INTO `r_categoria_tour` VALUES (3106,22,29);
INSERT INTO `r_categoria_tour` VALUES (3107,14,29);
INSERT INTO `r_categoria_tour` VALUES (3108,27,29);
INSERT INTO `r_categoria_tour` VALUES (3109,13,29);
INSERT INTO `r_categoria_tour` VALUES (3124,9,13);
INSERT INTO `r_categoria_tour` VALUES (3125,28,13);
INSERT INTO `r_categoria_tour` VALUES (3126,14,13);
INSERT INTO `r_categoria_tour` VALUES (3144,24,90);
INSERT INTO `r_categoria_tour` VALUES (3145,29,90);
INSERT INTO `r_categoria_tour` VALUES (3146,14,90);
INSERT INTO `r_categoria_tour` VALUES (3148,1,89);
INSERT INTO `r_categoria_tour` VALUES (3149,24,89);
INSERT INTO `r_categoria_tour` VALUES (3150,29,89);
INSERT INTO `r_categoria_tour` VALUES (3151,28,89);
INSERT INTO `r_categoria_tour` VALUES (3187,1,63);
INSERT INTO `r_categoria_tour` VALUES (3188,24,63);
INSERT INTO `r_categoria_tour` VALUES (3189,29,63);
INSERT INTO `r_categoria_tour` VALUES (3190,28,63);
INSERT INTO `r_categoria_tour` VALUES (3215,1,91);
INSERT INTO `r_categoria_tour` VALUES (3216,24,91);
INSERT INTO `r_categoria_tour` VALUES (3217,28,91);
INSERT INTO `r_categoria_tour` VALUES (3218,36,91);
/*!40000 ALTER TABLE `r_categoria_tour` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table r_menu_user
#

DROP TABLE IF EXISTS `r_menu_user`;
CREATE TABLE `r_menu_user` (
  `id_r_menu_user` int(10) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(10) NOT NULL,
  `menu_id` int(10) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  `nivel` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_r_menu_user`)
) ENGINE=MyISAM AUTO_INCREMENT=1518 DEFAULT CHARSET=latin1;

#
# Dumping data for table r_menu_user
#
LOCK TABLES `r_menu_user` WRITE;
/*!40000 ALTER TABLE `r_menu_user` DISABLE KEYS */;

INSERT INTO `r_menu_user` VALUES (1508,1,4,NULL,1);
INSERT INTO `r_menu_user` VALUES (1509,1,2,NULL,1);
INSERT INTO `r_menu_user` VALUES (1510,1,1,NULL,1);
INSERT INTO `r_menu_user` VALUES (1511,1,13,NULL,2);
INSERT INTO `r_menu_user` VALUES (1512,1,14,NULL,2);
INSERT INTO `r_menu_user` VALUES (1513,1,18,NULL,2);
INSERT INTO `r_menu_user` VALUES (1514,1,19,NULL,2);
INSERT INTO `r_menu_user` VALUES (1515,1,11,NULL,2);
INSERT INTO `r_menu_user` VALUES (1516,1,10,NULL,2);
INSERT INTO `r_menu_user` VALUES (1517,1,12,NULL,2);
/*!40000 ALTER TABLE `r_menu_user` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table restricciones_amenidades
#

DROP TABLE IF EXISTS `restricciones_amenidades`;
CREATE TABLE `restricciones_amenidades` (
  `id_restricciones` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_restriccion` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_restricciones`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

#
# Dumping data for table restricciones_amenidades
#
LOCK TABLES `restricciones_amenidades` WRITE;
/*!40000 ALTER TABLE `restricciones_amenidades` DISABLE KEYS */;

INSERT INTO `restricciones_amenidades` VALUES (1,'Sin Restricciones');
INSERT INTO `restricciones_amenidades` VALUES (2,'Uso Indebido de Instalaciones');
INSERT INTO `restricciones_amenidades` VALUES (3,'Atraso en Cuotas Condominales');
INSERT INTO `restricciones_amenidades` VALUES (4,'Impago Definitivo');
INSERT INTO `restricciones_amenidades` VALUES (5,'Adeudos Extraordinarios');
INSERT INTO `restricciones_amenidades` VALUES (6,'Adeudos por Reservas');
/*!40000 ALTER TABLE `restricciones_amenidades` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table servicio_transfers_addons
#

DROP TABLE IF EXISTS `servicio_transfers_addons`;
CREATE TABLE `servicio_transfers_addons` (
  `idtransferaddon` int(11) NOT NULL AUTO_INCREMENT,
  `addon` varchar(300) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`idtransferaddon`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table servicio_transfers_addons
#
LOCK TABLES `servicio_transfers_addons` WRITE;
/*!40000 ALTER TABLE `servicio_transfers_addons` DISABLE KEYS */;

INSERT INTO `servicio_transfers_addons` VALUES (1,'Autobus de lujo');
INSERT INTO `servicio_transfers_addons` VALUES (2,'Capacidad maxima 45 pasajeros');
/*!40000 ALTER TABLE `servicio_transfers_addons` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table servicio_transfers_destinos
#

DROP TABLE IF EXISTS `servicio_transfers_destinos`;
CREATE TABLE `servicio_transfers_destinos` (
  `idtransferdestino` int(11) NOT NULL AUTO_INCREMENT,
  `iddestino` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtransferdestino`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table servicio_transfers_destinos
#
LOCK TABLES `servicio_transfers_destinos` WRITE;
/*!40000 ALTER TABLE `servicio_transfers_destinos` DISABLE KEYS */;

INSERT INTO `servicio_transfers_destinos` VALUES (1,39);
INSERT INTO `servicio_transfers_destinos` VALUES (2,34);
/*!40000 ALTER TABLE `servicio_transfers_destinos` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table servicio_transfers_hotel
#

DROP TABLE IF EXISTS `servicio_transfers_hotel`;
CREATE TABLE `servicio_transfers_hotel` (
  `idhotel` int(11) NOT NULL AUTO_INCREMENT,
  `nombrehotel` varchar(350) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`idhotel`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table servicio_transfers_hotel
#
LOCK TABLES `servicio_transfers_hotel` WRITE;
/*!40000 ALTER TABLE `servicio_transfers_hotel` DISABLE KEYS */;

INSERT INTO `servicio_transfers_hotel` VALUES (1,'Mundo Imperial');
INSERT INTO `servicio_transfers_hotel` VALUES (2,'Princess');
INSERT INTO `servicio_transfers_hotel` VALUES (3,'Pierre Marques');
INSERT INTO `servicio_transfers_hotel` VALUES (4,'Banyan Tree');
INSERT INTO `servicio_transfers_hotel` VALUES (5,'Camino Real');
INSERT INTO `servicio_transfers_hotel` VALUES (6,'Las Brisas');
INSERT INTO `servicio_transfers_hotel` VALUES (7,'Grand Hotel');
INSERT INTO `servicio_transfers_hotel` VALUES (8,'The One');
INSERT INTO `servicio_transfers_hotel` VALUES (9,'Otro');
INSERT INTO `servicio_transfers_hotel` VALUES (10,'Hotel Estrella del Mar');
/*!40000 ALTER TABLE `servicio_transfers_hotel` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table servicio_transfers_modelos
#

DROP TABLE IF EXISTS `servicio_transfers_modelos`;
CREATE TABLE `servicio_transfers_modelos` (
  `idtransfersmodelos` int(11) NOT NULL AUTO_INCREMENT,
  `modelo` varchar(350) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `imagen` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idtransfersmodelos`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table servicio_transfers_modelos
#
LOCK TABLES `servicio_transfers_modelos` WRITE;
/*!40000 ALTER TABLE `servicio_transfers_modelos` DISABLE KEYS */;

INSERT INTO `servicio_transfers_modelos` VALUES (1,'AUTO','vento.jpg');
INSERT INTO `servicio_transfers_modelos` VALUES (2,'VAN','van.jpg');
INSERT INTO `servicio_transfers_modelos` VALUES (3,'AUTOBUS DE LUJO','autobus.jpg');
/*!40000 ALTER TABLE `servicio_transfers_modelos` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table servicio_transfers_tarifa
#

DROP TABLE IF EXISTS `servicio_transfers_tarifa`;
CREATE TABLE `servicio_transfers_tarifa` (
  `idtarifa` int(11) NOT NULL AUTO_INCREMENT,
  `idtipohotel` int(11) NOT NULL DEFAULT '0',
  `idmodel` int(11) NOT NULL DEFAULT '0',
  `roundtrip` tinyint(2) NOT NULL DEFAULT '0',
  `precio_sencillo` decimal(10,2) NOT NULL DEFAULT '0.00',
  `precio_round` decimal(10,2) NOT NULL DEFAULT '0.00',
  `id_moneda` int(11) DEFAULT NULL,
  `fecha_ini` date DEFAULT NULL,
  `fecha_end` date DEFAULT NULL,
  `min_pasajeros` int(11) DEFAULT NULL,
  `max_pasajeros` int(11) DEFAULT NULL,
  PRIMARY KEY (`idtarifa`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table servicio_transfers_tarifa
#
LOCK TABLES `servicio_transfers_tarifa` WRITE;
/*!40000 ALTER TABLE `servicio_transfers_tarifa` DISABLE KEYS */;

INSERT INTO `servicio_transfers_tarifa` VALUES (1,1,3,1,240,480,2,'2017-01-17','2017-12-31',1,45);
INSERT INTO `servicio_transfers_tarifa` VALUES (2,2,3,1,240,480,2,'2017-01-17','2017-12-31',1,45);
INSERT INTO `servicio_transfers_tarifa` VALUES (3,3,3,1,240,480,2,'2017-01-17','2017-12-31',1,45);
INSERT INTO `servicio_transfers_tarifa` VALUES (4,4,3,1,240,480,2,'2017-01-17','2017-12-31',1,45);
INSERT INTO `servicio_transfers_tarifa` VALUES (5,5,3,1,240,480,2,'2017-01-17','2017-12-31',1,45);
INSERT INTO `servicio_transfers_tarifa` VALUES (6,6,3,1,240,480,2,'2017-01-17','2017-12-31',1,45);
INSERT INTO `servicio_transfers_tarifa` VALUES (7,7,1,1,260,520,2,'2017-01-17','2017-12-31',1,2);
INSERT INTO `servicio_transfers_tarifa` VALUES (8,8,1,1,260,520,2,'2017-01-17','2017-12-31',3,9);
/*!40000 ALTER TABLE `servicio_transfers_tarifa` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table servicio_transfers_tipo
#

DROP TABLE IF EXISTS `servicio_transfers_tipo`;
CREATE TABLE `servicio_transfers_tipo` (
  `idtipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombretipo` varchar(450) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`idtipo`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table servicio_transfers_tipo
#
LOCK TABLES `servicio_transfers_tipo` WRITE;
/*!40000 ALTER TABLE `servicio_transfers_tipo` DISABLE KEYS */;

INSERT INTO `servicio_transfers_tipo` VALUES (1,'Aeropuerto -> Hotel');
INSERT INTO `servicio_transfers_tipo` VALUES (2,'Hotel -> Aeropuerto');
INSERT INTO `servicio_transfers_tipo` VALUES (3,'Hotel -> Campo de Golf');
INSERT INTO `servicio_transfers_tipo` VALUES (4,'Campo de Golf -> Aeropuerto');
/*!40000 ALTER TABLE `servicio_transfers_tipo` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table servicio_transfers_tipo_hotel
#

DROP TABLE IF EXISTS `servicio_transfers_tipo_hotel`;
CREATE TABLE `servicio_transfers_tipo_hotel` (
  `idtipohotel` int(11) NOT NULL AUTO_INCREMENT,
  `iddestino` int(11) NOT NULL DEFAULT '0',
  `idtipo` int(11) NOT NULL DEFAULT '0',
  `idubicacion` int(11) NOT NULL DEFAULT '0',
  `idhotel` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idtipohotel`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table servicio_transfers_tipo_hotel
#
LOCK TABLES `servicio_transfers_tipo_hotel` WRITE;
/*!40000 ALTER TABLE `servicio_transfers_tipo_hotel` DISABLE KEYS */;

INSERT INTO `servicio_transfers_tipo_hotel` VALUES (1,39,1,1,1);
INSERT INTO `servicio_transfers_tipo_hotel` VALUES (2,39,1,1,2);
INSERT INTO `servicio_transfers_tipo_hotel` VALUES (3,39,1,1,3);
INSERT INTO `servicio_transfers_tipo_hotel` VALUES (4,39,1,1,4);
INSERT INTO `servicio_transfers_tipo_hotel` VALUES (5,39,1,1,5);
INSERT INTO `servicio_transfers_tipo_hotel` VALUES (6,39,1,1,6);
INSERT INTO `servicio_transfers_tipo_hotel` VALUES (7,34,1,1,9);
INSERT INTO `servicio_transfers_tipo_hotel` VALUES (8,34,1,1,10);
/*!40000 ALTER TABLE `servicio_transfers_tipo_hotel` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table servicio_transfers_ubicacion
#

DROP TABLE IF EXISTS `servicio_transfers_ubicacion`;
CREATE TABLE `servicio_transfers_ubicacion` (
  `idubicacion` int(11) NOT NULL AUTO_INCREMENT,
  `ubicacion` varchar(300) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`idubicacion`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Dumping data for table servicio_transfers_ubicacion
#
LOCK TABLES `servicio_transfers_ubicacion` WRITE;
/*!40000 ALTER TABLE `servicio_transfers_ubicacion` DISABLE KEYS */;

INSERT INTO `servicio_transfers_ubicacion` VALUES (1,'Aeropuerto');
INSERT INTO `servicio_transfers_ubicacion` VALUES (2,'Campo de Golf');
INSERT INTO `servicio_transfers_ubicacion` VALUES (3,'Hotel');
/*!40000 ALTER TABLE `servicio_transfers_ubicacion` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table tipo_condominios
#

DROP TABLE IF EXISTS `tipo_condominios`;
CREATE TABLE `tipo_condominios` (
  `id_tipo_condominio` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_tipo_condominio` varchar(1000) NOT NULL DEFAULT '',
  `status` varchar(10) NOT NULL,
  `nombre_tipo_condominio_eng` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_condominio`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

#
# Dumping data for table tipo_condominios
#
LOCK TABLES `tipo_condominios` WRITE;
/*!40000 ALTER TABLE `tipo_condominios` DISABLE KEYS */;

INSERT INTO `tipo_condominios` VALUES (1,'Condominio Maestro','on','');
INSERT INTO `tipo_condominios` VALUES (2,'Sub Condominio','on','');
INSERT INTO `tipo_condominios` VALUES (3,'R�gimen P en C','on','');
INSERT INTO `tipo_condominios` VALUES (4,'Sin R�gimen Legal','on','');
INSERT INTO `tipo_condominios` VALUES (5,'Condominio Vertical','on','');
INSERT INTO `tipo_condominios` VALUES (6,'Condominio Horizontal','on','');
INSERT INTO `tipo_condominios` VALUES (7,'Condominio de Terreno Urbano','on','');
INSERT INTO `tipo_condominios` VALUES (8,'Condominio Tipo Mixto Ver / Hor','on','');
INSERT INTO `tipo_condominios` VALUES (9,'Condominio Uso Habitacional','on','');
INSERT INTO `tipo_condominios` VALUES (10,'Condominio Uso Comercial o Servicios','on','');
INSERT INTO `tipo_condominios` VALUES (11,'Condominio Uso Industrial','on','');
INSERT INTO `tipo_condominios` VALUES (12,'Condominio Uso Mixto','on','');
INSERT INTO `tipo_condominios` VALUES (13,'Construcci�n en Proyecto','on','');
INSERT INTO `tipo_condominios` VALUES (14,'Construccion Nueva','on','');
INSERT INTO `tipo_condominios` VALUES (15,'Construcci�n en Uso Activo','on','');
/*!40000 ALTER TABLE `tipo_condominios` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table type_user
#

DROP TABLE IF EXISTS `type_user`;
CREATE TABLE `type_user` (
  `id_type_user` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `respaldo` int(11) NOT NULL,
  `status` varchar(5) NOT NULL,
  `agregar` varchar(255) DEFAULT NULL,
  `eliminar` varchar(255) DEFAULT NULL,
  `editar` varchar(255) DEFAULT NULL,
  `restriccion` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_type_user`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

#
# Dumping data for table type_user
#
LOCK TABLES `type_user` WRITE;
/*!40000 ALTER TABLE `type_user` DISABLE KEYS */;

INSERT INTO `type_user` VALUES (1,'Administrador',0,'on','on','on','on',NULL);
/*!40000 ALTER TABLE `type_user` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table ubicacion
#

DROP TABLE IF EXISTS `ubicacion`;
CREATE TABLE `ubicacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL DEFAULT '',
  `no_delegados_tarde` int(11) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  `no_operadores_tarde` int(11) DEFAULT NULL,
  `no_operadores_dia` int(255) DEFAULT NULL,
  `no_delegados_dia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Tabla de Estados de la Rep�blica Mexicana';

#
# Dumping data for table ubicacion
#
LOCK TABLES `ubicacion` WRITE;
/*!40000 ALTER TABLE `ubicacion` DISABLE KEYS */;

INSERT INTO `ubicacion` VALUES (1,'PALMAS',1,'on',5,8,1);
INSERT INTO `ubicacion` VALUES (2,'TUMBEKA',1,'on',3,4,1);
INSERT INTO `ubicacion` VALUES (3,'EJIDO',1,'on',3,4,1);
/*!40000 ALTER TABLE `ubicacion` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table user_construccion
#

DROP TABLE IF EXISTS `user_construccion`;
CREATE TABLE `user_construccion` (
  `id_user_construccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT NULL,
  `status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id_user_construccion`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

#
# Dumping data for table user_construccion
#
LOCK TABLES `user_construccion` WRITE;
/*!40000 ALTER TABLE `user_construccion` DISABLE KEYS */;

INSERT INTO `user_construccion` VALUES (1,'Administrador','on');
INSERT INTO `user_construccion` VALUES (2,'Comit� de Vigilancia','on');
INSERT INTO `user_construccion` VALUES (3,'Cond�minos','on');
INSERT INTO `user_construccion` VALUES (4,'Jardineria','on');
INSERT INTO `user_construccion` VALUES (5,'Mantenimiento','on');
INSERT INTO `user_construccion` VALUES (6,'Seguridad','on');
INSERT INTO `user_construccion` VALUES (7,'Visitantes','on');
INSERT INTO `user_construccion` VALUES (8,'Uso General','on');
/*!40000 ALTER TABLE `user_construccion` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table uso_construccion
#

DROP TABLE IF EXISTS `uso_construccion`;
CREATE TABLE `uso_construccion` (
  `id_uso_construccion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) DEFAULT '0',
  `status` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id_uso_construccion`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

#
# Dumping data for table uso_construccion
#
LOCK TABLES `uso_construccion` WRITE;
/*!40000 ALTER TABLE `uso_construccion` DISABLE KEYS */;

INSERT INTO `uso_construccion` VALUES (1,'Acceso Principal','on');
INSERT INTO `uso_construccion` VALUES (2,'Acceso Secundario','on');
INSERT INTO `uso_construccion` VALUES (3,'Almacenamiento','on');
INSERT INTO `uso_construccion` VALUES (4,'Comercial','on');
INSERT INTO `uso_construccion` VALUES (5,'Delimitacion Perimetral','on');
INSERT INTO `uso_construccion` VALUES (6,'Estacionamiento Condominos','on');
INSERT INTO `uso_construccion` VALUES (7,'Estacionamiento Visitas','on');
INSERT INTO `uso_construccion` VALUES (8,'Oficinas Administrativas','on');
INSERT INTO `uso_construccion` VALUES (9,'Recreativo','on');
INSERT INTO `uso_construccion` VALUES (10,'Seguridad','on');
INSERT INTO `uso_construccion` VALUES (11,'Transportacion','on');
INSERT INTO `uso_construccion` VALUES (12,'Vialidad Interna','on');
INSERT INTO `uso_construccion` VALUES (13,'Acceso de Personal','on');
/*!40000 ALTER TABLE `uso_construccion` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table uso_inmuebles
#

DROP TABLE IF EXISTS `uso_inmuebles`;
CREATE TABLE `uso_inmuebles` (
  `id_inmueble` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_inmueble` varchar(400) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_inmueble`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

#
# Dumping data for table uso_inmuebles
#
LOCK TABLES `uso_inmuebles` WRITE;
/*!40000 ALTER TABLE `uso_inmuebles` DISABLE KEYS */;

INSERT INTO `uso_inmuebles` VALUES (1,'En Uso por el Arrendatario','on');
INSERT INTO `uso_inmuebles` VALUES (2,'En Uso por el Propietario','on');
INSERT INTO `uso_inmuebles` VALUES (3,'Sin Uso Disponible para Renta','on');
INSERT INTO `uso_inmuebles` VALUES (4,'Sin Uso Disponible para Venta\r\n','on');
INSERT INTO `uso_inmuebles` VALUES (5,'Sin Uso No Disponible','on');
INSERT INTO `uso_inmuebles` VALUES (6,'Sin Uso Propietario Desconocido','on');
INSERT INTO `uso_inmuebles` VALUES (7,'Sub Condominio','on');
/*!40000 ALTER TABLE `uso_inmuebles` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table usuarios_system
#

DROP TABLE IF EXISTS `usuarios_system`;
CREATE TABLE `usuarios_system` (
  `id_user_system` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `apellido` varchar(300) NOT NULL,
  `telefono` varchar(299) NOT NULL,
  `direccion` varchar(500) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `nivel_user` int(6) NOT NULL,
  `usuario_sistema` varchar(3) NOT NULL,
  `status` varchar(4) NOT NULL,
  `correo` varchar(1000) DEFAULT NULL,
  `observaciones` text,
  `firma` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_user_system`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

#
# Dumping data for table usuarios_system
#
LOCK TABLES `usuarios_system` WRITE;
/*!40000 ALTER TABLE `usuarios_system` DISABLE KEYS */;

INSERT INTO `usuarios_system` VALUES (1,'Administrador','','','','admin','4dm1n',1,'','on','','',NULL);
INSERT INTO `usuarios_system` VALUES (5,'jesus gabriel 1','','2882821','reg. 102 mza. 108 lte .16 av 149 entre 28  y 30 11111','gabo1','Gabo19841',1,'','on','info@sectornetcancun.com1','test1',NULL);
/*!40000 ALTER TABLE `usuarios_system` ENABLE KEYS */;
UNLOCK TABLES;

#
# Source for table zonas
#

DROP TABLE IF EXISTS `zonas`;
CREATE TABLE `zonas` (
  `id_zona` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id_zona`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

#
# Dumping data for table zonas
#
LOCK TABLES `zonas` WRITE;
/*!40000 ALTER TABLE `zonas` DISABLE KEYS */;

INSERT INTO `zonas` VALUES (3,'Playa del Carmen','on');
INSERT INTO `zonas` VALUES (4,'Riviera Maya','on');
INSERT INTO `zonas` VALUES (7,'Zona Hotelera','on');
INSERT INTO `zonas` VALUES (8,'cozumel','on');
/*!40000 ALTER TABLE `zonas` ENABLE KEYS */;
UNLOCK TABLES;

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
