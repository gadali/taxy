<?php
include("../includes/login.php");
$amenidad= lista($link, "operadores", "where asignado='off' order by nombre asc", "");
$ubi= lista($link, "ubicacion", "order by nombre asc", "");
//$opc= lista($link, "operadoresc", "order by nombre asc", "");

//print_r($amenidad);
?>
<!DOCTYPE html>
<!--
BeyondAdmin - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.5
Version: 1.4.2
Purchase: http://wrapbootstrap.com
-->

<html ">
<!-- Head -->
<head>
    <meta charset="utf-8" />
    <title>Roles </title>

    <meta name="description" content="data tables" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon">

    <!--Basic Styles-->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link id="bootstrap-rtl-link" href="" rel="stylesheet" />
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/assets/css/weather-icons.min.css" rel="stylesheet" />

    <!--Fonts-->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">

    <!--Beyond styles-->
    <link id="beyond-link" href="/assets/css/beyond.min.css" rel="stylesheet" />
    <link href="/assets/css/demo.min.css" rel="stylesheet" />
    <link href="/assets/css/typicons.min.css" rel="stylesheet" />
    <link href="/assets/css/animate.min.css" rel="stylesheet" />
    <link id="skin-link" href="" rel="stylesheet" type="text/css" />

    <!--Page Related styles-->
    <link href="/assets/css/dataTables.bootstrap.css" rel="stylesheet" />

    <!--Skin Script: Place this script in head to load scripts for skins and rtl support-->
    <script src="/assets/js/skins.min.js"></script>
</head>
<!-- /Head -->
<!-- Body -->
<body>
    <!-- Loading Container -->
    <div class="loading-container">
        <div class="loader"></div>
    </div>
    <!--  /Loading Container -->
    <!-- Navbar -->
   <?php include("../includes/topvar.php"); ?>
    <!-- Main Container -->
    <div class="main-container container-fluid">
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <div class="page-sidebar" id="sidebar">
               <?php 
                	include("../includes/menu.php"); 
                ?>
            </div>
            <!-- /Page Sidebar --> 
            
            <!-- Page Content -->
            <div class="page-content">
                <!-- Page Breadcrumb -->
                <div class="page-breadcrumbs">
                    <ul class="breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="/">Home</a>
                        </li>
                        <li class="active">Aldeas</li>
                    </ul>
                </div>
                <!-- /Page Breadcrumb -->
               
                <!-- Page Body -->
                <div class="page-body">
                   
                    <div class="row">
                       
                        	<?php 
                        	$date = date("d-m-Y");
                        	for($a=0; $a<=30; $a++){
								$mod_date = strtotime($date."+ 1 days");
								$fecha_ac= date("d-m-Y",$mod_date);
								$date= $fecha_ac;
								$fechas[]= $date;
                        	}
                        	$b=0;
                        	$c=0;
                        	
                        	foreach($fechas as $f){
                        		$c++;
                        		
                        		print '
                        			<div class="col-xs-12 col-md-6">
				                            <div class="well with-header  with-footer">
				                                <div class="header bg-blue">
				                                    '.$f.'
				                                </div>
						                               <div class="panel-group accordion" id="accordion'.$c.'">
                        		';
                        		
                        		foreach($ubi as $ub){
                        			$b++;
                        			
                                   $operadores_dia='';
                                    for ($i=0; $i<$ub["operadores_dia"]; $i++) { 
                                        $operador= array_pop($amenidad);
                                        $operadores_dia.= $operador["nombre"]."-".$operador["tel"]."<br>";
                                        $y= 'UPDATE operadores set asignado="on" where id_operador='.$operador["id"];
                                        $yy= mysql_query($y, $link);
                                        $num= count($amenidad);
	                        			if($num==0){
	                                		$amenidad= lista($link, "operadores", "where asignado='off' order by nombre asc", "");
	                                	}
                                    }

                                    $operadores_tarde='';
                                    for ($r=0; $r< $ub["operadores_tarde"]; $r++) { 
                                        $operador_tarde= array_pop($amenidad);
                                        $operadores_tarde.= $operador_tarde["nombre"]."-".$operador_tarde["tel"]."<br>";
                                        $q= 'UPDATE operadores set asignado="on" where id_operador='.$operador_tarde["id"];
                                        $qq= mysql_query($q, $link);
                                        $num= count($amenidad);
	                        			if($num==0){
	                                		$amenidad= lista($link, "operadores", "where asignado='off' order by nombre asc", "");
	                                	}
                                    }
 									
 									
                        			print '
	                        			
	                                                   
	                                                            <h4 class="panel-title">
	                                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion'.$c.'" href="#cuerpo'.$b.'">
	                                                                   '.$ub["nombre"].'
	                                                                </a>
	                                                            </h4>
	                                                      
	                                                        
	                                                            <div class="panel-body border-red">
	                                                            	operadores turno dia '.$ub["operadores_dia"].'  <br>
                                                                    '.$operadores_dia.'  <br>
	                                                            	operadores turno Vespertino '.$ub["operadores_tarde"].'  <br>
                                                                    '.$operadores_tarde.'
                                                                    <br><br>
                                                                    '.$num.'
	                                                           </div>
	                                                        
	                                                
	                        		';
	                        		$l= 'UPDATE operadores set asignado="off" where asignado="on"';
                                	$ll= mysql_query($l, $link);
                                	
                        		}
                        		
                        print '</div>
			                            </div>
			                           
			                        </div>';
                        	}
                        	?>
                        
                    </div>
                </div>
                <!-- /Page Body -->
            </div>
            <!-- /Page Content -->
        </div>
        <!-- /Page Container -->
        <!-- Main Container -->

    </div>

    <!--Basic Scripts-->
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/slimscroll/jquery.slimscroll.min.js"></script>

    <!--Beyond Scripts-->
    <script src="/assets/js/beyond.min.js"></script>

    <!--Page Related Scripts-->
    <script src="/assets/js/datatable/jquery.dataTables.min.js"></script>
    <script src="/assets/js/datatable/ZeroClipboard.js"></script>
    <script src="/assets/js/datatable/dataTables.tableTools.min.js"></script>
    <script src="/assets/js/datatable/dataTables.bootstrap.min.js"></script>
    <script src="/assets/js/datatable/datatables-init.js"></script>
    <script src="/assets/js/eliminar_datos.js"></script>
    <script>
        InitiateSearchableDataTable.init();
    </script>
    
</body>
<!--  /Body -->
</html>
